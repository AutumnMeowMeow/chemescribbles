ChemE-Scribbles TODO List
=========================

Infrastructure
--------------

  - Define a binary interaction table format and use it

  - Define a unit operations input file (streams, units, method, etc.)


Methods
-------

  - Move to mixture VLE


Graphs
------

  - Pxy diagram

  - Thiele diagram

  - Vapor pressure curve
