March 24, 2018

Time to pick this back up.  I have switched roles at work and am now
in a controls/automation position, so I find process design / process
engineering to be a bit more fun for my hobby time.  (I am SO weird, I
know.)  I want to get more things working so that in time I can have
my very own simulator even if it lacks a real UI.

Fixed up the Javadoc warnings.  Looks like I had an ideal fluid flash
working, and was about to get a binary interaction method in.  The
classic test here is Wilson method with water-ethanol, looking for
that famous azeotrope.  I will get onto that as we get out of spring.

May 8, 2017

RK and SRK are now in.  IdealFluid is mostly working too.

This is turning out to be quite a bit easier than it seemed at first.

May 4, 2017

PengRobinson is working well enough to create a pressure-enthalpy
diagram.  Along the way I have discovered:

* The residual enthalpy INCLUDES the phase change energy.  EOS all by
  themselves are enough to estimate the vapor pressure curve AND heat
  of vaporization.  All they need is ideal gas heat capacity and heat
  of formation to do the rest.

* JFreeChart is awesome as always.  Sooo easy to get my chart up and
  running.

* I absolutely love the Property interface.  There is no way I am ever
  going back to "convert everything into SI, do math, then have client
  convert it back".

Tomorrow I hope to clean up a little more and get this pushed up.

December 1, 2016

Making progress.  The first big check is going well: I can use
PengRobinson to estimate residual properties (Hr, Sr) and so far it is
matching the answers from Smith, Van Ness, and Abbott.  I have a very
simple method to pull properties from files and have started making a
"database" (ick, too strong a word) for compounds and properties.
This "build a simulator using only textbooks" thing is starting to
feel like it might be able to come together.

November 12, 2016

Holy balls this was a long day.

So it turns out that a cubic equation of state is not actually cubic,
it is a rational cubic.  Meaning its derivative (dP/dV) is fourth
order in V.  This makes quite a bit of difference in how one thinks
about things.

November 11, 2016

I've got a basic Peng-Robinson saturated pressure finder.  Performance
has been awful, but what is interesting is where the bottleneck is: it
is in the floating point conversion going to the logging calls.  For
now I am using a DEBUG compile guard, but that will eventually need to
become isXenabled() calls.

Next up is separating the "calculate this" logic from the "find the
root of that".  I also need to better define the bounding box for an
isotherm / isobar by locating the extrema in V and then solving the
corresponding OTHER saturated value.  So for example in the find Psat
case, I can immediately find the V extrema, then need to plug those in
with the given Tsat to find the minimum and maximum Psat bounds, and
then restrict the search to that.  Same goes for using V and Psat to
find the Tsat bounds.  It will be some algebra to find the forms of
those equations, but once done it should make finding the vapor
pressure curves very quick indeed.

Time to get some sleep.

November 3, 2016

This project has finally really started.  There is no timeline for
anything useful, it might sit forever as just "what I do on a few
random evenings", but then again it might also bloom into something
interesting in about 2-3 years.  So what will this be?  Ultimately, a
process simulator that does NOT care about CAPE-OPEN or look like the
standard set of streams and blocks that the DOE ASPEN project made
popular circa 1983.

This simulator has the following design goals:

* Written in Java, to take advantage of the vast tools available in
  the Java ecosystem.  No CAPE-OPEN, no OLE, instead we have
  JFreeCharts, commons-math, and the ability to run over web, command
  line, or traditional GUI.  (Maybe even a computer algebra system for
  custom methods...)

* Initially, a limited number of methods.  Ideal gas, Peng-Robinson,
  Redlich-Kwong, Wilson, NRTL, and of course water.  On the other
  terms (heat capacity, heat of formation, viscosity, etc.) I will
  start with the methods in Reid, Prausnitz, and Poling, and no
  others.  If this somehow takes off then outside contributors can add
  classes to implement other methods.

* Easy programmability in Java.  A Pxy diagram should be as simple as
  "Pxy(comp1, comp2, pressure, n);" which should pull up a chart that
  can be trivially printed.  I will not spend time worrying right now
  about parsing the ASPEN INPUT file specification.  My own equivalent
  will be developed only after I have about 80% coverage of my own use
  cases.

* ABSOLUTELY ZERO DATA OR REFERENCES THAT REQUIRE PAYMENT TO OBTAIN.
  Screw DIPPR, it ain't happening.  98% usability is possible using
  the data in textbooks, NIST, Perry's, etc.: I will hand-copy them in
  as I use it to solve my own problems.

* Followup to the above: if this ever takes off and becomes a premier
  open-source chemical process simulator, then equipment vendors need
  to know now that I WILL NOT build secrecy into this, EVER.  If you
  want people to use the parameters of your Super-Sekret Random
  Pakking in this program, then you will need to provide those
  parameters in clear form and anyone will be able to see them, use
  them, and copy them into other simulators.  I'm not going to enable
  anyone to hide their true performance characteristics.

All that said, let's begin!

I've got stubs in for the first method (Peng Robinson), a few basic
properties, and the loading elements and compounds.  All of the
internal calcs will be in SI of course, but the properties will
trivially translate to/from SI.  The object structure is coming out
smoothly, it looks loosely similar to the typical terminology so far
but with some small twists.  For example I'm pretty sure I want
Composition to have a phase parameter, such that it can specify a
vapor, liquid, or total composition -- and then the corresponding
flash calc can use whichever phase composition was specified to figure
out the rest.
