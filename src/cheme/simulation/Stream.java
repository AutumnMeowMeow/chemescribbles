/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.simulation;

import java.util.List;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import cheme.compounds.Mixture;
import cheme.compounds.MoleFraction;
import cheme.methods.Method;
import cheme.methods.ThermoStateException;
import cheme.properties.Enthalpy;
import cheme.properties.Pressure;
import cheme.properties.Temperature;

/**
 * A Stream represents a molar flow of a vapor/liquid fluid through a
 * process.
 */
public class Stream {

    /**
     * The Logger to emit to.
     */
    private static Logger log = LogManager.getLogger(Stream.class.getName());

    /**
     * The stream's composition.
     */
    private Mixture components;

    /**
     * This stream's name.
     */
    private String name;

    /**
     * Get this stream's name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Set this stream's name.
     *
     * @param name the name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * This stream's temperature.
     */
    private Temperature temperature;

    /**
     * This stream's pressure.
     */
    private Pressure pressure;

    /**
     * Compute the molecular weight for this mixture.
     *
     * @return molecular weight
     */
    public final double mw() {
        return components.mw();
    }

    /**
     * Construct a stream using a list of components.  Note that the final
     * mole fractions will be normalized against those provided.  So one
     * could for example provide a list of compounds and mole flows and they
     * would be converted to mole fractions here.
     *
     * @param name the name of this stream
     * @param T the temperature of this stream
     * @param P the pressure of this stream
     * @param components a list of components and mole fractions.  Stream
     * creates its own copy of this list.
     */
    public Stream(final String name, final Temperature T, final Pressure P,
        final List<MoleFraction> components) {

        log.entry(T, P, components);

        this.name               = name;
        this.components         = new Mixture(components);
        this.temperature        = T;
        this.pressure           = P;
    }

    /**
     * Make human-readable description of this Stream.
     *
     * @return displayable String
     */
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(name);
        str.append(": ");
        str.append(temperature);
        str.append(" ");
        str.append(pressure);
        str.append("\n");
        str.append(components);
        return str.toString();
    }

    /**
     * Compute the real enthalpy at a specific temperature and pressure.
     *
     * @param method the method to use
     * @param T temperature to compute enthalpy at
     * @param P pressure to compute enthalpy at
     * @return the enthalpy value
     * @throws ThermoStateException if the temperature is outside the valid
     * range
     */
    public final Enthalpy H(final Method method, final Temperature T,
        final Pressure P) throws ThermoStateException {

        // TODO
        return Enthalpy.at(0, "J/mol");
    }

}
