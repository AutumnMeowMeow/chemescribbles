/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.tests.data;

import java.util.ArrayList;
import java.util.Random;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.commons.math3.stat.StatUtils;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Stroke;
import java.io.File;
import java.io.IOException;
import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RefineryUtilities;

import cheme.controls.Distribution;
import cheme.controls.SignalStats;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.TDistribution;
import org.apache.commons.math3.special.Erf;

/**
 * Test statistics concepts.
 */
public class StatisticsTests {

    /**
     * The Logger to emit to.
     */
    private static Logger log = LogManager.getLogger(StatisticsTests.class.getName());

    /**
     * Track a signal through a line out, ramp up, and ramp down.
     */
    private static void test1() {
        ArrayList<TimePair> changes = new ArrayList<TimePair>();
        changes.add(new TimePair(100, 2.0, 1.0));
        changes.add(new TimePair(200, 0.0, 1.0));
        changes.add(new TimePair(300, 0.0, 5.0));
        changes.add(new TimePair(400, 0.0, 1.0));
        changes.add(new TimePair(500, -1.0, 1.0));
        changes.add(new TimePair(800, 0.0, 1.0));
        changes.add(new TimePair(1000,0.0, 3.0));
        changes.add(new TimePair(1200,0.0, 1.0, 200.0));

        SignalTest signal = new SignalTest(0, 1.0, changes);
        double [] signalData = new double[1500];
        for (int i = 0; i < signalData.length; i++) {
            signalData[i] = signal.getNext();
        }

        XYSeries rawDataCurve = new XYSeries("Raw Data", false);
        XYSeries avgCurve = new XYSeries("Online Avg", false);
        XYSeries stdDevCurve = new XYSeries("Online StdDev", false);
        XYSeries uclCurve = new XYSeries("UCL", false);
        XYSeries lclCurve = new XYSeries("LCL", false);
        XYSeries slopeCurve = new XYSeries("Slope", false);
        int cycleN = 30;
        double confidence = 0.95;
        SignalStats stats = new SignalStats(confidence, cycleN);
        for (int i = 0; i < signalData.length; i++) {
            stats.add(signalData[i]);
            rawDataCurve.add(i, signalData[i]);
            avgCurve.add(i, stats.getMean());
            stdDevCurve.add(i, stats.getStdDev());
            uclCurve.add(i, stats.getUcl());
            lclCurve.add(i, stats.getLcl());
            slopeCurve.add(i, stats.getSlope());
        }

        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(rawDataCurve);
        dataset.addSeries(avgCurve);
        dataset.addSeries(stdDevCurve);
        dataset.addSeries(uclCurve);
        dataset.addSeries(lclCurve);
        dataset.addSeries(slopeCurve);
        createLivePlot(dataset);
    }

    /**
     * Run the unit tests.
     */
    public static void runTests() {
        try {
            log.info("Beginning statistics tests...");

            if (true) {
                test1();
                return;
            }

            double [] randomData = new double[360];
            double range = 10.0;
            Random random = new Random(0);
            for (int i = 0; i < randomData.length; i++) {
                randomData[i] = random.nextGaussian() * range /* + 4 * i */;
            }
            log.info("Mean: " + StatUtils.mean(randomData));
            log.info("Var:  " + StatUtils.variance(randomData));

            // Perform the "online algorithm" for computing mean and
            // variance.
            int count = 0;
            double mean = 0.0;
            double M2 = 0.0;
            for (int i = 0; i < randomData.length; i++) {
                count++;
                double newValue = randomData[i];
                double delta = newValue - mean;
                mean = mean + delta / count;
                double delta2 = newValue - mean;
                M2 = M2 + (delta * delta2);
                log.info("Iter " + i + " mean " + mean +
                    " var " + (M2 / (count - 1)));
            }

            double [] window = new double[30];

            /*
            // Now try two kinds of moving average: full and exponential
            // decay
            double ema = randomData[0];
            // double alpha = 1.0 / window.length;
            double alpha = 0.25;
            double emaSlope = randomData[1] - randomData[0];

            XYSeries rawDataCurve = new XYSeries("Raw Data", false);
            XYSeries avgCurve = new XYSeries("Moving Avg", false);
            XYSeries emaCurve = new XYSeries("Exp Decay Moving Avg", false);
            XYSeries rawSlopeCurve = new XYSeries("Slope Raw Data", false);
            XYSeries emaSlopeCurve = new XYSeries("Slope EDM Avg", false);
            for (int i = 0; i + window.length < randomData.length; i++) {
                for (int j = 0; j < window.length; j++) {
                    window[j] = randomData[i + j];
                }
                ema = (alpha * randomData[i]) + (1.0 - alpha) * ema;
                rawDataCurve.add(i, randomData[i]);
                avgCurve.add(i, StatUtils.mean(window));
                emaCurve.add(i, ema);
                if (i > 0) {
                    rawSlopeCurve.add(i - 1, randomData[i] - randomData[i - 1]);
                    emaSlope = (alpha * (randomData[i] - randomData[i - 1])) +
                        (1.0 - alpha) * emaSlope;
                    emaSlopeCurve.add(i - 1, emaSlope);

                    log.info("Iter " + i + " AVG: " + StatUtils.mean(window) +
                        " EMA: " + ema + " SLOPE RAW: " +
                        (randomData[i] - randomData[i - 1]) +
                        " SLOPE EMA: " + emaSlope);
                } else {
                    log.info("Iter " + i + " AVG: " + StatUtils.mean(window) +
                        " EMA: " + ema);
                }
            }
            */

            XYSeries rawDataCurve = new XYSeries("Raw Data", false);
            XYSeries avgCurve = new XYSeries("Online Avg", false);
            XYSeries stdDevCurve = new XYSeries("Online StdDev", false);
            XYSeries uclCurve = new XYSeries("UCL", false);
            XYSeries lclCurve = new XYSeries("LCL", false);
            XYSeries ucl2Curve = new XYSeries("UCL2", false);
            XYSeries lcl2Curve = new XYSeries("LCL2", false);
            XYSeries totalAvgCurve = new XYSeries("Total Avg", false);
            Distribution dist = new Distribution();
            Distribution totalDist = new Distribution();
            double confidence = 0.9;
            double confidence2 = 0.95;
            for (int i = 0; i + window.length < randomData.length;
                 i += window.length) {

                dist.reset();
                for (int j = 0; j < window.length; j++) {
                    rawDataCurve.add(i + j, randomData[i + j]);
                    dist.add(randomData[i + j]);
                    totalDist.add(randomData[i + j]);
                    avgCurve.add(i + j, dist.getMean());
                    totalAvgCurve.add(i + j, totalDist.getMean());
                    // stdDevCurve.add(i + j, dist.getStdDev());
                    uclCurve.add(i + j, dist.getUcl(confidence));
                    lclCurve.add(i + j, dist.getLcl(confidence));
                    ucl2Curve.add(i + j, dist.getUcl(confidence2));
                    lcl2Curve.add(i + j, dist.getLcl(confidence2));

                    window[j] = randomData[i + j];
                }
            }

            XYSeriesCollection dataset = new XYSeriesCollection();
            dataset.addSeries(rawDataCurve);
            dataset.addSeries(avgCurve);
            dataset.addSeries(totalAvgCurve);
            dataset.addSeries(stdDevCurve);
            dataset.addSeries(uclCurve);
            dataset.addSeries(lclCurve);
            dataset.addSeries(ucl2Curve);
            dataset.addSeries(lcl2Curve);
            /*
            dataset.addSeries(emaCurve);
            dataset.addSeries(rawSlopeCurve);
            dataset.addSeries(emaSlopeCurve);
             */
            createLivePlot(dataset);

            /*
            for (double i = 1; i <= 20; i += 1) {
                log.info("" + i/10.0 + "\t" + Distribution.gamma(i/10.0));
            }
            */

            if (false) {

            /*
            for (double i = -20; i <= 20; i += 1) {
                double mine = Distribution.erf(i/10.0);
                double theirs = Erf.erf(i/10.0);
                log.info("" + i/10.0 + "\t" +
                    mine + "\t" + theirs + "\t" + (theirs - mine));
            }
            for (int v = 1; v < 22; v++) {
                log.info("V = " + v);
                TDistribution tPDF = new TDistribution(v);
                for (double t = -20; t <= 20; t += 1) {
                    double mine = Distribution.tDensity(t/10.0, v);
                    double theirs = tPDF.density(t/10.0);
                    log.info("" + t/10.0 + "\t" +
                        mine + "\t" + theirs + "\t" + (theirs - mine));
                }
                for (double t = -20; t <= 20; t += 1) {
                    double mine = Distribution.tCumulative(t/10.0, v);
                    double theirs = tPDF.cumulativeProbability(t/10.0);
                    log.info("" + t/10.0 + "\t" +
                        mine + "\t" + theirs + "\t" + (theirs - mine));
                }
            }
            */

            Distribution d1 = new Distribution();
            d1.add(3.0);
            d1.add(4.0);
            d1.add(1.0);
            d1.add(2.1);
            Distribution d2 = new Distribution();
            d2.add(490.2);
            d2.add(340.0);
            d2.add(433.9);

            log.info("Same: " + d1.isSame(d2, 0.90));

            log.info("D1: " + d1.getLcl(0.95) + " < " + d1.getMean() +
                " < " + d1.getUcl(0.95));
            log.info("D2: " + d2.getLcl(0.95) + " < " + d2.getMean() +
                " < " + d2.getUcl(0.95));

            log.info("D1: " + d1.getLcl(0.90) + " < " + d1.getMean() +
                " < " + d1.getUcl(0.90));
            log.info("D1: " + d1.getLcl(0.45) + " < " + d1.getMean() +
                " < " + d1.getUcl(0.45));

            }

            log.info("Done 1.");
        } catch (Throwable t) {
            log.error(t, t);
        }
    }

    /**
     * Main entry point.
     *
     * @param args Command line arguments
     */
    public static void main(final String [] args) {
        try {
            runTests();
        } catch (Throwable t) {
            log.error(t, t);
        }
    }

    /**
     * Create a new XY plot and display it.
     *
     * @param dataset series' to plot
     */
    private static void createLivePlot(final XYSeriesCollection dataset) {

        JFreeChart chart = ChartFactory.createScatterPlot("PLOT",
            "X", "Y",
            dataset,
            PlotOrientation.VERTICAL,
            false,      // Legend
            true,       // Tooltips
            false);     // URLs

        XYPlot plot = chart.getXYPlot();

        NumberAxis yAxis = new NumberAxis("Y");
        // yAxis.setRange(1.0, 3.0);
        plot.setRangeAxis(yAxis);

        NumberAxis xAxis = new NumberAxis("X");
        plot.setDomainAxis(xAxis);

        chart.setBackgroundPaint(Color.WHITE);
        plot.setOutlinePaint(Color.BLACK);

        // Set markers
        Stroke thickStroke = new BasicStroke(1);
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesPaint(0, Color.BLUE);
        // renderer.setSeriesLinesVisible(0, false);
        renderer.setSeriesShapesVisible(0, true);
        renderer.setSeriesStroke(0, thickStroke, false);
        renderer.setSeriesPaint(1, Color.RED);
        // renderer.setSeriesLinesVisible(1, false);
        renderer.setSeriesShapesVisible(1, true);
        renderer.setSeriesStroke(1, thickStroke, false);
        renderer.setSeriesPaint(2, Color.GREEN);
        // renderer.setSeriesLinesVisible(2, false);
        renderer.setSeriesShapesVisible(2, true);
        renderer.setSeriesStroke(2, thickStroke, false);
        renderer.setSeriesPaint(3, Color.CYAN);
        // renderer.setSeriesLinesVisible(3, false);
        renderer.setSeriesShapesVisible(3, true);
        renderer.setSeriesStroke(3, thickStroke, false);
        renderer.setSeriesPaint(4, Color.YELLOW);
        // renderer.setSeriesLinesVisible(4, false);
        renderer.setSeriesShapesVisible(4, true);
        renderer.setSeriesStroke(4, thickStroke, false);
        plot.setRenderer(renderer);

        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(800, 600));
        JFrame frame = new JFrame() {
            {
                setContentPane(chartPanel);
                setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                pack();
                RefineryUtilities.centerFrameOnScreen(this);
                setVisible(true);
            }
        };
    }

    /**
     * Create a new XY plot and save it to a file.
     *
     * @param pngFilename file to save chart to, as PNG
     * @param dataset series' to plot
     */
    private static void createPlot(final String pngFilename,
        final XYSeriesCollection dataset) throws IOException {

        JFreeChart chart = ChartFactory.createScatterPlot("PLOT",
            "X", "Y",
            dataset,
            PlotOrientation.VERTICAL,
            false,      // Legend
            true,       // Tooltips
            false);     // URLs

        XYPlot plot = chart.getXYPlot();

        NumberAxis yAxis = new NumberAxis("Y");
        // yAxis.setRange(1.0, 3.0);
        plot.setRangeAxis(yAxis);

        NumberAxis xAxis = new NumberAxis("X");
        plot.setDomainAxis(xAxis);

        chart.setBackgroundPaint(Color.WHITE);
        plot.setOutlinePaint(Color.BLACK);

        // Set markers
        Stroke thickStroke = new BasicStroke(1);
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesPaint(0, Color.BLUE);
        // renderer.setSeriesLinesVisible(0, false);
        renderer.setSeriesShapesVisible(0, true);
        renderer.setSeriesStroke(0, thickStroke, false);
        renderer.setSeriesPaint(1, Color.RED);
        // renderer.setSeriesLinesVisible(1, false);
        renderer.setSeriesShapesVisible(1, true);
        renderer.setSeriesStroke(1, thickStroke, false);
        renderer.setSeriesPaint(2, Color.GREEN);
        // renderer.setSeriesLinesVisible(2, false);
        renderer.setSeriesShapesVisible(2, true);
        renderer.setSeriesStroke(2, thickStroke, false);
        renderer.setSeriesPaint(3, Color.CYAN);
        // renderer.setSeriesLinesVisible(3, false);
        renderer.setSeriesShapesVisible(3, true);
        renderer.setSeriesStroke(3, thickStroke, false);
        renderer.setSeriesPaint(4, Color.YELLOW);
        // renderer.setSeriesLinesVisible(4, false);
        renderer.setSeriesShapesVisible(4, true);
        renderer.setSeriesStroke(4, thickStroke, false);
        plot.setRenderer(renderer);

        ChartUtilities.saveChartAsPNG(new File(pngFilename), chart, 1500, 800,
            new ChartRenderingInfo(new StandardEntityCollection()));
    }

}
