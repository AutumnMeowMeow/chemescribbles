/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.tests.data;

import java.util.ArrayList;
import java.util.Random;

/**
 * SignalTest provides a simulated signal with noise that can ramp up and
 * down at specified "time" intervals.
 */
public class SignalTest {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The current "time".
     */
    private int time = 0;

    /**
     * The current signal mean value.
     */
    private double mean = 0.0;

    /**
     * The current signal desired standard deviation.
     */
    private double sigma = 1.0;

    /**
     * The current slope.
     */
    private double slope = 0.0;

    /**
     * The list of changes to make to the signal.
     */
    private ArrayList<TimePair> changes = null;

    /**
     * The random number generator used to create data points.
     */
    private Random random = new Random(0);

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor, creates a straight linear signal.
     *
     * @param x0 the initial mean value for the signal
     * @param sigma the initial standard deviation
     */
    public SignalTest(final double x0, final double sigma) {
        this(x0, sigma, 0.0);
    }

    /**
     * Public constructor, creates a sloped linear signal.
     *
     * @param x0 the initial mean value for the signal
     * @param sigma the initial standard deviation
     * @param slope the slope
     */
    public SignalTest(final double x0, final double sigma, final double slope) {
        this.mean = x0;
        this.sigma = sigma;
        this.slope = slope;
    }

    /**
     * Public constructor, creates a changing signal.
     *
     * @param x0 the initial mean value for the signal
     * @param sigma the initial standard deviation
     * @param changes the list of changes to make to the signal
     */
    public SignalTest(final double x0, final double sigma,
        final ArrayList<TimePair> changes) {

        this.mean = x0;
        this.sigma = sigma;
        this.changes = new ArrayList<TimePair>(changes);
    }

    // ------------------------------------------------------------------------
    // SignalTest -------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Get the next point.
     *
     * @return the next point
     */
    public double getNext() {
        time++;
        mean += slope;
        if ((changes != null) && (changes.size() > 0)) {
            TimePair nextChange = changes.get(0);
            if (nextChange.time <= time) {
                changes.remove(0);
                sigma = nextChange.sigma;
                slope = nextChange.slope;
                if (nextChange.stepChange != null) {
                    mean += nextChange.stepChange;
                }
            }
        }
        return ((random.nextGaussian() * sigma) + mean);
    }

}
