/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.tests.data;

/**
 * TimePair is a tuple of (int, slope, sigma, stepChange).
 */
public class TimePair {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The "time".
     */
    public int time = 0;

    /**
     * The slope.
     */
    public double slope = 0.0;

    /**
     * The standard deviation.
     */
    public double sigma = 1.0;

    /**
     * The new step change to the mean, or null if this tuple is not a step
     * change.
     */
    public Double stepChange = null;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor, no step change.
     *
     * @param time the time
     * @param slope the slope
     * @param sigma the desired standard deviation
     */
    public TimePair(final int time, final double slope, final double sigma) {
        this(time, slope, sigma, null);
    }

    /**
     * Public constructor.
     *
     * @param time the time
     * @param slope the slope
     * @param sigma the desired standard deviation
     * @param stepChange the step change to the mean
     */
    public TimePair(final int time, final double slope, final double sigma,
        final Double stepChange) {

        this.time = time;
        this.slope = slope;
        this.sigma = sigma;
        this.stepChange = stepChange;
    }

    // ------------------------------------------------------------------------
    // TimePair ---------------------------------------------------------------
    // ------------------------------------------------------------------------

}
