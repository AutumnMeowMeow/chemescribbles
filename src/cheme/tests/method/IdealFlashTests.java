/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.tests.method;

import java.util.ArrayList;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import cheme.compounds.*;
import cheme.methods.*;
import cheme.properties.*;
import cheme.ui.*;

/**
 * Test flash methods using IdealFluid.
 */
public class IdealFlashTests {

    /**
     * The Logger to emit to.
     */
    private static Logger log = LogManager.getLogger(IdealFlashTests.class.getName());

    /**
     * Test if two strings are equal, and if not throw an exception.
     *
     * @param s1 first string
     * @param s2 second string
     */
    private static void testAssert(String s1, String s2) {
        if (!s1.equals(s2)) {
            throw new RuntimeException("Test failed: " + s1 + " != " + s2);
        }
    }

    /**
     * Run the unit tests.
     *
     * @throws ThermoStateException if a test throws
     */
    public static void runTests() throws ThermoStateException {
        log.info("Beginning ideal flash tests...");

        Equilibrium state;
        IdealFluid ideal = new IdealFluid();
        Compound benzene = Compound.getByIdString("Benzene");
        Compound water = Compound.getByIdString("Water");
        ArrayList<MoleFraction> moleFracs = new ArrayList<MoleFraction>();
        moleFracs.add(new MoleFraction(benzene, 0.5));
        moleFracs.add(new MoleFraction(water, 0.5));
        Mixture mixture = new Mixture(moleFracs);

        state = ideal.state(mixture, Temperature.at(80, "C"),
            Pressure.at(0, "psig"));
        testAssert(String.format("%4.2f", state.VFrac()), "0.00");
        testAssert(String.format("%s", state.phase().toString()),
            "LIQUID_MIXTURE");

        state = ideal.state(mixture, Temperature.at(95, "C"),
            Pressure.at(0, "psig"));
        testAssert(String.format("%4.2f", state.VFrac()), "1.00");
        testAssert(String.format("%s", state.phase().toString()),
            "VAPOR_MIXTURE");

        state = ideal.state(mixture, Temperature.at(90, "C"),
            Pressure.at(0, "psig"));
        log.info(state.toString());
        testAssert(String.format("%s", state.phase().toString()),
            "VLE_MIXTURE");

        state = ideal.state(mixture, Temperature.at(92.5, "C"),
            Pressure.at(0, "psig"));
        log.info(state.toString());
        testAssert(String.format("%s", state.phase().toString()),
            "VLE_MIXTURE");

        new TXYDiagram(benzene, water, ideal, Pressure.at(0, "psig"), "psig",
            Temperature.at(78, "C"), Temperature.at(101, "C"), "C", 0.0, 1.0,
            51);

        Compound ethanol = Compound.getByIdString("Ethanol");
        new TXYDiagram(ethanol, water, ideal, Pressure.at(0, "psig"), "psig",
            Temperature.at(78, "C"), Temperature.at(101, "C"), "C", 0.0, 1.0,
            51);

        log.info("Done.");
    }

    /**
     * Main entry point.
     *
     * @param args Command line arguments
     */
    public static void main(final String [] args) {
        try {
            runTests();
        } catch (Throwable t) {
            log.error(t, t);
        }
    }
}
