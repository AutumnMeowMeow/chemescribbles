/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.tests.method;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import cheme.compounds.Compound;
import cheme.methods.*;
import cheme.properties.*;

/**
 * Some miscellaneous computations performed while developing Scribbles.
 */
public class MiscComputations {

    /**
     * The Logger to emit to.
     */
    private static Logger log = LogManager.getLogger(MiscComputations.class.getName());

    /**
     * Main entry point.
     *
     * @param args Command line arguments
     */
    public static void main(final String [] args) {
        try {
            log.info("Starting up...");

            Compound STC = Compound.getByIdString("SiCl4");
            PengRobinson pr = new PengRobinson();
            Equilibrium state;

            double TMax = STC.Tc().C();
            double PMax = STC.Pc().psig();

            System.out.println("# STC vapor pressure curves, Peng-Robinson method");
            
            System.out.printf("P [psig],T [C],rho_l [lb/ft3],rho_unstable [lb/ft3],rho_v [lb/ft3]\n");

            for (double tC = STC.Tp().C(); tC < TMax; tC += 0.1) {
                double MW = STC.mw();
                double toDensity = MW / (453.593 * 35.3145);
                state = pr.vle(STC, Temperature.at(tC, "C"));
                double p = state.Psat().psig();
                double t = state.Tsat().C();
                System.out.printf("%15.12f %5.3f %15.12f %15.12f %15.12f %d\n",
                    p, t, toDensity / state.LiquidV().m3mol(),
                    toDensity / state.InteriorCubicEOSV().m3mol(),
                    toDensity / state.VaporV().m3mol(), state.iter());
            }
            System.out.println();
            System.out.println();
            System.out.println();

            for (double pPsig = STC.Pp().psig() + 0.01; pPsig < PMax; pPsig += 1) {
                double MW = STC.mw();
                double toDensity = MW / (453.593 * 35.3145);
                state = pr.vle(STC, Pressure.at(pPsig, "psig"));
                double p = state.Psat().psig();
                double t = state.Tsat().C();
                System.out.printf("%15.12f %5.3f %15.12f %15.12f %15.12f %d\n",
                    p, t, toDensity / state.LiquidV().m3mol(),
                    toDensity / state.InteriorCubicEOSV().m3mol(),
                    toDensity / state.VaporV().m3mol(), state.iter());
            }
            System.out.println();
            System.out.println();
            System.out.println();
            
            System.out.println("# STC Ideal gas properties");
            System.out.println("Cp_IG: " +
                STC.Cp_IG(Temperature.at(298.15, 'K')).kJmolK() + " kJ/mol-K");
            System.out.println("H_IG: " +
                STC.H_IG(Temperature.at(298.15, 'K')).kJmol() + " kJ/mol");
            System.out.println("H_IG: " +
                STC.H_IG(Temperature.at(323, 'C')).kJmol() + " kJ/mol");
            System.out.println();
            System.out.println();
            System.out.println();


            TMax = STC.Tc().C();
            PMax = STC.Pc().psig();

            System.out.println("# STC vapor pressure curves, Peng-Robinson method, including real enthalpies");
            System.out.printf("# [psig],T [C],rho_l [lb/ft3],rho_unstable [lb/ft3],rho_v [lb/ft3], iter, H_l [BTU/lb], H_v [BTU/lb]\n");


            for (double tC = 60; tC < TMax; tC += 1) {
                double MW = STC.mw();
                double toDensity = MW / (453.593 * 35.3145);
                state = pr.vle(STC, Temperature.at(tC, "C"));
                double p = state.Psat().psig();
                double t = state.Tsat().C();
                System.out.printf("%15.12f %5.3f %15.12f %15.12f %15.12f %d %15.12f %15.12f\n",
                    p, t, toDensity / state.LiquidV().m3mol(),
                    toDensity / state.InteriorCubicEOSV().m3mol(),
                    toDensity / state.VaporV().m3mol(), state.iter(),
                    state.LiquidH().BTUlbmol() / MW,
                    state.VaporH().BTUlbmol() / MW
                );
            }
            System.out.println();
            System.out.println();
            System.out.println();

            Pressure p = Pressure.at(200, "psig");
            Temperature t = Temperature.at(200, "C");

            System.out.println("# STC real gas properties");
            log.info("Ethalpy at " + p + ", " + t +
                " " + (STC.H(pr, t, p).BTUlbmol() / STC.mw()));

            p = Pressure.at(506, "psig");
            t = Temperature.at(233.85, "C");

            // Should be near -1655 BTU/lb
            log.info("Ethalpy at " + p + ", " + t +
                " " + (STC.H(pr, t, p).BTUlbmol() / STC.mw()));

            state = pr.state(STC, Temperature.at(220, 'C'),
                Pressure.at(200, "psig"));
            log.info("STC at 220C and 200 psig: vapor Z " + state.VaporZ() +
                " rho " +
                (1 / state.VaporV().m3mol()) * STC.mw() / 35.3145 / 453.593 +
                " lb/ft3" + " V: " + state.VaporV().m3mol() + "m3/mol" );

            log.info("Done.");
        } catch (Throwable t) {
            log.error(t, t);
        }

    }
    
}
