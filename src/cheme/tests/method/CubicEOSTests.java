/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.tests.method;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import cheme.compounds.Compound;
import cheme.methods.*;
import cheme.properties.*;

/**
 * Test cubic equation of state methods.  Note that these tests depend
 * completely on the critical temperature, pressure, and acentric factor
 * stored in Compounds.csv.  If those change, these tests will need to be
 * changed also.
 */
public class CubicEOSTests {

    /**
     * The Logger to emit to.
     */
    private static Logger log = LogManager.getLogger(CubicEOSTests.class.getName());

    /**
     * Test if two strings are equal, and if not throw an exception.
     *
     * @param s1 first string
     * @param s2 second string
     */
    private static void testAssert(String s1, String s2) {
        if (!s1.equals(s2)) {
            throw new RuntimeException("Test failed: " + s1 + " != " + s2);
        }
    }

    /**
     * Run the unit tests.
     *
     * @throws ThermoStateException if a test throws
     */
    public static void runTests() throws ThermoStateException {
        log.info("Beginning cubic equation of state tests...");

        Equilibrium state;
        PengRobinson pr = new PengRobinson();

        // Smith, Van Ness, and Abbott problem 6.16
        Compound acetylene = Compound.getByIdString("Acetylene");
        state = pr.state(acetylene, Temperature.at(300, 'K'),
            Pressure.at(40, "bara"));
        log.info("Acetylene at 300K and 40 bar: Z " + state.VaporZ() +
            " HR " + state.VaporHR() +
            " SR " + state.VaporSR());
        testAssert(String.format("%4.3f", state.VaporZ()), "0.667");
        testAssert(String.format("%4.2e", state.VaporHR().Jmol()),
            "-2.66e+03");
        testAssert(String.format("%3.2f", state.VaporSR().JmolK()),
            "-6.41");

        Compound argon = Compound.getByIdString("Argon");
        state = pr.state(argon, Temperature.at(175, 'K'),
            Pressure.at(75, "bara"));
        log.info("Argon at 175K and 75 bar: Z " + state.VaporZ() +
            " HR " + state.VaporHR() +
            " SR " + state.VaporSR());
        testAssert(String.format("%4.3f", state.VaporZ()), "0.572");
        testAssert(String.format("%4.3e", state.VaporHR().Jmol()),
            "-2.146e+03");
        testAssert(String.format("%4.2f", state.VaporSR().JmolK()),
            "-8.85");

        Compound benzene = Compound.getByIdString("Benzene");
        state = pr.state(benzene, Temperature.at(575, 'K'),
            Pressure.at(30, "bara"));
        log.info("Benzene at 575K and 30 bar: Z " + state.VaporZ() +
            " HR " + state.VaporHR() +
            " SR " + state.VaporSR());
        testAssert(String.format("%4.3f", state.VaporZ()), "0.754");
        testAssert(String.format("%4.2e", state.VaporHR().Jmol()),
            "-3.86e+03");
        testAssert(String.format("%4.1f", state.VaporSR().JmolK()),
            "-4.8");


        // Smith, Van Ness, and Abbott problem 6.14
        state = (new RedlichKwong()).state(acetylene,
            Temperature.at(300, 'K'), Pressure.at(40, "bara"));
        log.info("Acetylene at 300K and 40 bar: Z " + state.VaporZ() +
            " HR " + state.VaporHR() +
            " SR " + state.VaporSR());
        testAssert(String.format("%4.3f", state.VaporZ()), "0.695");
        testAssert(String.format("%4.3e", state.VaporHR().Jmol()),
            "-2.302e+03");
        testAssert(String.format("%3.3f", state.VaporSR().JmolK()),
            "-5.461");

        // Smith, Van Ness, and Abbott problem 6.15
        state = (new SoaveRedlichKwong()).state(acetylene,
            Temperature.at(300, 'K'), Pressure.at(40, "bara"));
        log.info("Acetylene at 300K and 40 bar: Z " + state.VaporZ() +
            " HR " + state.VaporHR() +
            " SR " + state.VaporSR());
        testAssert(String.format("%4.3f", state.VaporZ()), "0.691");
        testAssert(String.format("%4.3e", state.VaporHR().Jmol()),
            "-2.595e+03");
        testAssert(String.format("%3.2f", state.VaporSR().JmolK()),
            "-6.41");

        log.info("Done.");
    }

    /**
     * Main entry point.
     *
     * @param args Command line arguments
     */
    public static void main(final String [] args) {
        try {
            runTests();
        } catch (Throwable t) {
            log.error(t, t);
        }
    }
}
