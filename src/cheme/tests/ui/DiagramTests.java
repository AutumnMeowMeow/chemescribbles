/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.tests.ui;

import java.util.ArrayList;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import cheme.compounds.Compound;
import cheme.methods.*;
import cheme.properties.*;
import cheme.ui.PressureEnthalpyDiagram;

/**
 * Test diagrams/charts.
 */
public class DiagramTests {

    /**
     * The Logger to emit to.
     */
    private static Logger log = LogManager.getLogger(DiagramTests.class.getName());

    /**
     * Run the unit tests.
     */
    public static void runTests() {
        try {
            log.info("Beginning diagram tests...");

            Compound STC = Compound.getByIdString("SiCl4");
            Compound TCS = Compound.getByIdString("SiHCl3");

            ArrayList<Density> rhoList = new ArrayList<Density>();
            rhoList.add(Density.at(0.5 / STC.mw(), "lbmol/ft3"));
            rhoList.add(Density.at(1 / STC.mw(), "lbmol/ft3"));
            rhoList.add(Density.at(1.5 / STC.mw(), "lbmol/ft3"));
            rhoList.add(Density.at(2 / STC.mw(), "lbmol/ft3"));
            rhoList.add(Density.at(4 / STC.mw(), "lbmol/ft3"));
            rhoList.add(Density.at(6 / STC.mw(), "lbmol/ft3"));
            rhoList.add(Density.at(10 / STC.mw(), "lbmol/ft3"));
            rhoList.add(Density.at(15 / STC.mw(), "lbmol/ft3"));
            rhoList.add(Density.at(20 / STC.mw(), "lbmol/ft3"));
            rhoList.add(Density.at(30 / STC.mw(), "lbmol/ft3"));
            rhoList.add(Density.at(40 / STC.mw(), "lbmol/ft3"));
            rhoList.add(Density.at(50 / STC.mw(), "lbmol/ft3"));
            rhoList.add(Density.at(70 / STC.mw(), "lbmol/ft3"));
            rhoList.add(Density.at(90 / STC.mw(), "lbmol/ft3"));

            new PressureEnthalpyDiagram(STC, new PengRobinson(),
                Enthalpy.at(-1800 * STC.mw(), "BTU/lbmol"),
                Enthalpy.at(-1500 * STC.mw(), "BTU/lbmol"), "BTU/lbmol",
                Pressure.at(0, "psig"), Pressure.at(1000, "psig"), "psig",
                Temperature.at(-50, "C"), Temperature.at(700, "C"),
                (700 + 50) / 50 + 1, "C", rhoList, "lbmol/ft3", true);

            new PressureEnthalpyDiagram(STC, new RedlichKwong(),
                Enthalpy.at(-1800 * STC.mw(), "BTU/lbmol"),
                Enthalpy.at(-1500 * STC.mw(), "BTU/lbmol"), "BTU/lbmol",
                Pressure.at(0, "psig"), Pressure.at(1000, "psig"), "psig",
                Temperature.at(-50, "C"), Temperature.at(700, "C"),
                (700 + 50) / 50 + 1, "C", rhoList, "lbmol/ft3", true);

            new PressureEnthalpyDiagram(STC, new SoaveRedlichKwong(),
                Enthalpy.at(-1800 * STC.mw(), "BTU/lbmol"),
                Enthalpy.at(-1500 * STC.mw(), "BTU/lbmol"), "BTU/lbmol",
                Pressure.at(0, "psig"), Pressure.at(1000, "psig"), "psig",
                Temperature.at(-50, "C"), Temperature.at(700, "C"),
                (700 + 50) / 50 + 1, "C", rhoList, "lbmol/ft3", true);

            new PressureEnthalpyDiagram(STC, new IdealFluid(),
                Enthalpy.at(-1800 * STC.mw(), "BTU/lbmol"),
                Enthalpy.at(-1500 * STC.mw(), "BTU/lbmol"), "BTU/lbmol",
                Pressure.at(0, "psig"), Pressure.at(1000, "psig"), "psig",
                Temperature.at(-50, "C"), Temperature.at(700, "C"),
                (700 + 50) / 50 + 1, "C", rhoList, "lbmol/ft3", true);

            new PressureEnthalpyDiagram(TCS, new PengRobinson(),
                Enthalpy.at(-1700 * TCS.mw(), "BTU/lbmol"),
                Enthalpy.at(-1400 * TCS.mw(), "BTU/lbmol"), "BTU/lbmol",
                Pressure.at(0, "psig"), Pressure.at(1000, "psig"), "psig",
                Temperature.at(-50, "C"), Temperature.at(700, "C"),
                (700 + 50) / 50 + 1, "C", rhoList, "lbmol/ft3", true);

            log.info("Done.");
        } catch (Throwable t) {
            log.error(t, t);
        }
    }

    /**
     * Main entry point.
     *
     * @param args Command line arguments
     */
    public static void main(final String [] args) {
        try {
            runTests();
        } catch (Throwable t) {
            log.error(t, t);
        }
    }

}
