/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RefineryUtilities;

import cheme.compounds.Compound;
import cheme.compounds.Mixture;
import cheme.compounds.MoleFraction;
import cheme.methods.Method;
import cheme.methods.ThermoStateException;
import cheme.properties.Equilibrium;
import cheme.properties.Phase;
import cheme.properties.Pressure;
import cheme.properties.Temperature;

/**
 * This class creates a Txy diagram using the JFreeChart library.
 */
public class TXYDiagram extends JFrame {

    /**
     * Serializable version.
     */
    private static final long serialVersionUID = 1;

    /**
     * Create a new Txy diagram and display it in a window.
     *
     * @param compound1 the first species to compute
     * @param compound2 the second species to compute
     * @param method the method to use
     * @param pressure the system pressure
     * @param pressureUnit the units for the pressure label
     * @param temperatureMin the minimum temperature
     * @param temperatureMax the maximum temperature
     * @param temperatureUnit the units for the temperature (Y) axis
     * @param xMin the minimum x_i/y_i to compute
     * @param xMax the maximum x_i/y_i to compute
     * @param N the number of points to compute, including the points at xMin
     * and xMax
     * @throws ThermoStateException if an unrecoverable calculation error
     * occurs
     */
    public TXYDiagram(final Compound compound1, final Compound compound2,
        final Method method, final Pressure pressure, final String pressureUnit,
        final Temperature temperatureMin, final Temperature temperatureMax,
        final String temperatureUnit, final double xMin, final double xMax,
        final int N) throws ThermoStateException {

        super("Txy Diagram: " + compound1.toString() + "(1)/" +
            compound2.toString() + "(2) " + method.toString());

        final XYSeries liquidCurve = new XYSeries("Liquid", false);
        final XYSeries vaporCurve = new XYSeries("Vapor", false);

        if (xMax < xMin) {
            throw new IllegalArgumentException("xMin must be less than xMax");
        }
        if ((xMin < 0) || (xMin > 1)) {
            throw new IllegalArgumentException("xMin must be between 0 and 1");
        }
        if ((xMax < 0) || (xMax > 1)) {
            throw new IllegalArgumentException("xMin must be between 0 and 1");
        }
        if (N < 2) {
            throw new IllegalArgumentException("N must be >= 2");
        }

        double dx = (xMax - xMin) / (N - 1);
        double x = xMin;
        Temperature Tb1 = compound1.TsatFunction().eval(pressure);
        Temperature Tb2 = compound2.TsatFunction().eval(pressure);
        for (int i = 0; i < N; i++) {

            if (x > 1.0) {
                x = 1.0;
            }

            MoleFraction x1 = new MoleFraction(compound1, x);
            MoleFraction x2 = new MoleFraction(compound2, 1.0 - x);
            List<MoleFraction> components = new ArrayList<MoleFraction>(2);
            components.add(x1);
            components.add(x2);
            Mixture mixture = new Mixture(components);

            // TODO: write state(..., VaporFraction) instead of this
            // brute-force approach.

            // Find a valid temperature for VLE
            double dt = 1;
            boolean wentUp = true;
            for (double t = Tb2.K() - x * (Tb2.K() - Tb1.K()); ;) {
                Temperature T = Temperature.at(t, "K");
                Equilibrium state = method.state(mixture, T, pressure);
                if (state.phase() == Phase.VLE_MIXTURE) {
                    List<MoleFraction> Xi = state.Xi();
                    List<MoleFraction> Yi = state.Yi();
                    liquidCurve.add(Xi.get(0).fraction, T.get(temperatureUnit));
                    vaporCurve.add(Yi.get(0).fraction, T.get(temperatureUnit));
                    // We have our point, bail out.
                    break;
                }
                if (state.phase() == Phase.VAPOR_MIXTURE) {
                    // We have exceeded the two-phase temperature, come down.
                    if (wentUp) {
                        wentUp = false;
                        dt /= 2;
                    }
                    t -= dt;
                } else {
                    // We are below the two-phase temperature, go up.
                    if (wentUp == false) {
                        wentUp = true;
                        dt /= 2;
                    }
                    t += dt;
                }
            }

            x += dx;
        }

        final XYSeriesCollection dataSet = new XYSeriesCollection();
        dataSet.addSeries(liquidCurve);
        dataSet.addSeries(vaporCurve);

        final JFreeChart chart = ChartFactory.createScatterPlot("" +
            "Txy Diagram: " + compound1.toString() + "(1)/" +
            compound2.toString() + "(2) " + method.toString(),
            "Temperature [" + temperatureUnit + "]",
            "x(1), y(1)",
            dataSet,
            PlotOrientation.VERTICAL,
            false,      // Legend
            true,       // Tooltips
            false);     // URLs

        final XYPlot plot = chart.getXYPlot();
        final NumberAxis fractionAxis = new NumberAxis("x(1), y(1)");
        final NumberAxis temperatureAxis = new NumberAxis("Temperature [" +
            temperatureUnit + "]");
        fractionAxis.setRange(0.0, 1.0);
        temperatureAxis.setRange(temperatureMin.get(temperatureUnit),
            temperatureMax.get(temperatureUnit));

        plot.setDomainAxis(fractionAxis);
        plot.setRangeAxis(temperatureAxis);
        chart.setBackgroundPaint(Color.WHITE);
        plot.setOutlinePaint(Color.BLACK);

        // Set markers
        Stroke thickStroke = new BasicStroke(3);
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesPaint(0, Color.BLUE);
        renderer.setSeriesLinesVisible(0, false);
        renderer.setSeriesShapesVisible(0, true);
        renderer.setSeriesStroke(0, thickStroke, false);
        renderer.setSeriesPaint(1, Color.RED);
        renderer.setSeriesLinesVisible(1, false);
        renderer.setSeriesShapesVisible(1, true);
        renderer.setSeriesStroke(1, thickStroke, false);
        plot.setRenderer(renderer);

        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(800, 600));
        setContentPane(chartPanel);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        pack();
        RefineryUtilities.centerFrameOnScreen(this);
        setVisible(true);
    }

}
