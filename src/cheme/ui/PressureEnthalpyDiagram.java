/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Ellipse2D;
import java.util.List;
import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.LogarithmicAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RefineryUtilities;

import cheme.compounds.Compound;
import cheme.methods.Method;
import cheme.methods.ThermoStateException;
import cheme.properties.Density;
import cheme.properties.Enthalpy;
import cheme.properties.Equilibrium;
import cheme.properties.Phase;
import cheme.properties.Pressure;
import cheme.properties.Temperature;
import cheme.properties.Volume;

/**
 * This class creates a pressure-enthalpy diagram using the JFreeChart
 * library.
 */
public class PressureEnthalpyDiagram extends JFrame {

    /**
     * Serializable version.
     */
    private static final long serialVersionUID = 1;

    /**
     * Control the increment on the pressure axis.
     *
     * @param P the pressure to add to
     * @param reallySharpAndSlow if true, use a sliding scale to try to make
     * a smooth curve
     */
    private double addP(final double P, final boolean reallySharpAndSlow) {
        if (reallySharpAndSlow) {
            return P + 0.2;
        }

        if (P < 1) {
            return (P + 0.5);
        } else if (P < 10) {
            return (P + 1);
        } else if (P < 70) {
            return (P + 2);
        } else if (P < 100) {
            return (P + 10);
        } else if (P < 700) {
            return (P + 20);
        } else {
            return (P + 50);
        }
    }

    /**
     * Create a new pressure-enthalpy diagram and display it in a window.
     *
     * @param compound the species to compute
     * @param method the method to use
     * @param enthalpyMin the minimum bound on the enthalpy (X) axis
     * @param enthalpyMax the maximum bound on the enthalpy (X) axis
     * @param enthalpyUnit the units for the enthalpy (X) axis
     * @param pressureMin the minimum bound on the pressure (Y) axis
     * @param pressureMax the maximum bound on the pressure (Y) axis
     * @param pressureUnit the units for the pressure (Y) axis
     * @param temperatureMin the minimum temperature
     * @param temperatureMax the maximum temperature
     * @param temperatureLineCount the number of temperature isotherms to
     * draw, which includes the minimum and maximum temperature lines.
     * @param temperatureUnit the units for the isotherm labels
     * @param densityLines the lines of constant density to plot
     * @param densityUnit the units for the constant density labels
     * @param massUnits if true, use mass units rather than molar units
     * @throws ThermoStateException if an unrecoverable calculation error
     * occurs
     */
    public PressureEnthalpyDiagram(final Compound compound, final Method method,
        final Enthalpy enthalpyMin, final Enthalpy enthalpyMax,
        final String enthalpyUnit,
        final Pressure pressureMin, final Pressure pressureMax,
        final String pressureUnit,
        final Temperature temperatureMin, final Temperature temperatureMax,
        final int temperatureLineCount, final String temperatureUnit,
        final List<Density> densityLines, final String densityUnit,
        final boolean massUnits) throws ThermoStateException {

        this(compound, method, enthalpyMin, enthalpyMax, enthalpyUnit,
            pressureMin, pressureMax, pressureUnit, temperatureMin,
            temperatureMax, temperatureLineCount, temperatureUnit,
            densityLines, densityUnit, massUnits, false);
    }

    /**
     * Create a new pressure-enthalpy diagram and display it in a window.
     *
     * @param compound the species to compute
     * @param method the method to use
     * @param enthalpyMin the minimum bound on the enthalpy (X) axis
     * @param enthalpyMax the maximum bound on the enthalpy (X) axis
     * @param enthalpyUnit the units for the enthalpy (X) axis
     * @param pressureMin the minimum bound on the pressure (Y) axis
     * @param pressureMax the maximum bound on the pressure (Y) axis
     * @param pressureUnit the units for the pressure (Y) axis
     * @param temperatureMin the minimum temperature
     * @param temperatureMax the maximum temperature
     * @param temperatureLineCount the number of temperature isotherms to
     * draw, which includes the minimum and maximum temperature lines.
     * @param temperatureUnit the units for the isotherm labels
     * @param densityLines the lines of constant density to plot
     * @param densityUnit the units for the constant density labels
     * @param massUnits if true, use mass units rather than molar units
     * @param reallySharpAndSlow if true, draw many points to generate a very
     * smooth curve
     * @throws ThermoStateException if an unrecoverable calculation error
     * occurs
     */
    public PressureEnthalpyDiagram(final Compound compound, final Method method,
        final Enthalpy enthalpyMin, final Enthalpy enthalpyMax,
        final String enthalpyUnit,
        final Pressure pressureMin, final Pressure pressureMax,
        final String pressureUnit,
        final Temperature temperatureMin, final Temperature temperatureMax,
        final int temperatureLineCount, final String temperatureUnit,
        final List<Density> densityLines, final String densityUnit,
        final boolean massUnits,
        final boolean reallySharpAndSlow) throws ThermoStateException {

        super("Pressure-Enthalpy Diagram: " + compound.toString() + " " +
            method.toString());

        Equilibrium state;

        double mw = 1;
        String enthalpyUnitString = enthalpyUnit;
        if (massUnits) {
            mw = compound.mw();
            enthalpyUnitString = Enthalpy.massUnit(enthalpyUnit);
        }

        // Compute the phase envelope
        final XYSeries liquidCurve = new XYSeries("Liquid", false);
        final XYSeries vaporCurve = new XYSeries("Vapor", false);
        double P0 = pressureMin.get(pressureUnit);
        double Pc = compound.Pc().get(pressureUnit);
        double P1 = pressureMax.get(pressureUnit);
        for (double P = P0; (P < Pc) && (P < P1); ) {
            state = method.vle(compound, Pressure.at(P, pressureUnit));
            double p = state.Psat().get(pressureUnit);
            liquidCurve.add(state.LiquidH().get(enthalpyUnit) / mw, p);
            vaporCurve.add(state.VaporH().get(enthalpyUnit) / mw, p);

            // This is a ln(P) chart, plot fewer points as P increases.
            P = addP(P, reallySharpAndSlow);
        }

        // Add the critical point
        final XYSeries criticalPoint = new XYSeries("Critical Point");
        state = method.vle(compound, compound.Tc());
        double p = compound.Pc().get(pressureUnit);
        criticalPoint.add((state.LiquidH().get(enthalpyUnit) +
                state.VaporH().get(enthalpyUnit)) / 2.0 / mw, p);

        final XYSeriesCollection dataSet = new XYSeriesCollection();
        dataSet.addSeries(liquidCurve);
        dataSet.addSeries(vaporCurve);
        dataSet.addSeries(criticalPoint);

        // Compute the isotherms
        double Tmin = temperatureMin.K();
        double Tmax = temperatureMax.K();
        double dT = 0;
        if (temperatureLineCount > 1) {
            dT = (Tmax - Tmin) / (temperatureLineCount - 1);
        }

        int isothermSeriesMax = 3;
        for (int i = 0; i < temperatureLineCount; i++) {
            isothermSeriesMax++;
            Temperature T = Temperature.at(Tmin + dT * i, "K");
            final XYSeries isothermCurve = new XYSeries("T = " +
                T.get(temperatureUnit), false);
            for (double P = P0; P < P1; ) {
                state = method.state(compound, T, Pressure.at(P, pressureUnit));
                if (state.VaporH() != null) {
                    isothermCurve.add(state.VaporH().get(enthalpyUnit) / mw,
                        P);
                } else {
                    isothermCurve.add(state.LiquidH().get(enthalpyUnit) / mw,
                        P);
                }

                // This is a ln(P) chart, plot fewer points as P increases.
                P = addP(P, reallySharpAndSlow);
            }
            dataSet.addSeries(isothermCurve);
        }

        int densitySeriesMax = isothermSeriesMax;
        for (Density rho: densityLines) {
            densitySeriesMax++;
            Volume V = rho.getVolume();
            final XYSeries densityCurve = new XYSeries("rho = " +
                rho.get(densityUnit), false);
            for (double P = P0; P < P1; ) {
                try {
                    state = method.state(compound, Pressure.at(P, pressureUnit),
                        V);
                } catch (ThermoStateException e) {
                    // We can get a temperature that is outside the heat
                    // capacity data for this density and pressure.
                }
                if ((state != null) &&
                    (state.phase() != Phase.LIQUID) &&
                    (state.phase() != Phase.VAPOR)
                ) {
                    // Disregard data inside the two-phase region.
                } else if (state != null) {
                    if (state.VaporH() != null) {
                        densityCurve.add(state.VaporH().get(enthalpyUnit) / mw,
                            P);
                    } else {
                        densityCurve.add(state.LiquidH().get(enthalpyUnit) / mw,
                            P);
                    }
                }

                // This is a ln(P) chart, plot fewer points as P increases.
                P = addP(P, reallySharpAndSlow);
            }
            dataSet.addSeries(densityCurve);
        }

        final JFreeChart chart = ChartFactory.createScatterPlot("" +
            "Pressure-Enthalpy Diagram: " + compound.toString() + " " +
            method.toString(),
            "Ethalpy [" + enthalpyUnitString + "]",
            "Pressure [" + pressureUnit + "]",
            dataSet,
            PlotOrientation.VERTICAL,
            false,      // Legend
            true,       // Tooltips
            false);     // URLs

        final XYPlot plot = chart.getXYPlot();
        final NumberAxis enthalpyAxis = new NumberAxis("Ethalpy [" +
            enthalpyUnitString + "]");
        final NumberAxis pressureAxis = new LogarithmicAxis("Pressure [" +
            pressureUnit + "]");
        enthalpyAxis.setRange(enthalpyMin.get(enthalpyUnit) / mw,
            enthalpyMax.get(enthalpyUnit) / mw);
        pressureAxis.setRange(pressureMin.get(pressureUnit),
            pressureMax.get(pressureUnit));

        plot.setDomainAxis(enthalpyAxis);
        plot.setRangeAxis(pressureAxis);
        chart.setBackgroundPaint(Color.WHITE);
        plot.setOutlinePaint(Color.BLACK);

        // Set markers
        Stroke thickStroke = new BasicStroke(3);
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        Shape criticalPointShape = new Ellipse2D.Double(-5, -5, 10, 10);
        renderer.setSeriesShape(2, criticalPointShape);
        renderer.setSeriesPaint(2, Color.BLACK);
        renderer.setSeriesPaint(0, Color.BLUE);
        renderer.setSeriesLinesVisible(0, true);
        renderer.setSeriesShapesVisible(0, false);
        renderer.setSeriesStroke(0, thickStroke, false);
        renderer.setSeriesPaint(1, Color.RED);
        renderer.setSeriesLinesVisible(1, true);
        renderer.setSeriesShapesVisible(1, false);
        renderer.setSeriesStroke(1, thickStroke, false);

        float [] dashed = new float[2];
        dashed[0] = 2.0f;
        dashed[1] = 2.0f;
        Stroke isothermStroke = new BasicStroke(1.0f,
            BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 1.0f, dashed, 0.0f);
        for (int i = 3; i < isothermSeriesMax; i++) {
            renderer.setSeriesLinesVisible(i, true);
            renderer.setSeriesShapesVisible(i, false);
            renderer.setSeriesPaint(i, Color.RED);
            renderer.setSeriesStroke(i, isothermStroke, false);
        }

        for (int i = isothermSeriesMax; i < densitySeriesMax; i++) {
            renderer.setSeriesLinesVisible(i, true);
            renderer.setSeriesShapesVisible(i, false);
            renderer.setSeriesPaint(i, Color.BLUE);
            // renderer.setSeriesStroke(i, isothermStroke, false);
        }

        plot.setRenderer(renderer);

        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(800, 600));
        setContentPane(chartPanel);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        pack();
        RefineryUtilities.centerFrameOnScreen(this);
        setVisible(true);
    }

}
