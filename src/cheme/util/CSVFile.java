/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

/**
 * This class reads a CSV file.
 */
public class CSVFile {

    /**
     * Data is stored in a list of lists.
     */
    private ArrayList<ArrayList<String>> allTokens;

    /**
     * Get tokens read from the CSV file.
     *
     * @return the tokens
     */
    public ArrayList<ArrayList<String>> getTokens() {
        return allTokens;
    }

    /**
     * Retrieve one row of CSV data as a new set of properties.
     *
     * @param keyName the column name containing the key to select by
     * @param keyValue the key value to select on
     * @return the properties, or null if not found
     */
    public Properties getRow(final String keyName, final String keyValue) {
        if (allTokens.size() < 2) {
            return null;
        }

        ArrayList<String> namesRow = allTokens.get(0);
        int idx = 0;
        boolean found = false;
        for (String token: namesRow) {
            if (token.equals(keyName)) {
                found = true;
                break;
            }
            idx++;
        }
        if (!found) {
            return null;
        }
        for (int row = 2; row < allTokens.size(); row++) {
            ArrayList<String> rowTokens = allTokens.get(row);
            if (rowTokens.get(idx).equals(keyValue)) {
                // System.out.println("rowTokens: " + rowTokens);

                // This is the row we want
                Properties properties = new Properties();
                for (int col = 0; col < namesRow.size(); col++) {
                    properties.put(namesRow.get(col), rowTokens.get(col));
                }
                return properties;
            }
        }
        // We never found a matching row.
        return null;
    }

    /**
     * Retrieve all matching rows of CSV data as a list of sets of
     * properties.
     *
     * @param keyName the column name containing the key to select by
     * @param keyValue the key value to select on
     * @return the properties, or null if not found
     */
    public ArrayList<Properties> getRows(final String keyName,
        final String keyValue) {

        if (allTokens.size() < 2) {
            return null;
        }

        ArrayList<Properties> allRows = new ArrayList<Properties>();
        
        ArrayList<String> namesRow = allTokens.get(0);
        int idx = 0;
        boolean found = false;
        for (String token: namesRow) {
            if (token.equals(keyName)) {
                found = true;
                break;
            }
            idx++;
        }
        if (!found) {
            return null;
        }
        for (int row = 2; row < allTokens.size(); row++) {
            ArrayList<String> rowTokens = allTokens.get(row);
            if (rowTokens.get(idx).equals(keyValue)) {
                // System.out.println("rowTokens: " + rowTokens);

                // This is the row we want
                Properties properties = new Properties();
                for (int col = 0; col < namesRow.size(); col++) {
                    properties.put(namesRow.get(col), rowTokens.get(col));
                }
                allRows.add(properties);
            }
        }
        if (allRows.size() == 0) {
            // We never found a matching row.
            return null;
        }
        return allRows;
    }

    /**
     * Retrieve the names and units of each row of this CSV data.
     *
     * @return the properties, or null if not found
     */
    public Properties getUnits() {
        if (allTokens.size() < 2) {
            return null;
        }
        ArrayList<String> namesRow = allTokens.get(0);
        ArrayList<String> unitsRow = allTokens.get(1);
        Properties properties = new Properties();
        for (int col = 0; col < namesRow.size(); col++) {
            properties.put(namesRow.get(col), unitsRow.get(col));
        }
        return properties;
    }

    /**
     * Read a CSV file from an InputStream.  Blank lines and lines beginning
     * with '#' and ';' are ignored.
     *
     * @param in stream to read from
     * @throws IOException if a java.io operation throws
     */
    public CSVFile(final InputStream in) throws IOException {
        allTokens = new ArrayList<ArrayList<String>>();

        // Read each line, break it into tokens (honoring " as a quote
        // character), and append to allTokens.
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        for (;;) {
            String line = reader.readLine();
            if (line == null) {
                break;
            }
            line = line.trim();
            if (line.length() == 0) {
                // Skip empty lines.
                continue;
            }
            if (line.startsWith(";") || line.startsWith("#")) {
                // Skip comment lines.
                continue;
            }

            // Break the CSV into multiple tokens:
            //
            //   - '\"' is the quote character.
            //   - ',' is the separator.
            StringBuilder token = new StringBuilder();
            boolean inQuote = false;
            ArrayList<String> lineTokens = new ArrayList<String>();
            for (int i = 0; i < line.length(); i++) {
                char ch = line.charAt(i);
                if (ch == '\"') {
                    inQuote = !inQuote;
                    continue;
                }
                if ((ch == ',') && (inQuote == false)) {
                    lineTokens.add(token.toString());
                    token = new StringBuilder();
                    continue;
                }
                token.append(ch);
            }
            if (token.length() > 0) {
                lineTokens.add(token.toString());
            }
            allTokens.add(lineTokens);
        }

        // All done, close the reader.
        reader.close();
    }

}
