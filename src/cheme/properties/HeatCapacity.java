/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.properties;

import java.util.HashSet;
import java.util.Set;

/**
 * This class defines molar heat capacity and provides conversions for
 * numerous unit types.
 */
public final class HeatCapacity implements Property, Comparable<HeatCapacity> {

    /**
     * List the supported unit strings that can be used to construct an
     * instance of this Property.
     */
    static final public Set<String> units;

    /**
     * Static constructor sets global properties.
     */
    static {
        // Supported units.
        units = new HashSet<String>();
        units.add("J/mol-K");
        units.add("kJ/mol-K");
        units.add("BTU/lbmol-R");
    }

    /**
     * The value of this heat capacity property in joules per mol-K.
     */
    private double jmolK;

    /**
     * Return the supported unit strings that can be used to construct an
     * instance of this Property.
     */
    public Set<String> SupportedUnits() {
        return units;
    }

    /**
     * Create a new heat capacity instance using a particular unit.
     *
     * @param value the numeric value
     * @param unit "J/mol-K", "BTU/lbmol-R", etc. as returned by
     * SupportedUnits()
     * @return the heat capacity in the desired units
     */
    public static HeatCapacity at(final double value, final String unit) {
        return new HeatCapacity(value, unit);
    }

    /**
     * Set this instance to a new value using a particular unit.
     *
     * @param value the numeric value
     * @param unit "Pa", "psig", etc. as returned by SupportedUnits()
     */
    private void set(final double value, final String unit) {
        if (unit.equals("J/mol-K")) {
            jmolK = value;
        } else if (unit.equals("kJ/mol-K")) {
            jmolK = value * 1000.0;
        } else if (unit.equals("BTU/lbmol-R")) {
            jmolK = value / (453.593 * 1.8 * 9.4782e-4);
        } else {
            throw new IllegalArgumentException("Invalid heat capacity unit: " +
                unit);
        }
    }

    /**
     * Get this instance's value using a particular unit.
     *
     * @param unit "Pa", "K", etc. as returned by SupportedUnits()
     * @return value the numeric value
     */
    public double get(final String unit) {
        if (unit.equals("J/mol-K")) {
            return jmolK;
        } else if (unit.equals("kJ/mol-K")) {
            return jmolK / 1000.0;
        } else if (unit.equals("BTU/lbmol-R")) {
            return jmolK * (453.593 * 1.8 * 9.4782e-4);
        } else {
            throw new IllegalArgumentException("Invalid heat capacity unit: " +
                unit);
        }
    }

    /**
     * Get this instance's equivalent enthalpy units string.
     *
     * @param unit "Pa", "K", etc. as returned by SupportedUnits()
     * @return "J/mol", "BTU/lbmol", etc. matching those units returned by
     * SupportedUnits()
     */
    public static String enthalpyUnit(final String unit) {
        if (unit.equals("J/mol-K")) {
            return "J/mol";
        } else if (unit.equals("kJ/mol-K")) {
            return "kJ/mol";
        } else if (unit.equals("BTU/lbmol-R")) {
            return "BTU/lbmol";
        } else {
            throw new IllegalArgumentException("Invalid heat capacity unit: " +
                unit);
        }
    }

    /**
     * Create a new heat capacity instance using a particular unit.
     *
     * @param value the numeric value
     * @param unit "J/mol-K", "BTU/lbmol-R", etc. as returned by
     * SupportedUnits()
     */
    public HeatCapacity(final double value, final String unit) {
        set(value, unit);
    }

    /**
     * Convert this heat capacity to J/mol-K.
     *
     * @return the heat capacity in J/mol-K
     */
    public double JmolK() {
        return jmolK;
    }

    /**
     * Convert this heat capacity to kJ/mol-K.
     *
     * @return the heat capacity in kJ/mol-K
     */
    public double kJmolK() {
        return jmolK / 1000.0;
    }

    /**
     * Convert this heat capacity to BTU/lbmol-R.
     *
     * @return the heat capacity in BTU/lbmol-R
     */
    public double BTUlbmolR() {
        return jmolK * (453.593 * 1.8 * 9.4782e-4);
    }

    /**
     * Comparison operator.
     *
     * @param that another HeatCapacity instance
     * @return -1, 0, 1 depending on whether or not this instance is less
     * than, the same, or greater than than that instance
     */
    public final int compareTo(final HeatCapacity that) {
        if (this.jmolK < that.jmolK) {
            return -1;
        } else if (this.jmolK > that.jmolK) {
            return 1;
        }
        return 0;
    }

}
