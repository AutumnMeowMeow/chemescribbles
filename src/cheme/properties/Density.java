/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.properties;

import java.util.HashSet;
import java.util.Set;

/**
 * This class defines molar and mass density and provides conversions for
 * numerous unit types.
 */
public final class Density implements Property, Comparable<Density> {

    /**
     * List the supported unit strings that can be used to construct an
     * instance of this Property.
     */
    static final public Set<String> units;

    /**
     * Static constructor sets global properties.
     */
    static {
        // Supported units.
        units = new HashSet<String>();
        units.add("mol/L");
        units.add("mol/cm3");
        units.add("lbmol/ft3");
        units.add("mol/m3");
        units.add("kmol/m3");
    }

    /**
     * The value of this density property in moles per cubic meters.
     */
    private double mol;

    /**
     * Return the supported unit strings that can be used to construct an
     * instance of this Property.
     */
    public Set<String> SupportedUnits() {
        return units;
    }

    /**
     * Create a new density instance using a particular unit.
     *
     * @param value the numeric value
     * @param unit "m3/mol", "ft3/lbmol", etc. as returned by
     * SupportedUnits()
     * @return the density in the desired units
     */
    public static Density at(final double value, final String unit) {
        return new Density(value, unit);
    }

    /**
     * Set this instance to a new value using a particular unit.
     *
     * @param value the numeric value
     * @param unit "Pa", "psig", etc. as returned by SupportedUnits()
     */
    private void set(final double value, final String unit) {
        if (unit.equals("mol/m3")) {
            mol = value;
        } else if (unit.equals("kmol/m3")) {
            mol = value * 1000.0;
        } else if (unit.equals("lbmol/ft3")) {
            mol = value * (453.593 * 35.3145);
        } else {
            throw new IllegalArgumentException("Invalid density unit: " +
                unit);
        }
    }

    /**
     * Get this instance's value using a particular unit.
     *
     * @param unit "Pa", "K", etc. as returned by SupportedUnits()
     * @return value the numeric value
     */
    public double get(final String unit) {
        if (unit.equals("mol/m3")) {
            return mol;
        } else if (unit.equals("kmol/m3")) {
            return mol / 1000.0;
        } else if (unit.equals("lbmol/ft3")) {
            return mol / (453.593 * 35.3145);
        } else {
            throw new IllegalArgumentException("Invalid density unit: " +
                unit);
        }
    }

    /**
     * Get this instance's equivalent mass-based density units string.
     *
     * @param unit "mol/m3", "lbmol/ft3", etc. as returned by SupportedUnits()
     * @return "g/m3", "lb/ft3", etc. corresponding to similar units
     * returned by SupportedUnits()
     */
    public static String massUnit(final String unit) {
        if (unit.equals("mol/m3")) {
            return "g/m3";
        } else if (unit.equals("kmol/m3")) {
            return "kg/m3";
        } else if (unit.equals("lbmol/ft3")) {
            return "lb/ft3";
        } else {
            throw new IllegalArgumentException("Invalid density unit: " +
                unit);
        }
    }

    /**
     * Convert this instance's density value into a volume value.
     *
     * @return value the density
     */
    public Volume getVolume() {
        return Volume.at(1 / mol, "m3/mol");
    }

    /**
     * Create a new density instance using a particular unit.
     *
     * @param value the numeric value
     * @param unit "m3/mol", "ft3/lbmol", etc. as returned by
     * SupportedUnits()
     */
    public Density(final double value, final String unit) {
        set(value, unit);
    }

    /**
     * Convert this density to mol/m3.
     *
     * @return the density in mol/m3
     */
    public double molm3() {
        return mol;
    }

    /**
     * Comparison operator.
     *
     * @param that another Density instance
     * @return -1, 0, 1 depending on whether or not this instance is less
     * than, the same, or greater than than that instance
     */
    public final int compareTo(final Density that) {
        if (this.mol < that.mol) {
            return -1;
        } else if (this.mol > that.mol) {
            return 1;
        }
        return 0;
    }

}
