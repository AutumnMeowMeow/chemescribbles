/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.properties;

import java.util.HashSet;
import java.util.Set;

/**
 * This class defines pressure and provides conversions for numerous unit
 * types.
 */
public final class Pressure implements Property, Comparable<Pressure> {

    /**
     * List the supported unit strings that can be used to construct an
     * instance of this Property.
     */
    static final public Set<String> units;

    /**
     * Static constructor sets global properties.
     */
    static {
        // Supported units.
        units = new HashSet<String>();
        units.add("atm");
        units.add("bara");
        units.add("barg");
        units.add("Pa");
        units.add("psia");
        units.add("psig");
        units.add("mmHg");
    }

    /**
     * The value of this pressure property in Pascals.
     */
    private double pascals;

    /**
     * Return the supported unit strings that can be used to construct an
     * instance of this Property.
     */
    public Set<String> SupportedUnits() {
        return units;
    }

    /**
     * Create a new pressure instance using a particular unit.
     *
     * @param value the numeric value
     * @param unit "Pa", "psig", etc. as returned by SupportedUnits()
     * @return the pressure in the desired units
     */
    public static Pressure at(final double value, final String unit) {
        return new Pressure(value, unit);
    }

    /**
     * Set this pressure instance to a new value using a particular unit.
     *
     * @param value the numeric value
     * @param unit "Pa", "psig", etc. as returned by SupportedUnits()
     */
    private void set(final double value, final String unit) {
        if (unit.equals("Pa")) {
            pascals = value;
        } else if (unit.equals("psia")) {
            pascals = value * 101325.0 / 14.696;
        } else if (unit.equals("psig")) {
            pascals = (value + 14.696) * 101325.0 / 14.696;
        } else if (unit.equals("bara")) {
            pascals = value * 100000.0;
        } else if (unit.equals("barg")) {
            pascals = (value + 1.0) * 100000.0;
        } else if (unit.equals("atm")) {
            pascals = value * 101325.0;
        } else if (unit.equals("mmHg")) {
            pascals = value * 101325.0 / 760;
        } else {
            throw new IllegalArgumentException("Invalid pressure unit: " +
                unit);
        }
    }

    /**
     * Get this instance's value using a particular unit.
     *
     * @param unit "Pa", "K", etc. as returned by SupportedUnits()
     * @return value the numeric value
     */
    public double get(final String unit) {
        if (unit.equals("Pa")) {
            return pascals;
        } else if (unit.equals("psia")) {
            return pascals / 101325.0 * 14.696;
        } else if (unit.equals("psig")) {
            return pascals / 101325.0 * 14.696 - 14.696;
        } else if (unit.equals("bara")) {
            return pascals / 100000.0;
        } else if (unit.equals("barg")) {
            return pascals / 100000.0 - 1.0;
        } else if (unit.equals("atm")) {
            return pascals / 101325.0;
        } else if (unit.equals("mmHg")) {
            return pascals / 101325.0 * 760;
        } else {
            throw new IllegalArgumentException("Invalid pressure unit: " +
                unit);
        }
    }

    /**
     * Create a new pressure instance using a particular unit.
     *
     * @param value the numeric value
     * @param unit "Pa", "psig", etc. as returned by SupportedUnits()
     */
    public Pressure(final double value, final String unit) {
        set(value, unit);
    }

    /**
     * Convert this pressure to Pascals.
     *
     * @return the pressure in Pascals
     */
    public double Pascals() {
        return pascals;
    }

    /**
     * Convert this pressure to Pascals.
     *
     * @return the pressure in Pascals
     */
    public double Pa() {
        return pascals;
    }

    /**
     * Convert this pressure to psia.
     *
     * @return the pressure in psia
     */
    public double psia() {
        return ((pascals / 101325.0) * 14.696);
    }

    /**
     * Convert this pressure to psig.
     *
     * @return the pressure in psig
     */
    public double psig() {
        return ((pascals / 101325.0) * 14.696) - 14.696;
    }

    /**
     * Convert this pressure to bara.
     *
     * @return the pressure in bara
     */
    public double bara() {
        return pascals / 100000.0;
    }

    /**
     * Convert this pressure to barg.
     *
     * @return the pressure in barg
     */
    public double barg() {
        return (pascals / 100000.0) - 1;
    }

    /**
     * Convert this pressure to atm.
     *
     * @return the pressure in atm
     */
    public double atm() {
        return pascals / 101325.0;
    }

    /**
     * Convert this pressure to mm Hg (torr).
     *
     * @return the pressure in mm Hg
     */
    public double mmHg() {
        return pascals / 101325.0 * 760;
    }

    /**
     * Make human-readable description of this Pressure.
     *
     * @return displayable String
     */
    @Override
    public String toString() {
        return String.format("%f psig", psig());
    }

    /**
     * Comparison operator.
     *
     * @param that another Pressure instance
     * @return -1, 0, 1 depending on whether or not this instance is less
     * than, the same, or greater than than that instance
     */
    public final int compareTo(final Pressure that) {
        if (this.pascals < that.pascals) {
            return -1;
        } else if (this.pascals > that.pascals) {
            return 1;
        }
        return 0;
    }

}
