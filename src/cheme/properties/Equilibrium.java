/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.properties;

import java.util.HashMap;
import java.util.List;

import cheme.compounds.MoleFraction;

/**
 * An Equilibrium contains a collection of thermodynamic properties to
 * represent a single thermodynamic state.  The Integer keys in Equilibrium
 * (KEY_*) can be used to set and retrieve the thermodynamic values
 * supported.  Each key has a convenience method to retrieve it that is just
 * its name in camel case.
 */
public class Equilibrium extends HashMap<Integer, Object> {

    /**
     * HashMap implements the Serializable interface.
     */
    public static final long serialVersionUID                   = 1;

    /**
     * Key: number of iterations required to compute this state.
     */
    public static final Integer KEY_ITER                        = 10;

    /**
     * Get the number of iterations used to compute this state.
     *
     * @return the value, or null if it was never set by a method
     */
    public Integer iter() {
        return (Integer) get(KEY_ITER);
    }

    /**
     * Key: vapor molar volume.
     */
    public static final Integer KEY_VAPOR_V                     = 100;

    /**
     * Get vapor molar volume.
     *
     * @return the value, or null if it was never set by a method
     */
    public Volume VaporV() {
        return (Volume) get(KEY_VAPOR_V);
    }

    /**
     * Key: liquid molar volume.
     */
    public static final Integer KEY_LIQUID_V                    = 101;

    /**
     * Get liquid molar volume.
     *
     * @return the value, or null if it was never set by a method
     */
    public Volume LiquidV() {
        return (Volume) get(KEY_LIQUID_V);
    }

    /**
     * Key: interior unstable vapor/liquid molar volume as computed by a
     * cubic equation of state method.  This value has no accepted physical
     * meaning, but is an artifact of many EOS calculations so is kept for
     * callers to have access to.
     */
    public static final Integer KEY_INTERIOR_CUBIC_EOS_V        = 102;

    /**
     * Get interior unstable vapor/liquid molar volume as computed by a cubic
     * equation of state method.
     *
     * @return the value, or null if it was never set by a method
     */
    public Volume InteriorCubicEOSV() {
        return (Volume) get(KEY_INTERIOR_CUBIC_EOS_V);
    }

    /**
     * Key: saturated pressure.
     */
    public static final Integer KEY_PSAT                        = 103;

    /**
     * Get saturated pressure.
     *
     * @return the value, or null if it was never set by a method
     */
    public Pressure Psat() {
        return (Pressure) get(KEY_PSAT);
    }

    /**
     * Key: saturated temperature.
     */
    public static final Integer KEY_TSAT                        = 104;

    /**
     * Get saturated temperature.
     *
     * @return the value, or null if it was never set by a method
     */
    public Temperature Tsat() {
        return (Temperature) get(KEY_TSAT);
    }

    /**
     * Key: pressure.
     */
    public static final Integer KEY_P                           = 105;

    /**
     * Get pressure.
     *
     * @return the value, or null if it was never set by a method
     */
    public Pressure P() {
        return (Pressure) get(KEY_P);
    }

    /**
     * Key: temperature.
     */
    public static final Integer KEY_T                           = 106;

    /**
     * Get temperature.
     *
     * @return the value, or null if it was never set by a method
     */
    public Temperature T() {
        return (Temperature) get(KEY_T);
    }

    /**
     * Key: phase of this state.
     */
    public static final Integer KEY_PHASE                       = 110;

    /**
     * Get the phase of this equilibrium state.
     *
     * @return the value, or null if it was never set by a method
     */
    public Phase phase() {
        return (Phase) get(KEY_PHASE);
    }

    /**
     * Key: vapor compressibility (dimensionless).
     */
    public static final Integer KEY_VAPOR_Z                     = 111;

    /**
     * Get vapor compressibility.
     *
     * @return the value, or null if it was never set by a method
     */
    public Double VaporZ() {
        return (Double) get(KEY_VAPOR_Z);
    }

    /**
     * Key: liquid molar compressibility (dimensionless).
     */
    public static final Integer KEY_LIQUID_Z                    = 112;

    /**
     * Get liquid compressibility.
     *
     * @return the value, or null if it was never set by a method
     */
    public Double LiquidZ() {
        return (Double) get(KEY_LIQUID_Z);
    }

    /**
     * Key: vapor phase molar residual enthalpy (a.k.a. enthalpy departure).
     */
    public static final Integer KEY_VAPOR_HR                    = 113;

    /**
     * Get vapor phase molar residual enthalpy.
     *
     * @return the value, or null if it was never set by a method
     */
    public Enthalpy VaporHR() {
        return (Enthalpy) get(KEY_VAPOR_HR);
    }

    /**
     * Key: liquid phase molar residual enthalpy (a.k.a. enthalpy departure).
     */
    public static final Integer KEY_LIQUID_HR                   = 114;

    /**
     * Get liquid phase molar residual enthalpy.
     *
     * @return the value, or null if it was never set by a method
     */
    public Enthalpy LiquidHR() {
        return (Enthalpy) get(KEY_LIQUID_HR);
    }

    /**
     * Key: vapor phase molar residual entropy (a.k.a. entropy departure).
     */
    public static final Integer KEY_VAPOR_SR                    = 115;

    /**
     * Get vapor phase molar residual entropy.
     *
     * @return the value, or null if it was never set by a method
     */
    public Entropy VaporSR() {
        return (Entropy) get(KEY_VAPOR_SR);
    }

    /**
     * Key: liquid phase molar residual entropy (a.k.a. entropy departure).
     */
    public static final Integer KEY_LIQUID_SR                   = 116;

    /**
     * Get liquid phase molar residual entropy.
     *
     * @return the value, or null if it was never set by a method
     */
    public Entropy LiquidSR() {
        return (Entropy) get(KEY_LIQUID_SR);
    }

    /**
     * Key: vapor phase molar residual Gibbs (a.k.a. Gibbs departure).
     */
    public static final Integer KEY_VAPOR_GR                    = 117;

    /**
     * Get vapor phase molar residual Gibbs.
     *
     * @return the value, or null if it was never set by a method
     */
    public Gibbs VaporGR() {
        return (Gibbs) get(KEY_VAPOR_GR);
    }

    /**
     * Key: liquid phase molar residual Gibbs (a.k.a. Gibbs departure).
     */
    public static final Integer KEY_LIQUID_GR                   = 118;

    /**
     * Get liquid phase molar residual Gibbs.
     *
     * @return the value, or null if it was never set by a method
     */
    public Gibbs LiquidGR() {
        return (Gibbs) get(KEY_LIQUID_GR);
    }

    /**
     * Key: vapor phase molar real enthalpy
     */
    public static final Integer KEY_VAPOR_H                     = 120;

    /**
     * Get vapor phase molar real enthalpy.
     *
     * @return the value, or null if it was never set by a method
     */
    public Enthalpy VaporH() {
        return (Enthalpy) get(KEY_VAPOR_H);
    }

    /**
     * Key: liquid phase molar real enthalpy.
     */
    public static final Integer KEY_LIQUID_H                    = 121;

    /**
     * Get liquid phase molar real enthalpy.
     *
     * @return the value, or null if it was never set by a method
     */
    public Enthalpy LiquidH() {
        return (Enthalpy) get(KEY_LIQUID_H);
    }

    /**
     * Key: vapor fraction.
     */
    public static final Integer KEY_VFRAC                       = 130;

    /**
     * Get vapor fraction.
     *
     * @return the value, or null if it was never set by a method
     */
    public Double VFrac() {
        return (Double) get(KEY_VFRAC);
    }

    /**
     * Key: vapor phase molar components.
     */
    public static final Integer KEY_Y_I                         = 131;

    /**
     * Get vapor phase molar components.
     *
     * @return the value, or null if it was never set by a method
     */
    public List<MoleFraction> Yi() {
        @SuppressWarnings("unchecked")
        List<MoleFraction> result = (List<MoleFraction>) get(KEY_Y_I);
        return result;
    }

    /**
     * Key: liquid phase molar components.
     */
    public static final Integer KEY_X_I                         = 132;

    /**
     * Get liquid phase molar components.
     *
     * @return the value, or null if it was never set by a method
     */
    public List<MoleFraction> Xi() {
        @SuppressWarnings("unchecked")
        List<MoleFraction> result = (List<MoleFraction>) get(KEY_X_I);
        return result;
    }

    /**
     * Key: total phase molar components.
     */
    public static final Integer KEY_Z_I                         = 133;

    /**
     * Get total phase molar components.
     *
     * @return the value, or null if it was never set by a method
     */
    public List<MoleFraction> Zi() {
        @SuppressWarnings("unchecked")
        List<MoleFraction> result = (List<MoleFraction>) get(KEY_Z_I);
        return result;
    }

}
