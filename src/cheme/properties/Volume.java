/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.properties;

import java.util.HashSet;
import java.util.Set;

/**
 * This class defines molar and mass volume and provides conversions for
 * numerous unit types.
 */
public final class Volume implements Property, Comparable<Volume> {

    /**
     * List the supported unit strings that can be used to construct an
     * instance of this Property.
     */
    static final public Set<String> units;

    /**
     * Static constructor sets global properties.
     */
    static {
        // Supported units.
        units = new HashSet<String>();
        units.add("L/mol");
        units.add("cm3/mol");
        units.add("ft3/lbmol");
        units.add("m3/mol");
        units.add("m3/kmol");
    }

    /**
     * The value of this volume property in cubic meters per mol.
     */
    private double m3;

    /**
     * Return the supported unit strings that can be used to construct an
     * instance of this Property.
     */
    public Set<String> SupportedUnits() {
        return units;
    }

    /**
     * Create a new volume instance using a particular unit.
     *
     * @param value the numeric value
     * @param unit "m3/mol", "ft3/lbmol", etc. as returned by
     * SupportedUnits()
     * @return the Volume in the desired units
     */
    public static Volume at(final double value, final String unit) {
        return new Volume(value, unit);
    }

    /**
     * Set this instance to a new value using a particular unit.
     *
     * @param value the numeric value
     * @param unit "Pa", "psig", etc. as returned by SupportedUnits()
     */
    private void set(final double value, final String unit) {
        if (unit.equals("m3/mol")) {
            m3 = value;
        } else if (unit.equals("m3/kmol")) {
            m3 = value / 1000.0;
        } else if (unit.equals("ft/lbmol")) {
            m3 = value / (453.593 * 35.3145);
        } else {
            throw new IllegalArgumentException("Invalid volume unit: " +
                unit);
        }
    }

    /**
     * Get this instance's value using a particular unit.
     *
     * @param unit "Pa", "K", etc. as returned by SupportedUnits()
     * @return value the numeric value
     */
    public double get(final String unit) {
        if (unit.equals("m3/mol")) {
            return m3;
        } else if (unit.equals("m3/kmol")) {
            return m3 * 1000.0;
        } else if (unit.equals("ft/lbmol")) {
            return m3 * (453.593 * 35.3145);
        } else {
            throw new IllegalArgumentException("Invalid volume unit: " +
                unit);
        }
    }

    /**
     * Create a new volume instance using a particular unit.
     *
     * @param value the numeric value
     * @param unit "m3/mol", "ft3/lbmol", etc. as returned by
     * SupportedUnits()
     */
    public Volume(final double value, final String unit) {
        set(value, unit);
    }

    /**
     * Convert this volume to m3/mol.
     *
     * @return the volume as m3/mol
     */
    public double m3mol() {
        return m3;
    }

    /**
     * Comparison operator.
     *
     * @param that another Volume instance
     * @return -1, 0, 1 depending on whether or not this instance is less
     * than, the same, or greater than than that instance
     */
    public final int compareTo(final Volume that) {
        if (this.m3 < that.m3) {
            return -1;
        } else if (this.m3 > that.m3) {
            return 1;
        }
        return 0;
    }

}
