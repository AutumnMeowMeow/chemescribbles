/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.properties;

import java.util.HashSet;
import java.util.Set;

/**
 * This class defines temperature and provides conversions for Kelvin,
 * Rankine, Celsius, and Fahrenheit.
 */
public final class Temperature implements Property, Comparable<Temperature> {

    /**
     * List the supported unit strings that can be used to construct an
     * instance of this Property.
     */
    static final public Set<String> units;

    /**
     * Static constructor sets global properties.
     */
    static {
        // Supported units.
        units = new HashSet<String>();
        units.add("C");
        units.add("F");
        units.add("K");
        units.add("R");
    }

    /**
     * The value of this temperature property in Kelvin.
     */
    private double kelvin;

    /**
     * Return the supported unit strings that can be used to construct an
     * instance of this Property.
     */
    public Set<String> SupportedUnits() {
        return units;
    }

    /**
     * Create a new temperature instance using a particular unit.
     *
     * @param value the numeric value
     * @param unit "C", "F", "K", or "R" for Celsius, Fahrenheit, Kelvin, or
     * Rankine
     * @return the temperature in the desired units
     */
    public static Temperature at(final double value, final String unit) {
        return new Temperature(value, unit);
    }

    /**
     * Create a new temperature instance using a particular unit.
     *
     * @param value the numeric value
     * @param unit 'C', 'F', 'K', or 'R' for Celsius, Fahrenheit, Kelvin, or
     * Rankine
     * @return the temperature in the desired units
     */
    public static Temperature at(final double value, final char unit) {
        return new Temperature(value, unit);
    }

    /**
     * Set this instance to a new value using a particular unit.
     *
     * @param value the numeric value
     * @param unit "Pa", "psig", etc. as returned by SupportedUnits()
     */
    private void set(final double value, final char unit) {
        switch (unit) {
        case 'C':
            kelvin = value + 273.15;
            break;
        case 'K':
            kelvin = value;
            break;
        case 'R':
            kelvin = value / 1.8;
            break;
        case 'F':
            kelvin = (value + 459.67) / 1.8;
            break;
        default:
            throw new IllegalArgumentException("Invalid temperature unit: " +
                unit);
        }
    }

    /**
     * Get this instance's value using a particular unit.
     *
     * @param unit "Pa", "K", etc. as returned by SupportedUnits()
     * @return value the numeric value
     */
    public double get(final char unit) {
        switch (unit) {
        case 'C':
            return kelvin - 273.15;
        case 'K':
            return kelvin;
        case 'R':
            return kelvin * 1.8;
        case 'F':
            return (kelvin * 1.8) - 459.67;
        default:
            throw new IllegalArgumentException("Invalid temperature unit: " +
                unit);
        }
    }

    /**
     * Get this instance's value using a particular unit.
     *
     * @param unit "Pa", "K", etc. as returned by SupportedUnits()
     * @return value the numeric value
     */
    public double get(final String unit) {
        return get(unit.charAt(0));
    }

    /**
     * Set this instance to a new value using a particular unit.
     *
     * @param value the numeric value
     * @param unit "Pa", "psig", etc. as returned by SupportedUnits()
     */
    public void set(final double value, final String unit) {
        set(value, unit.charAt(0));
    }

    /**
     * Create a new temperature instance using a particular unit.
     *
     * @param value the numeric value
     * @param unit "C", "F", "K", or "R" for Celsius, Fahrenheit, Kelvin, or
     * Rankine
     */
    public Temperature(final double value, final String unit) {
        this(value, unit.charAt(0));
    }

    /**
     * Create a new temperature instance using a particular unit.
     *
     * @param value the numeric value
     * @param unit 'C', 'F', 'K', or 'R' for Celsius, Fahrenheit, Kelvin, or
     * Rankine
     */
    public Temperature(final double value, final char unit) {
        set(value, unit);
    }

    /**
     * Convert this temperature to Kelvin.
     *
     * @return the temperature as Kelvin
     */
    public double Kelvin() {
        return kelvin;
    }

    /**
     * Convert this temperature to Rankine.
     *
     * @return the temperature as Rankine
     */
    public double Rankine() {
        return kelvin * 1.8;
    }

    /**
     * Convert this temperature to Celsius (Centigrade).
     *
     * @return the temperature as Celsius
     */
    public double Celsius() {
        return kelvin - 273.15;
    }

    /**
     * Convert this temperature to Fahrenheit.
     *
     * @return the temperature as Farhrenheit
     */
    public double Fahrenheit() {
        return (kelvin * 1.8) - 459.67;
    }

    /**
     * Convert this temperature to Kelvin.
     *
     * @return the temperature as Kelvin
     */
    public double K() {
        return Kelvin();
    }

    /**
     * Convert this temperature to Rankine.
     *
     * @return the temperature as Rankine
     */
    public double R() {
        return Rankine();
    }

    /**
     * Convert this temperature to Celsius (Centigrade).
     *
     * @return the temperature as Celsius
     */
    public double C() {
        return Celsius();
    }

    /**
     * Convert this temperature to Fahrenheit.
     *
     * @return the temperature as Farhrenheit
     */
    public double F() {
        return Fahrenheit();
    }

    /**
     * Make human-readable description of this Temperature.
     *
     * @return displayable String
     */
    @Override
    public String toString() {
        return String.format("%f C", C());
    }

    /**
     * Comparison operator.
     *
     * @param that another Temperature instance
     * @return -1, 0, 1 depending on whether or not this instance is less
     * than, the same, or greater than than that instance
     */
    public final int compareTo(final Temperature that) {
        if (this.kelvin < that.kelvin) {
            return -1;
        } else if (this.kelvin > that.kelvin) {
            return 1;
        }
        return 0;
    }

}
