/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.properties;

import java.util.HashSet;
import java.util.Set;

/**
 * This class defines molar enthalpy and provides conversions for numerous
 * unit types.
 */
public final class Enthalpy implements Property, Comparable<Enthalpy> {

    /**
     * List the supported unit strings that can be used to construct an
     * instance of this Property.
     */
    static final public Set<String> units;

    /**
     * Static constructor sets global properties.
     */
    static {
        // Supported units.
        units = new HashSet<String>();
        units.add("J/mol");
        units.add("kJ/mol");
        units.add("BTU/lbmol");
    }

    /**
     * The value of this enthalpy property in joules per mol.
     */
    private double jmol;

    /**
     * Return the supported unit strings that can be used to construct an
     * instance of this Property.
     */
    public Set<String> SupportedUnits() {
        return units;
    }

    /**
     * Create a new enthalpy instance using a particular unit.
     *
     * @param value the numeric value
     * @param unit "J/mol", "BTU/lbmol", etc. as returned by SupportedUnits()
     * @return the enthalpy in the desired units
     */
    public static Enthalpy at(final double value, final String unit) {
        return new Enthalpy(value, unit);
    }

    /**
     * Set this instance to a new value using a particular unit.
     *
     * @param value the numeric value
     * @param unit "Pa", "psig", etc. as returned by SupportedUnits()
     */
    private void set(final double value, final String unit) {
        if (unit.equals("J/mol")) {
            jmol = value;
        } else if (unit.equals("kJ/mol")) {
            jmol = value * 1000.0;
        } else if (unit.equals("BTU/lbmol")) {
            jmol = value / (453.593 * 9.4782e-4);
        } else {
            throw new IllegalArgumentException("Invalid enthalpy unit: " +
                unit);
        }
    }

    /**
     * Get this instance's equivalent mass-based enthalpy units string.
     *
     * @param unit "Pa", "K", etc. as returned by SupportedUnits()
     * @return "J/kg", "BTU/lb", etc. corresponding to similar units returned
     * by SupportedUnits()
     */
    public static String massUnit(final String unit) {
        if (unit.equals("J/mol")) {
            return "J/g";
        } else if (unit.equals("kJ/mol")) {
            return "kJ/kg";
        } else if (unit.equals("BTU/lbmol")) {
            return "BTU/lb";
        } else {
            throw new IllegalArgumentException("Invalid enthalpy unit: " +
                unit);
        }
    }

    /**
     * Get this instance's value using a particular unit.
     *
     * @param unit "Pa", "K", etc. as returned by SupportedUnits()
     * @return value the numeric value
     */
    public double get(final String unit) {
        if (unit.equals("J/mol")) {
            return jmol;
        } else if (unit.equals("kJ/mol")) {
            return jmol / 1000.0;
        } else if (unit.equals("BTU/lbmol")) {
            return jmol * (453.593 * 9.4782e-4);
        } else {
            throw new IllegalArgumentException("Invalid enthalpy unit: " +
                unit);
        }
    }

    /**
     * Create a new enthalpy instance using a particular unit.
     *
     * @param value the numeric value
     * @param unit "J/mol", "BTU/lbmol", etc. as returned by SupportedUnits()
     */
    public Enthalpy(final double value, final String unit) {
        set(value, unit);
    }

    /**
     * Convert this enthalpy to J/mol.
     *
     * @return the enthalpy in J/mol
     */
    public double Jmol() {
        return jmol;
    }

    /**
     * Convert this enthalpy to kJ/mol.
     *
     * @return the enthalpy in kJ/mol
     */
    public double kJmol() {
        return jmol / 1000.0;
    }

    /**
     * Convert this enthalpy to BTU/lbmol.
     *
     * @return the enthalpy in BTU/lbmol
     */
    public double BTUlbmol() {
        return jmol * (453.593 * 9.4782e-4);
    }

    /**
     * Make human-readable description of this Enthalpy.
     *
     * @return displayable String
     */
    @Override
    public String toString() {
        return String.format("%f J/mol", Jmol());
    }

    /**
     * Comparison operator.
     *
     * @param that another Enthalpy instance
     * @return -1, 0, 1 depending on whether or not this instance is less
     * than, the same, or greater than than that instance
     */
    public final int compareTo(final Enthalpy that) {
        if (this.jmol < that.jmol) {
            return -1;
        } else if (this.jmol > that.jmol) {
            return 1;
        }
        return 0;
    }

}
