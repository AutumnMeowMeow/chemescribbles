/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.properties;

import java.util.HashSet;
import java.util.Set;

/**
 * This class defines molar and mass compositions and provides conversions
 * for numerous unit types.
 */
public final class Composition implements Property {

    /**
     * List the supported unit strings that can be used to construct an
     * instance of this Property.
     */
    static final public Set<String> units;

    /**
     * Static constructor sets global properties.
     */
    static {
        // Supported units.
        units = new HashSet<String>();
        units.add("molfrac");
        units.add("massfrac");
    }

    /**
     * Return the supported unit strings that can be used to construct an
     * instance of this Property.
     */
    public Set<String> SupportedUnits() {
        return units;
    }

    /**
     * Create a new composition instance using a particular unit.
     *
     * @param value the numeric value
     * @param unit "molfrac", "massfrac", etc. as returned by
     * SupportedUnits()
     * @return the composition in the desired units
     */
    public static Composition at(final double value, final String unit) {
        return new Composition(value, unit);
    }

    /**
     * Get this instance's value using a particular unit.
     *
     * @param unit "molfrac", "massfrac", etc. as returned by
     * SupportedUnits()
     * @return value the numeric value
     */
    public double get(final String unit) {
        if (unit.equals("molfrac")) {
            // TODO
            return -1;
        } else if (unit.equals("massfrac")) {
            // TODO
            return -1;
        } else {
            throw new IllegalArgumentException("Invalid composition unit: " +
                unit);
        }
    }

    /**
     * Create a new composition instance using a particular unit.
     *
     * @param value the numeric value
     * @param unit "molfrac", "massfrac", etc. as returned by
     * SupportedUnits()
     */
    public Composition(final double value, final String unit) {
        if (unit.equals("molfrac")) {
            // TODO
        } else if (unit.equals("massfrac")) {
            // TODO
        } else {
            throw new IllegalArgumentException("Invalid composition unit: " +
                unit);
        }
    }

}
