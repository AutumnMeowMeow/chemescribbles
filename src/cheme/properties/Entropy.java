/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.properties;

import java.util.HashSet;
import java.util.Set;

/**
 * This class defines molar entropy and provides conversions for numerous
 * unit types.
 */
public final class Entropy implements Property, Comparable<Entropy> {

    /**
     * List the supported unit strings that can be used to construct an
     * instance of this Property.
     */
    static final public Set<String> units;

    /**
     * Static constructor sets global properties.
     */
    static {
        // Supported units.
        units = new HashSet<String>();
        units.add("J/mol-K");
        units.add("kJ/mol-K");
        units.add("BTU/lbmol-R");
    }

    /**
     * The value of this entropy property in joules per mol-K.
     */
    private double jmolK;

    /**
     * Return the supported unit strings that can be used to construct an
     * instance of this Property.
     */
    public Set<String> SupportedUnits() {
        return units;
    }

    /**
     * Create a new entropy instance using a particular unit.
     *
     * @param value the numeric value
     * @param unit "J/mol-K", "BTU/lbmol-R", etc. as returned by
     * SupportedUnits()
     * @return the entropy in the desired units
     */
    public static Entropy at(final double value, final String unit) {
        return new Entropy(value, unit);
    }

    /**
     * Set this instance to a new value using a particular unit.
     *
     * @param value the numeric value
     * @param unit "Pa", "psig", etc. as returned by SupportedUnits()
     */
    private void set(final double value, final String unit) {
        if (unit.equals("J/mol-K")) {
            jmolK = value;
        } else if (unit.equals("kJ/mol-K")) {
            jmolK = value * 1000.0;
        } else if (unit.equals("BTU/lbmol-R")) {
            jmolK = value / (453.593 * 1.8 * 9.4782e-4);
        } else {
            throw new IllegalArgumentException("Invalid entropy unit: " +
                unit);
        }
    }

    /**
     * Get this instance's value using a particular unit.
     *
     * @param unit "Pa", "K", etc. as returned by SupportedUnits()
     * @return value the numeric value
     */
    public double get(final String unit) {
        if (unit.equals("J/mol-K")) {
            return jmolK;
        } else if (unit.equals("kJ/mol-K")) {
            return jmolK / 1000.0;
        } else if (unit.equals("BTU/lbmol-R")) {
            return jmolK * (453.593 * 1.8 * 9.4782e-4);
        } else {
            throw new IllegalArgumentException("Invalid entropy unit: " +
                unit);
        }
    }

    /**
     * Create a new entropy instance using a particular unit.
     *
     * @param value the numeric value
     * @param unit "J/mol-K", "BTU/lbmol-R", etc. as returned by
     * SupportedUnits()
     */
    public Entropy(final double value, final String unit) {
        set(value, unit);
    }

    /**
     * Convert this entropy to J/mol-K.
     *
     * @return the entropy in J/mol-K
     */
    public double JmolK() {
        return jmolK;
    }

    /**
     * Convert this entropy to kJ/mol-K.
     *
     * @return the entropy in kJ/mol-K
     */
    public double kJmolK() {
        return jmolK / 1000.0;
    }

    /**
     * Convert this entropy to BTU/lbmol-R.
     *
     * @return the entropy in BTU/lbmol-R
     */
    public double BTUlbmolR() {
        return jmolK * (453.593 * 1.8 * 9.4782e-4);
    }

    /**
     * Make human-readable description of this Entropy.
     *
     * @return displayable String
     */
    @Override
    public String toString() {
        return String.format("%f J/mol-K", JmolK());
    }

    /**
     * Comparison operator.
     *
     * @param that another Entropy instance
     * @return -1, 0, 1 depending on whether or not this instance is less
     * than, the same, or greater than than that instance
     */
    public final int compareTo(final Entropy that) {
        if (this.jmolK < that.jmolK) {
            return -1;
        } else if (this.jmolK > that.jmolK) {
            return 1;
        }
        return 0;
    }

}
