/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.methods;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.analysis.solvers.LaguerreSolver;
import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.linear.BlockRealMatrix;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.logging.log4j.Logger;

import cheme.compounds.Compound;
import cheme.compounds.Mixture;
import cheme.compounds.MoleFraction;
import cheme.properties.Constants;
import cheme.properties.Enthalpy;
import cheme.properties.Entropy;
import cheme.properties.Equilibrium;
import cheme.properties.Gibbs;
import cheme.properties.Phase;
import cheme.properties.Pressure;
import cheme.properties.Temperature;
import cheme.properties.Volume;

/**
 * This class performs various thermodynamic calculations via a generalized
 * cubic equation of state method.  NOTE THAT THE PUBLIC FUNCTIONS ARE NOT
 * RE-ENTRANT.
 *
 * These parameters follow the derivation in section 3.5 of _Introduction to
 * Chemical Engineering Thermodynamics_ 6th Ed, by Smith, van Ness, and
 * Abbott.
 */
public abstract class CubicEOS implements Method, ActivityModel {

    /**
     * The Logger to emit to.
     */
    protected static Logger log = null;

    /**
     * If true, allow tracing to the Logger.
     */
    private static final boolean DEBUG = false;

    // Constants used by the method
    private static final double R = Constants.R;

    // These are set by the implementing classes.
    protected double omega;
    protected double epsilon;
    protected double sigma;
    protected double psi;

    // Objects used by the methods
    private LaguerreSolver solver = new LaguerreSolver();

    /**
     * Convergence criteria.
     */
    private Convergence convergence = new Convergence();

    /**
     * Compute the alpha(Tr, w) function.
     *
     * @param Tr the reduced temperature
     * @param w the acentric factor
     * @return alpha
     */
    protected abstract double alpha(final double Tr, final double w);

    /**
     * Compute the d(ln(alpha(Tr, w)))/d(Tr) function.
     *
     * @param Tr the reduced temperature
     * @param w the acentric factor
     * @return alpha
     */
    protected abstract double dLnAlpha(final double Tr, final double w);

    /**
     * Compute the critical volume.
     *
     * @param compound the compound
     * @throws ThermoStateException if this state is not physically realizable
     */
    private double criticalVolume(final Compound compound)
        throws ThermoStateException {

        return criticalVolume(compound.Tc().K(), compound.Pc().Pa(),
            compound.omega());
    }

    /**
     * Compute the critical volume.
     *
     * @param Tc the critical temperature in K
     * @param Pc the critical pressure in Pa
     * @param w the acentric factor
     * @throws ThermoStateException if this state is not physically realizable
     */
    private double criticalVolume(final double Tc, final double Pc,
        final double w) throws ThermoStateException {

        // Compute the volume roots at Pc
        double alpha = 1;
        double a = (psi * alpha * Math.pow(R, 2) * Math.pow(Tc, 2) / Pc);
        double b = (omega * R * Tc / Pc);

        /*
         * The cubic equation to solve is:
         *
         * -P * V^3 +
         * (P*b-P*sigma*b-P*epsilon*b+R*T) * V^2 +
         * (R*T*epsilon*b - a(T) + R*T*sigma*b +
         *     P*b^2*sigma + P*epsilon*b^2 - P*epsilon*b^2*sigma) * V +
         * P*epsilon*b^3*sigma+a(T)*b+R*T*epsilon*b^2*sigma
         *    = 0
         *
         * We convert this to a*V^3 + b*V^2 + c*V + d = 0 and solve.
         */
        double[] cubic = new double[4];
        cubic[3] = -Pc;

        cubic[2] = Pc * b - Pc * sigma * b -
                Pc * epsilon * b + R * Tc;

        cubic[1] = R * Tc * epsilon * b - a +
                R * Tc * sigma * b +
                Pc * Math.pow(b, 2) * sigma +
                Pc * epsilon * Math.pow(b, 2) -
                Pc * epsilon * Math.pow(b, 2) * sigma;

        cubic[0] = Pc * epsilon * Math.pow(b, 3) * sigma +
                a * b + R * Tc * epsilon * Math.pow(b, 2) * sigma;

        if (DEBUG) {
            log.trace("A: " + cubic[3] + " B: " + cubic[2] +
                " C: " + cubic[1] + " D: " + cubic[0]);
        }

        double vStart = R * Tc / Pc;
        Complex [] roots = solver.solveAllComplex(cubic, vStart);

        for (int i = 0; i < roots.length; i++) {
            if (DEBUG) {
                log.trace("V (search): " + roots[i]);
            }

            if (!convergence.isZero(roots[i].getImaginary())) {
                // Complex root, skip this one.
                continue;
            }
            // We found a real root, return it.
            return roots[i].getReal();
        }

        // Did not find a real root, this is an error
        throw new ThermoStateException("No real volume root found");
    }

    /**
     * Compute and fill in the liquid equilibrium values.
     *
     * @param result the Equilibrium to add these values to
     * @param compound pure compound
     * @param p the pressure in Pa
     * @param t the temperature in K
     * @param Vl the liquid volume in m3/mol
     * @param a the a(T) value calculated for this EOS
     * @param b the b(Pc, Tc) value calculated for this EOS
     * @param alpha the alpha(w, T) value calculated for this EOS
     * @param Tr the reduced temperature
     * @param w the compound's acentric factor
     * @throws ThermoStateException if this state is not physically realizable
     */
    private void addLiquidValues(final Equilibrium result,
        final Compound compound, final double p, final double t,
        final double Vl, final double a, final double b, final double alpha,
        final double Tr, final double w) throws ThermoStateException {

        // Get the liquid properties
        double Zl = Vl * p / (R * t);
        double beta = b * p / (R * t);
        double Il = 1.0 / (sigma - epsilon) * Math.log((Zl + sigma * beta) /
            (Zl + epsilon * beta));
        double q = a / (b * R * t);
        double fugacity_l = Math.exp(Zl - 1 - Math.log(Zl - beta) - q * Il);

        if (DEBUG) {
            log.trace(String.format("Vl: %f", Vl));
            log.trace(String.format("Zl: %f", Zl));
            log.trace(String.format("beta: %f", beta));
            log.trace(String.format("Il: %f", Il));
            log.trace(String.format("q: %f", q));
            log.debug(String.format("fugacity liq: %f", fugacity_l));
        }

        result.put(Equilibrium.KEY_LIQUID_V, Volume.at(Vl, "m3/mol"));
        result.put(Equilibrium.KEY_LIQUID_Z, Zl);

        double GRl = R * t * (Zl - 1 - Math.log(Zl - beta) - q * Il);
        result.put(Equilibrium.KEY_LIQUID_GR, Gibbs.at(GRl, "J/mol"));

        double d_ln_alpha = dLnAlpha(Tr, w);

        double HRl = R * t * (Zl - 1 + q * Il * (d_ln_alpha - 1));
        result.put(Equilibrium.KEY_LIQUID_HR, Enthalpy.at(HRl, "J/mol"));

        double SRl = R * (Math.log(Zl - beta) + q * Il * d_ln_alpha);
        result.put(Equilibrium.KEY_LIQUID_SR, Entropy.at(SRl, "J/mol-K"));

        if (compound.Cp_IG_Function() != null) {
            // Compound has a heat capacity function, so go ahead and
            // put the real enthalpies in.
            double H_0 = compound.Hf_IG().Jmol();
            double H_v = compound.Cp_IG_Function().integrate(Constants.STP_T,
                Temperature.at(t, "K")).Jmol();

            result.put(Equilibrium.KEY_LIQUID_H,
                Enthalpy.at(H_0 + H_v + HRl, "J/mol"));
        }
    }

    /**
     * Compute and fill in the vapor equilibrium values.
     *
     * @param result the Equilibrium to add these values to
     * @param compound pure compound
     * @param p the pressure in Pa
     * @param t the temperature in K
     * @param Vv the vapor volume in m3/mol
     * @param a the a(T) value calculated for this EOS
     * @param b the b(Pc, Tc) value calculated for this EOS
     * @param alpha the alpha(w, T) value calculated for this EOS
     * @param Tr the reduced temperature
     * @param w the compound's acentric factor
     * @throws ThermoStateException if this state is not physically realizable
     */
    private void addVaporValues(final Equilibrium result,
        final Compound compound, final double p, final double t,
        final double Vv, final double a, final double b, final double alpha,
        final double Tr, final double w) throws ThermoStateException {

        // Get the vapor properties
        double Zv = Vv * p / (R * t);
        double beta = b * p / (R * t);
        double Iv = 1.0 / (sigma - epsilon) * Math.log((Zv + sigma * beta) /
            (Zv + epsilon * beta));
        double q = a / (b * R * t);
        double fugacity_v = Math.exp(Zv - 1 - Math.log(Zv - beta) - q * Iv);

        if (DEBUG) {
            log.trace(String.format("Vv: %f", Vv));
            log.trace(String.format("Zv: %f", Zv));
            log.trace(String.format("beta: %f", beta));
            log.trace(String.format("Iv: %f", Iv));
            log.trace(String.format("q: %f", q));
            log.debug(String.format("fugacity vapor: %f", fugacity_v));
        }

        // Save and return the results. ---------------------------------------

        result.put(Equilibrium.KEY_VAPOR_V, Volume.at(Vv, "m3/mol"));
        result.put(Equilibrium.KEY_VAPOR_Z, Zv);

        double GRv = R * t * (Zv - 1 - Math.log(Zv - beta) - q * Iv);
        result.put(Equilibrium.KEY_VAPOR_GR, Gibbs.at(GRv, "J/mol"));

        double d_ln_alpha = dLnAlpha(Tr, w);

        double HRv = R * t * (Zv - 1 + q * Iv * (d_ln_alpha - 1));
        result.put(Equilibrium.KEY_VAPOR_HR, Enthalpy.at(HRv, "J/mol"));

        double SRv = R * (Math.log(Zv - beta) + q * Iv * d_ln_alpha);
        result.put(Equilibrium.KEY_VAPOR_SR, Entropy.at(SRv, "J/mol-K"));

        if (compound.Cp_IG_Function() != null) {
            // Compound has a heat capacity function, so go ahead and
            // put the real enthalpies in.
            double H_0 = compound.Hf_IG().Jmol();
            double H_v = compound.Cp_IG_Function().integrate(Constants.STP_T,
                Temperature.at(t, "K")).Jmol();

            result.put(Equilibrium.KEY_VAPOR_H,
                Enthalpy.at(H_0 + H_v + HRv, "J/mol"));
        }
    }

    // Mixture state ----------------------------------------------------------

    /**
     * Determine the equilibrium properties of a mixture (vapor or liquid) at
     * a specific temperature and pressure.
     *
     * @param mixture the mixture
     * @param T temperature
     * @param P pressure
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium state(final Mixture mixture, final Temperature T,
        final Pressure P) throws ThermoStateException {

        /*
         * This problem is: find the vapor and liquid compositions, and vapor
         * fraction, for a mixture at specified total molar composition and
         * system pressure and temperature.
         *
         * The traditional way to solve this is to iterate on K values and
         * compositions.  But I prefer (for now) to go to the fundamental
         * relations instead:
         *
         *    gamma_i * x_i * P_i^{sat} = phi_i * y_i * P
         *    P_i^{sat} = f(T)
         *    x_i * L + y_i * V = z_i * (L + V)
         *    V = 1 - L
         *    \Sum x_i = 1
         *    \Sum y_i = 1
         *
         * These can all be represented as a simple linear algebra problem.
         * Assuming P and T are valid (e.g. this is a true multi-phase
         * system), then the trick is figuring out L to make the following
         * matrices work.
         *
         * Given N components, we will construct a 2N x 2N matrix A and 2N
         * column vectors x and b to solve the system A*x = b.
         *
         * Matrix A looks like:
         *
         * | g1*P_1^{sat}   0         ...    0        p1*(-P)    0  ...  0  |
         * |   0        g2*P_2^{sat}  ...    0         0   p2*(-P)  ...  0  |
         * |   :         :          :     :                 :    :   :   :  |
         * |   0         0          0   gN*P_N^{sat}   0    0   ... pN*(-P) |
         * |   L         0            ...    0        1-L   0   ...  0      |
         * |   0         L            ...    0         0   1-L  ...  0      |
         * |   :         :             :     0         :    :    :   :      |
         * |   0         0            ...    L         0    0   ... 1-L     |
         *
         *
         * Column x looks like:
         *
         * | x_1 |
         * | x_2 |
         * |  :  |
         * | x_n |
         * | y_1 |
         * | y_2 |
         * |  :  |
         * | y_n |
         *
         * Column b looks like:
         *
         * |  0 |
         * |  0 |
         * |  : |
         * |  0 |
         * | z_1 |
         * | z_2 |
         * |  :  |
         * | z_n |
         *
         *
         * The computation below is HIDEOUSLY EXPENSIVE.  But it works!  We
         * can iterate to find L by:
         *
         *     Invert A --> Ainv
         *     Find "x" by way of (Ainv . b)
         *     Renormalize this "x" --> xPrime
         *     Find "b" by way of (A . xPrime)
         *
         *     Sum the top half of the relations (Raoult's Law relations):
         *        - If the sum is negative, then the y_i's are dominating -->
         *          increase L.
         *        - If the sum is positive, then the x_i's are dominating -->
         *          decrease L.
         *     Once L is found, x_i and y_i are already known!
         *
         * I believe this matrix multiplication stuff is essentially the same
         * math as Smith, van Ness, and Abbott.  But now I can understand the
         * calculation path.
         *
         * And BTW hideously expensive is still solving a two-component flash
         * in ~5 milliseconds.
         */

        int N = mixture.size();

        // Perform a search on L to make the material balance solvable.
        // Start at 50/50.
        double L = 0.5;
        int iter = 0;
        int maxIter = 100;
        boolean wentUp = true;
        double delta = 0.1;

        RealVector xResult = null;
        Mixture liquidPhase = null;
        Mixture vaporPhase = null;

        for (;;) {
            if (DEBUG) {
                log.trace("ENTER iter " + iter + " L " + L +
                    " delta: " + delta);
            }

            BlockRealMatrix A = new BlockRealMatrix(2 * N, 2 * N);
            BlockRealMatrix b = new BlockRealMatrix(2 * N, 1);

            int i = 0;
            List<Double> gammas = null;
            if (liquidPhase != null) {
                gammas = gamma(liquidPhase, T, P);
            } else {
                gammas = new ArrayList<Double>(N);
                for (int j = 0; j < N; j++) {
                    gammas.set(j, 1.0);
                }
            }
            List<Double> phis = null;
            if (vaporPhase != null) {
                phis = phi(vaporPhase, T, P);
            } else {
                phis = new ArrayList<Double>(N);
                for (int j = 0; j < N; j++) {
                    phis.set(j, 1.0);
                }
            }
            for (MoleFraction component: mixture.getComponents()) {
                double Psat = component.compound.PsatFunction().eval(T).Pa();
                A.setEntry(i, i, Psat * gammas.get(i));
                A.setEntry(i, i + N, -P.Pa() * phis.get(i));
                A.setEntry(i + N, i, L);
                A.setEntry(i + N, i + N, 1.0 - L);
                b.setEntry(i + N, 0, component.fraction);
                i++;
            }

            if (DEBUG) {
                log.trace("A: " + A);
                log.trace("b: " + b);
            }

            RealMatrix Ainv = MatrixUtils.inverse(A);
            RealMatrix xPrime = Ainv.multiply(b);

            // Renormalize x and y
            double totalX = 0.0;
            double totalY = 0.0;
            for (i = 0; i < N; i++) {
                totalX += xPrime.getEntry(i, 0);
                totalY += xPrime.getEntry(i + N, 0);
            }
            for (i = 0; i < N; i++) {
                xPrime.setEntry(i, 0, xPrime.getEntry(i, 0) / totalX);
                xPrime.setEntry(i + N, 0, xPrime.getEntry(i + N, 0) / totalY);
            }

            // Now multiply (A . (Ainverse . xPrime)).  If the top half rows
            // are negative, we need more L.
            RealMatrix bPrime = A.multiply(xPrime);

            double difference = 0.0;
            for (i = 0; i < N; i++) {
                difference += bPrime.getEntry(i, 0);
            }
            if (DEBUG) {
                log.trace("xPrime: " + xPrime);
                log.trace("bPrime: " + bPrime);
                log.trace("difference: " + difference);
            }

            if (convergence.isZero(difference) && (liquidPhase != null)) {
                // If we nailed it, awesome
                if (DEBUG) {
                    log.trace("CONVERGED");
                }

                // Now save xPrime as our answer.
                xResult = xPrime.getColumnVector(0);
                break;
            }

            // TODO: set liquid and vapor phase compositions
            
            if (difference < 0) {
                // To make the pressure balance work, we don't have enough
                // liquid in the final equilibrium.  Increase L.
                if (wentUp == false) {
                    delta /= 2;
                    wentUp = true;
                }
                while (L + delta > 1.0) {
                    delta /= 2;
                }
                L += delta;
            } else {
                // The opposite of above.
                if (wentUp == true) {
                    delta /= 2;
                    wentUp = false;
                }
                while (L - delta < 0.0) {
                    delta /= 2;
                }
                L -= delta;
            }

            // See if we are out of iterations
            if (iter == maxIter) {
                log.warn("state(mixture, T, P): giving up after " + iter +
                    " iterations: " + L + " T " + T + " xPrime " + xPrime +
                    " bPrime " + bPrime);
                break;
            }
            iter++;

        } // for (;;)

        // Save and return the results. ---------------------------------------
        Equilibrium result = new Equilibrium();

        result.put(Equilibrium.KEY_P, P);
        result.put(Equilibrium.KEY_T, T);
        result.put(Equilibrium.KEY_ITER, iter);

        if (iter != maxIter) {
            if (DEBUG) {
                log.debug("L found after " + iter + " iterations: " + L);
                log.debug("x result: " + xResult);
            }
        } else {
            if (L < convergence.getRelativeDifference()) {
                // L < 0: this wants to be a vapor
                result.put(Equilibrium.KEY_VFRAC, 1.0);
                result.put(Equilibrium.KEY_PHASE, Phase.VAPOR_MIXTURE);
                result.put(Equilibrium.KEY_Y_I, mixture.getComponents());
                return result;
            }
            if ((1.0 - L) < convergence.getRelativeDifference()) {
                // L > 1: this wants to be a liquid
                result.put(Equilibrium.KEY_VFRAC, 0.0);
                result.put(Equilibrium.KEY_PHASE, Phase.LIQUID_MIXTURE);
                result.put(Equilibrium.KEY_X_I, mixture.getComponents());
                return result;
            }
            throw new ThermoStateException("Insufficient iterations to " +
                "find solution within convergence tolerance");
        }

        // Valid vapor/liquid mixture composition
        result.put(Equilibrium.KEY_VFRAC, 1.0 - L);
        result.put(Equilibrium.KEY_PHASE, Phase.VLE_MIXTURE);

        List<MoleFraction> yI = new ArrayList<MoleFraction>(mixture.size());
        List<MoleFraction> xI = new ArrayList<MoleFraction>(mixture.size());

        int i = 0;
        for (MoleFraction component: mixture.getComponents()) {
            MoleFraction x = new MoleFraction(component.compound,
                xResult.getEntry(i));
            MoleFraction y = new MoleFraction(component.compound,
                xResult.getEntry(i + N));
            xI.add(x);
            yI.add(y);
            i++;
        }

        result.put(Equilibrium.KEY_X_I, xI);
        result.put(Equilibrium.KEY_Y_I, yI);
        result.put(Equilibrium.KEY_Z_I, mixture.getComponents());
        return result;
    }

    /**
     * Determine the equilibrium properties of a mixture (vapor or liquid) at
     * a specific temperature and volume.
     *
     * @param mixture the mixture
     * @param T temperature
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium state(final Mixture mixture, final Temperature T,
        final Volume V) throws ThermoStateException {

        // TODO
        return null;
    }

    /**
     * Determine the equilibrium properties of a mixture (vapor or liquid) at
     * a specific pressure and volume.
     *
     * @param mixture the mixture
     * @param P pressure
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium state(final Mixture mixture, final Pressure P,
        final Volume V) throws ThermoStateException {

        // TODO
        return null;
    }

    // General state ----------------------------------------------------------

    /**
     * Determine the equilibrium properties of a pure compound (vapor or
     * liquid) at a specific temperature and pressure.
     *
     * @param compound pure compound
     * @param T temperature
     * @param P pressure
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium state(final Compound compound, final Temperature T,
        final Pressure P) throws ThermoStateException {

        log.entry(compound, T, P);

        double Tc = compound.Tc().K();
        double Pc = compound.Pc().Pa();
        double t = T.K();
        double p = P.Pa();
        if ((p > Pc) && (t > Tc)) {
            // This is a supercritical fluid.  Return the vapor() case.
            // log.info("1 vapor");
            return vapor(compound, T, P);
        }
        if (p > Pc) {
            // This is supercritical pressure but subcritical temperature.
            // Return the liquid() case.
            // log.info("2 liquid");
            return liquid(compound, T, P);
        }
        if (t > Tc) {
            // This is supercritical temperature but subcritical pressure.
            // Return the vapor() case.
            // log.info("3 vapor");
            return vapor(compound, T, P);
        }

        // At this point we might be in VLE.  Get the saturated pressure for
        // this temperature.  If the provided pressure (P) is below the
        // saturated pressure (Psat) then return vapor(), if it is above Psat
        // return liquid(), otherwise the caller nailed it and we can return
        // the result of vle().
        Equilibrium result = vle(compound, T);
        Pressure Psat = result.Psat();
        if (p < Psat.Pa()) {
            // log.info("4 vapor: " + p + " " + Psat.Pa());
            return vapor(compound, T, P);
        } else if (p > Psat.Pa()) {
            // log.info("5 liquid: " + p + " " + Psat.Pa());
            return liquid(compound, T, P);
        }
        return result;
    }

    /**
     * Determine the equilibrium properties of a pure compound (vapor or
     * liquid) at a specific temperature and volume.
     *
     * @param compound pure compound
     * @param T temperature
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium state(final Compound compound, final Temperature T,
        final Volume V) throws ThermoStateException {

        log.entry(compound, T, V);

        double v = V.m3mol();

        if (T.K() < compound.Tc().K()) {
            // This might be VLE.  Get the saturated properties to see.
            Equilibrium sat = vle(compound, T);
            if ((sat.LiquidV() != null) && (sat.VaporV() != null)) {
                double Vl = sat.LiquidV().m3mol();
                double Vv = sat.VaporV().m3mol();
                if ((v > Vl) && (v < Vv)) {
                    // We are in VLE.  What this means is that we have X
                    // moles of liquid at a higher density (Vl < V), and Y
                    // moles of vapor at a lower density (Vv > V):
                    //
                    // V = X*Vl + Y*Vv
                    // X + Y = 1
                    // V = X*Vl + (1 - X)*Vv
                    //   = X*Vl + Vv - X*Vv
                    //   = X*(Vl - Vv) + Vv
                    // X = (V - Vv) / (Vl - Vv)
                    sat.put(Equilibrium.KEY_VFRAC,
                        (v - Vv) / (Vl - Vv));
                    return sat;
                }
            }
        }

        // We are not in VLE.  Return either a liquid or vapor state.
        double Vc = criticalVolume(compound);
        if (v > Vc) {
            // This is a vapor-like density.  Return the vapor() case.
            return vapor(compound, T, V);
        } else {
            // This is a liquid-like density.  Return the liquid() case.

            // NOTE THAT STATE(T, V) MIGHT RETURN A LIQUID IN THE SAME
            // SUPERCRITICAL PRESSURE REGIME THAT STATE(T, P) RETURNS A
            // VAPOR.
            return liquid(compound, T, V);
        }
    }

    /**
     * Determine the equilibrium properties of a pure compound (vapor or
     * liquid) at a specific pressure and volume.
     *
     * @param compound pure compound
     * @param P pressure
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium state(final Compound compound, final Pressure P,
        final Volume V) throws ThermoStateException {

        log.entry(compound, P, V);

        double v = V.m3mol();

        if (P.Pa() < compound.Pc().Pa()) {
            // This might be VLE.  Get the saturated properties to see.
            Equilibrium sat = vle(compound, P);
            if ((sat.LiquidV() != null) && (sat.VaporV() != null)) {
                double Vl = sat.LiquidV().m3mol();
                double Vv = sat.VaporV().m3mol();
                if ((v > Vl) && (v < Vv)) {
                    // We are in VLE.  What this means is that we have X
                    // moles of liquid at a higher density (Vl < V), and Y
                    // moles of vapor at a lower density (Vv > V):
                    //
                    // V = X*Vl + Y*Vv
                    // X + Y = 1
                    // V = X*Vl + (1 - X)*Vv
                    //   = X*Vl + Vv - X*Vv
                    //   = X*(Vl - Vv) + Vv
                    // X = (V - Vv) / (Vl - Vv)
                    sat.put(Equilibrium.KEY_VFRAC,
                        (v - Vv) / (Vl - Vv));
                    return sat;
                }
            }
        }

        double Vc = criticalVolume(compound);
        if (v > Vc) {
            // This is a vapor-like density.  Return the vapor() case.
            return vapor(compound, P, V);
        } else {
            // This is a liquid-like density.  Return the liquid() case.
            //
            // NOTE THAT STATE(P, V) MIGHT RETURN A LIQUID IN THE SAME
            // SUPERCRITICAL PRESSURE REGIME THAT STATE(T, P) RETURNS A
            // VAPOR.
            return liquid(compound, P, V);
        }
    }

    // Vapor ------------------------------------------------------------------

    /**
     * Determine the equilibrium properties of a pure compound (vapor phase)
     * at a specific temperature and pressure.
     *
     * @param compound pure compound
     * @param T temperature
     * @param P pressure
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium vapor(final Compound compound, final Temperature T,
        final Pressure P) throws ThermoStateException {

        log.entry(compound, T, P);

        double t = T.K();
        double Tc = compound.Tc().K();
        double Pc = compound.Pc().Pa();
        double Tr = T.K() / compound.Tc().K();
        double w = compound.omega();
        double a = (psi * alpha(Tr, w) * Math.pow(R, 2) * Math.pow(Tc, 2) / Pc);
        double b = (omega * R * Tc / Pc);

        double volume = -1;
        double p = P.Pa();

        // Compute the volume roots at this P, and pick the largest real
        // value.  This is the vapor volume.

        /*
         * The cubic equation to solve is:
         *
         * -P * V^3 +
         * (P*b-P*sigma*b-P*epsilon*b+R*T) * V^2 +
         * (R*T*epsilon*b - a(T) + R*T*sigma*b +
         *     P*b^2*sigma + P*epsilon*b^2 - P*epsilon*b^2*sigma) * V +
         * P*epsilon*b^3*sigma+a(T)*b+R*T*epsilon*b^2*sigma
         *    = 0
         *
         * We convert this to a*V^3 + b*V^2 + c*V + d = 0 and solve.
         */
        double[] cubic = new double[4];
        cubic[3] = -p;

        cubic[2] = p * b - p * sigma * b -
                p * epsilon * b + R * t;

        cubic[1] = R * t * epsilon * b - a +
                R * t * sigma * b +
                p * Math.pow(b, 2) * sigma +
                p * epsilon * Math.pow(b, 2) -
                p * epsilon * Math.pow(b, 2) * sigma;

        cubic[0] = p * epsilon * Math.pow(b, 3) * sigma +
                a * b + R * t * epsilon * Math.pow(b, 2) * sigma;

        if (DEBUG) {
            log.trace("A: " + cubic[3] + " B: " + cubic[2] +
                " C: " + cubic[1] + " D: " + cubic[0]);
        }

        double vStart = 1;
        Complex [] roots = solver.solveAllComplex(cubic, vStart);

        for (int i = 0; i < roots.length; i++) {
            if (DEBUG) {
                log.trace("V (search): " + roots[i]);
            }

            if (convergence.isZero(roots[i].getImaginary())) {
                // Looking for the largest real value.
                if (volume < 0) {
                    // This is the first real value.
                    volume = roots[i].getReal();
                } else if (roots[i].getReal() > volume) {
                    volume = roots[i].getReal();
                }
            }
        }

        // Save and return the results. ---------------------------------------

        Equilibrium result = new Equilibrium();
        result.put(Equilibrium.KEY_ITER, 1);
        result.put(Equilibrium.KEY_P, P);
        result.put(Equilibrium.KEY_T, T);

        // This is a vapor-only case.
        result.put(Equilibrium.KEY_PHASE, Phase.VAPOR);

        addVaporValues(result, compound, p, t, volume, a, b, alpha(Tr, w), Tr, w);

        // All done!
        return result;
    }

    /**
     * Determine the equilibrium properties of a pure compound (vapor phase)
     * at a specific temperature and volume.
     *
     * @param compound pure compound
     * @param T temperature
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium vapor(final Compound compound, final Temperature T,
        final Volume V) throws ThermoStateException {

        log.entry(compound, T, V);

        // The equation is:
        //
        // P = R*T/(V - b) - a(T) / ((V + epsilon*b) * (V + sigma*b))
        double t = T.K();
        double Tc = compound.Tc().K();
        double Pc = compound.Pc().Pa();
        double Tr = T.K() / compound.Tc().K();
        double w = compound.omega();
        double a = (psi * alpha(Tr, w) * Math.pow(R, 2) * Math.pow(Tc, 2) / Pc);
        double b = (omega * R * Tc / Pc);
        double volume = V.m3mol();

        // p is in Pa
        double p = (R * t / (volume - b)) -
                a / ((volume + epsilon * b) * (volume + sigma * b));

        // Verify the bounds of T and V. --------------------------------------

        if ((p > Pc) && (t < Tc)) {
            // This is supposed to be a vapor state, which is not valid at
            // this temperature and pressure: supercritical pressure,
            // subcritical temperature.
            throw new ThermoStateException("Compound: " + compound +
                " T: " + T + " Tc: " + compound.Tc() +
                " P: " + p + " Pc: " + Pc + " Pa");
        }

        double Vc = criticalVolume(Tc, Pc, w);
        if (volume > Vc) {
            // This is supposed to be a vapor state, which is not valid at
            // this volume.
            throw new ThermoStateException("Compound: " + compound +
                " V: " + volume + " Vc: " + Vc + " m3/mol");
        }

        // Save and return the results. ---------------------------------------

        Equilibrium result = new Equilibrium();
        result.put(Equilibrium.KEY_ITER, 1);
        result.put(Equilibrium.KEY_P, Pressure.at(p, "Pa"));
        result.put(Equilibrium.KEY_T, T);

        // This is a vapor-only case.
        result.put(Equilibrium.KEY_PHASE, Phase.VAPOR);

        addVaporValues(result, compound, p, t, volume, a, b, alpha(Tr, w), Tr, w);

        // All done!
        return result;
    }

    /**
     * Determine the equilibrium properties of a pure compound (vapor phase)
     * at a specific pressure and volume.
     *
     * @param compound pure compound
     * @param P pressure
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium vapor(final Compound compound, final Pressure P,
        final Volume V) throws ThermoStateException {

        log.entry(compound, P, V);

        double Pc = compound.Pc().Pa();
        double Tc = compound.Tc().K();
        double w = compound.omega();
        double p = P.Pa();
        double v = V.m3mol();
        double b = (omega * R * Tc / Pc);

        // Verify the bounds of P and V. --------------------------------------

        double Vc = criticalVolume(Tc, Pc, w);
        if (v < Vc) {
            // This is supposed to be a vapor state, which is not valid at
            // this volume.
            throw new ThermoStateException("Compound: " + compound +
                " V: " + v + " Vc: " + Vc + " m3/mol");
        }

        // We now need to iterate to find the T that matches the EOS.

        // Start at the critical temperature.
        double t = compound.Tc().K();
        double minT = compound.Tp().K();
        double delta = (t - minT) / 3.0;

        int iter = 0;
        int maxIter = 100;
        boolean wentUp = true;
        double oldT = -1;

        double Tr = t / compound.Tc().K();
        double a = (psi * alpha(Tr, w) * Math.pow(R, 2) * Math.pow(Tc, 2) / Pc);

        for (;;) {
            if (DEBUG) {
                log.trace("ENTER iter " + iter + " t " + t +
                    " oldT " + oldT + " delta: " + delta);
            }

            // Recompute a(T)
            Tr = t / compound.Tc().K();
            a = (psi * alpha(Tr, w) * Math.pow(R, 2) * Math.pow(Tc, 2) / Pc);

            double testP = (R * t / (v - b)) -
                a / ((v + epsilon * b) * (v + sigma * b));

            // If we nailed it, awesome
            if (convergence.isSame(testP, p)) {
                if (DEBUG) {
                    log.trace("CONVERGED");
                }
                break;
            }

            if (testP > p) {
                // We are at a temperature corresponding to a higher pressure
                // than specified.  We need to go lower on the next
                // temperature guess.
                if (wentUp == true) {
                    // Last iteration we went up in temperature, now we need
                    // to switch direction.  Reduce delta.
                    delta *= 0.5;
                }
                while (t - delta <= minT) {
                    if (DEBUG) {
                        log.debug("OUT OF RANGE delta: " + delta);
                    }
                    delta *= 0.5;
                }
                t -= delta;
                wentUp = false;
            } else {
                // We are at a temperature corresponding to a lower pressure
                // than specified.  We need to go higher on the next
                // temperature guess.
                if (wentUp == false) {
                    // Last iteration we went down in temperature, now we
                    // need to switch direction.  Reduce delta.
                    delta *= 0.5;
                }
                t += delta;
                wentUp = true;
            }

            // See if we are out of iterations
            if (iter == maxIter) {
                log.warn("vapor(compound, P, V): giving up after " + iter +
                    " iterations: " + t);
                break;
            }
            iter++;

        } // for (;;)

        if (iter != maxIter) {
            if (DEBUG) {
                log.debug("T found after " + iter + " iterations: " +
                    oldT);
            }
        }

        // Save and return the results. ---------------------------------------

        Equilibrium result = new Equilibrium();
        result.put(Equilibrium.KEY_ITER, 1);
        result.put(Equilibrium.KEY_P, P);
        result.put(Equilibrium.KEY_T, Temperature.at(t, "K"));

        // This is a vapor-only case.
        result.put(Equilibrium.KEY_PHASE, Phase.VAPOR);

        addVaporValues(result, compound, p, t, v, a, b, alpha(Tr, w), Tr, w);

        // All done!
        return result;
    }

    // Liquid -----------------------------------------------------------------

    /**
     * Determine the equilibrium properties of a pure compound (liquid phase)
     * at a specific temperature and pressure.
     *
     * @param compound pure compound
     * @param T temperature
     * @param P pressure
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium liquid(final Compound compound, final Temperature T,
        final Pressure P) throws ThermoStateException {

        log.entry(compound, T, P);

        double t = T.K();
        double Tc = compound.Tc().K();
        double Pc = compound.Pc().Pa();
        double Tr = T.K() / compound.Tc().K();
        double w = compound.omega();
        double a = (psi * alpha(Tr, w) * Math.pow(R, 2) * Math.pow(Tc, 2) / Pc);
        double b = (omega * R * Tc / Pc);

        // Verify the bounds of T and P. --------------------------------------

        if (T.compareTo(compound.Tc()) > 0) {
            // This is a liquid state, which is not valid at this temperature
            // (it is supercritical).
            throw new ThermoStateException("Compound: " + compound +
                " T: " + T + " Tc: " + compound.Tc());
        }
        if (T.compareTo(compound.Tp()) < 0) {
            // This is a liquid state, which is not valid at this temperature
            // (it is below the triple point).
            throw new ThermoStateException("Compound: " + compound +
                " T: " + T + " Tp: " + compound.Tp());
        }
        if (P.compareTo(compound.Pp()) < 0) {
            // This is a liquid state, which is not valid at this pressure
            // (it is below the triple point).
            throw new ThermoStateException("Compound: " + compound +
                " P: " + P + " Pp: " + compound.Pp());
        }

        double volume = -1;
        double p = P.Pa();

        // Compute the volume roots at this P, and pick the smallest real
        // value.  This is the liquid volume.

        /*
         * The cubic equation to solve is:
         *
         * -P * V^3 +
         * (P*b-P*sigma*b-P*epsilon*b+R*T) * V^2 +
         * (R*T*epsilon*b - a(T) + R*T*sigma*b +
         *     P*b^2*sigma + P*epsilon*b^2 - P*epsilon*b^2*sigma) * V +
         * P*epsilon*b^3*sigma+a(T)*b+R*T*epsilon*b^2*sigma
         *    = 0
         *
         * We convert this to a*V^3 + b*V^2 + c*V + d = 0 and solve.
         */
        double[] cubic = new double[4];
        cubic[3] = -p;

        cubic[2] = p * b - p * sigma * b -
                p * epsilon * b + R * t;

        cubic[1] = R * t * epsilon * b - a +
                R * t * sigma * b +
                p * Math.pow(b, 2) * sigma +
                p * epsilon * Math.pow(b, 2) -
                p * epsilon * Math.pow(b, 2) * sigma;

        cubic[0] = p * epsilon * Math.pow(b, 3) * sigma +
                a * b + R * t * epsilon * Math.pow(b, 2) * sigma;

        if (DEBUG) {
            log.trace("A: " + cubic[3] + " B: " + cubic[2] +
                " C: " + cubic[1] + " D: " + cubic[0]);
        }

        double vStart = 1;
        Complex [] roots = solver.solveAllComplex(cubic, vStart);

        for (int i = 0; i < roots.length; i++) {
            if (DEBUG) {
                log.trace("V (search): " + roots[i]);
            }

            if (convergence.isZero(roots[i].getImaginary())) {
                // Looking for the smallest real value.
                if (volume < 0) {
                    // This is the first real value.
                    volume = roots[i].getReal();
                } else if (roots[i].getReal() < volume) {
                    volume = roots[i].getReal();
                }
            }
        }

        // Save and return the results. ---------------------------------------

        Equilibrium result = new Equilibrium();
        result.put(Equilibrium.KEY_ITER, 1);
        result.put(Equilibrium.KEY_P, P);
        result.put(Equilibrium.KEY_T, T);

        // This is a liquid-only case.
        result.put(Equilibrium.KEY_PHASE, Phase.LIQUID);

        addLiquidValues(result, compound, p, t, volume, a, b, alpha(Tr, w), Tr, w);

        // All done!
        return result;
    }

    /**
     * Determine the equilibrium properties of a pure compound (liquid phase)
     * at a specific temperature and volume.
     *
     * @param compound pure compound
     * @param T temperature
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium liquid(final Compound compound, final Temperature T,
        final Volume V) throws ThermoStateException {

        log.entry(compound, T, V);

        // The equation is:
        //
        // P = R*T/(V - b) - a(T) / ((V + epsilon*b) * (V + sigma*b))
        double t = T.K();
        double Tc = compound.Tc().K();
        double Pc = compound.Pc().Pa();
        double Tr = T.K() / compound.Tc().K();
        double w = compound.omega();
        double a = (psi * alpha(Tr, w) * Math.pow(R, 2) * Math.pow(Tc, 2) / Pc);
        double b = (omega * R * Tc / Pc);
        double volume = V.m3mol();

        // p is in Pa
        double p = (R * t / (volume - b)) -
                a / ((volume + epsilon * b) * (volume + sigma * b));

        // Verify the bounds of T and V. --------------------------------------

        if ((p > Pc) && (t < Tc)) {
            // This is supposed to be a vapor state, which is not valid at
            // this temperature and pressure: supercritical pressure,
            // subcritical temperature.
            throw new ThermoStateException("Compound: " + compound +
                " T: " + T + " Tc: " + compound.Tc() +
                " P: " + p + " Pc: " + Pc + " Pa");
        }

        double Vc = criticalVolume(Tc, Pc, w);
        if (volume > Vc) {
            // This is supposed to be a vapor state, which is not valid at
            // this volume.
            throw new ThermoStateException("Compound: " + compound +
                " V: " + volume + " Vc: " + Vc + " m3/mol");
        }

        // Save and return the results. ---------------------------------------

        Equilibrium result = new Equilibrium();
        result.put(Equilibrium.KEY_ITER, 1);
        result.put(Equilibrium.KEY_P, Pressure.at(p, "Pa"));
        result.put(Equilibrium.KEY_T, T);

        // This is a liquid-only case.
        result.put(Equilibrium.KEY_PHASE, Phase.LIQUID);

        addLiquidValues(result, compound, p, t, volume, a, b, alpha(Tr, w), Tr, w);

        // All done!
        return result;
    }

    /**
     * Determine the equilibrium properties of a pure compound (liquid phase)
     * at a specific pressure and volume.
     *
     * @param compound pure compound
     * @param P pressure
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium liquid(final Compound compound, final Pressure P,
        final Volume V) throws ThermoStateException {

        log.entry(compound, P, V);

        double Pc = compound.Pc().Pa();
        double Tc = compound.Tc().K();
        double w = compound.omega();
        double p = P.Pa();
        double v = V.m3mol();
        double b = (omega * R * Tc / Pc);

        // Verify the bounds of P and V. --------------------------------------

        double Vc = criticalVolume(Tc, Pc, w);
        if (v > Vc) {
            // This is supposed to be a liquid state, which is not valid at
            // this volume.
            throw new ThermoStateException("Compound: " + compound +
                " V: " + v + " Vc: " + Vc + " m3/mol");
        }

        // We now need to iterate to find the T that matches the EOS.

        // Start at the critical temperature.
        double t = compound.Tc().K();
        double minT = compound.Tp().K();
        double delta = (t - minT) / 3.0;

        int iter = 0;
        int maxIter = 100;
        boolean wentUp = true;
        double oldT = -1;

        double Tr = t / compound.Tc().K();
        double a = (psi * alpha(Tr, w) * Math.pow(R, 2) * Math.pow(Tc, 2) / Pc);

        for (;;) {
            if (DEBUG) {
                log.trace("ENTER iter " + iter + " t " + t +
                    " oldT " + oldT + " delta: " + delta);
            }

            // Recompute a(T)
            Tr = t / compound.Tc().K();
            a = (psi * alpha(Tr, w) * Math.pow(R, 2) * Math.pow(Tc, 2) / Pc);

            double testP = (R * t / (v - b)) -
                a / ((v + epsilon * b) * (v + sigma * b));

            // If we nailed it, awesome
            if (convergence.isSame(testP, p)) {
                if (DEBUG) {
                    log.trace("CONVERGED");
                }
                break;
            }

            if (testP > p) {
                // We are at a temperature corresponding to a higher pressure
                // than specified.  We need to go lower on the next
                // temperature guess.
                if (wentUp == true) {
                    // Last iteration we went up in temperature, now we need
                    // to switch direction.  Reduce delta.
                    delta *= 0.5;
                }
                while (t - delta <= minT) {
                    if (DEBUG) {
                        log.debug("OUT OF RANGE delta: " + delta);
                    }
                    delta *= 0.5;
                }
                t -= delta;
                wentUp = false;
            } else {
                // We are at a temperature corresponding to a lower pressure
                // than specified.  We need to go higher on the next
                // temperature guess.
                if (wentUp == false) {
                    // Last iteration we went down in temperature, now we
                    // need to switch direction.  Reduce delta.
                    delta *= 0.5;
                }
                t += delta;
                wentUp = true;
            }

            // See if we are out of iterations
            if (iter == maxIter) {
                log.warn("vapor(compound, P, V): giving up after " + iter +
                    " iterations: " + t);
                break;
            }
            iter++;

        } // for (;;)

        if (iter != maxIter) {
            if (DEBUG) {
                log.debug("T found after " + iter + " iterations: " +
                    oldT);
            }
        }

        // Save and return the results. ---------------------------------------

        Equilibrium result = new Equilibrium();
        result.put(Equilibrium.KEY_ITER, 1);
        result.put(Equilibrium.KEY_P, P);
        result.put(Equilibrium.KEY_T, Temperature.at(t, "K"));

        // This is a liquid-only case.
        result.put(Equilibrium.KEY_PHASE, Phase.LIQUID);

        addLiquidValues(result, compound, p, t, v, a, b, alpha(Tr, w), Tr, w);

        // All done!
        return result;
    }

    // Vapor/Liquid Equilibrium -----------------------------------------------

    /**
     * Determine the equilibrium properties of a pure compound at a specific
     * saturated temperature.
     *
     * @param compound pure compound
     * @param T temperature
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium vle(final Compound compound, final Temperature T)
        throws ThermoStateException {

        log.entry(compound, T);

        double Tsat = T.K();
        double Tc = compound.Tc().K();
        double Pc = compound.Pc().Pa();
        double Tr = T.K() / compound.Tc().K();
        double w = compound.omega();
        double a = (psi * alpha(Tr, w) * Math.pow(R, 2) * Math.pow(Tc, 2) / Pc);
        double b = (omega * R * Tc / Pc);

        // The bounds of the search
        double Pp = compound.Pp().Pa();
        double maxPsat = Pp;
        double minPsat = Pc;
        double vStart = 1;

        /*
         * The general procedure is as follows:
         *
         * 1. Verify that the provided Tsat is between the triple point and
         *    criticial point.
         *
         * 2. Determine the valid bounds of Psat by determining the extrema
         *    of the cubic in V (e.g. the locations where dV = 0).  This
         *    requires finding the real roots of a fourth-order polynomial.
         *
         * 3. Perform a search on Psat such that the difference between
         *    liquid and vapor fugacities is zero.  This result is Psat.
         *
         * 4. At the end of the search, save the intermediate values to the
         *    result object and return it.
         */


        // 1. Verify the bounds of Tsat. --------------------------------------

        if (T.compareTo(compound.Tc()) > 0) {
            // This is a vapor-liquid equilibrium, which is not valid at this
            // temperature (it is supercritical).
            throw new ThermoStateException("Compound: " + compound +
                " T: " + T + " Tc: " + compound.Tc());
        }
        if (T.compareTo(compound.Tp()) < 0) {
            // This is a vapor-liquid equilibrium, which is not valid at this
            // temperature (it is below the triple point).
            throw new ThermoStateException("Compound: " + compound +
                " T: " + T + " Tp: " + compound.Tp());
        }

        // 2. Solve the quadratic polynomial to find the bounds of Psat. ------

        /*
         * The real reason to do this search is to establish a region in
         * which the cubic volume roots used to find the vapor and liquid
         * fugacities are always real.  If we started with Psat guesses
         * outside that range, we could have only a single real root: no
         * problem, except that we would not have a solid heuristic to
         * determine if that was a liquid or a vapor root.  One would think
         * you could just use the critical volume, but then you still don't
         * have a clue on the proper step size to use to get back into the
         * range where there are three roots if you start your search outside
         * that range.
         */

        /*
         * The equation to solve is:
         *
         * R*T * V^4 +
         * (2*R*T*epsilon*b + 2*R*T*sigma*b - 2*a(T)) * V^3 +
         * (epsilon^2*b^2*R*T + sigma^2*b^2*R*T + 4*a(T)*b -
         *     epsilon*b*a(T) + 4*R*T*epsilon*b^2*sigma - sigma*b*a(T)) * V^2 +
         * (2*R*T*epsilon^2*b^3*sigma + 2*a(T)*b^2*sigma + 2*a(T)*epsilon*b^2 -
         *     2*a(T)*b^2 + 2*R*T*sigma^2*b^3*epsilon) * V -
         * a(T)*b^3*sigma + R*T*epsilon^2*b^4*sigma^2 - a(T)*epsilon*b^3
         *    = 0
         *
         * We convert this to a*V^4 + b*V^3 + c*V^2 + d*v + e = 0 and solve.
         */
        double[] quartic = new double[5];
        quartic[4] = R * Tsat;

        quartic[3] = 2 * R * Tsat * epsilon * b + 2 * R * Tsat * sigma * b -
                2 * a;

        quartic[2] = Math.pow(epsilon, 2) * Math.pow(b, 2) * R * Tsat +
                Math.pow(sigma, 2) * Math.pow(b, 2) * R * Tsat + 4 * a * b -
                epsilon * b * a +
                4 * R * Tsat * epsilon * Math.pow(b, 2) * sigma -
                sigma * b * a;

        quartic[1] = 2 * R * Tsat * Math.pow(epsilon, 2) * Math.pow(b, 3) * sigma +
                2 * a * Math.pow(b, 2) * sigma +
                2 * a * epsilon * Math.pow(b,2) - 2 * a * Math.pow(b, 2) +
                2 * R * Tsat * Math.pow(sigma, 2) * Math.pow(b, 3) * epsilon;

        quartic[0] = -a * Math.pow(b, 3) * sigma +
                R * Tsat * Math.pow(epsilon, 2) * Math.pow(b, 4) * Math.pow(sigma, 2) -
                a * epsilon * Math.pow(b, 3);

        if (DEBUG) {
            log.trace("A: " + quartic[4] + " B: " + quartic[3] +
                " C: " + quartic[2] + " D: " + quartic[1] +
                " E: " + quartic[0]);
        }

        Complex[] roots = solver.solveAllComplex(quartic, 1.0);

        if (DEBUG) {
            log.trace("roots.length " + roots.length);
        }

        for (int i = 0; i < roots.length; i++) {
            if (DEBUG) {
                log.debug("V (psat): " + roots[i]);
            }

            if (!convergence.isZero(roots[i].getImaginary())) {
                // Complex root, disregard

                if (DEBUG) {
                    // DEBUG: toss a stack trace here
                    throw new RuntimeException("complex root found!");
                }

                continue;
            }

            // Get the volume at the extrema
            double v = roots[i].getReal();

            // Convert to pressure
            double p = R*Tsat/(v-b) - a/((v+epsilon*b)*(v+sigma*b));
            if ((p > maxPsat) && (p < Pc)) {
                maxPsat = p;
                vStart = v;
            }
            if ((p < minPsat) && (p > Pp)) {
                minPsat = p;
            }
            if (DEBUG) {
                log.trace("  Pressure: " + Pressure.at(p, "Pa").psig() + " psig");
            }
        }
        if (minPsat < Pp) {
            minPsat = Pp;
        }
        if (minPsat == maxPsat) {
            // The EOS method doesn't predit the triple point.  If we have
            // only a max, relax the bounds to go below the triple point.
            minPsat = 0;
        }
        if (DEBUG) {
            log.debug("  Pressure MIN: " + Pressure.at(minPsat, "Pa").psig());
            log.debug("  Pressure MAX: " + Pressure.at(maxPsat, "Pa").psig());
            log.debug("----------------");
            log.debug("  Pressure MIN: " + minPsat);
            log.debug("  Pressure MAX: " + maxPsat);
        }

        // 3. Perform a search on Psat such that the difference between  ------
        //    liquid and vapor fugacities is zero.  This result is Psat. ------

        double testPsat = (minPsat + maxPsat) / 2;
        double delta = (maxPsat - minPsat) / 3;
        double oldPsat = Pc;
        double volumes[] = new double[3];

        // These values are used to compute the residual Gibbs, enthalpy, and
        // entropy at the end of this function.
        double Vl;
        double Vv;
        double Zl;
        double Zv;
        double beta;
        double Iv;
        double Il;
        double q;

        int iter = 0;
        int maxIter = 100;
        boolean wentUp = true;

        for (;;) {
            if (DEBUG) {
                log.trace("ENTER iter " + iter + " testPsat " + testPsat +
                    " oldPsat " + oldPsat + " delta: " + delta);
            }

            oldPsat = testPsat;
            double P = testPsat;

            // Compute the volume roots at this Psat

            /*
             * The cubic equation to solve is:
             *
             * -P * V^3 +
             * (P*b-P*sigma*b-P*epsilon*b+R*T) * V^2 +
             * (R*T*epsilon*b - a(T) + R*T*sigma*b +
             *     P*b^2*sigma + P*epsilon*b^2 - P*epsilon*b^2*sigma) * V +
             * P*epsilon*b^3*sigma+a(T)*b+R*T*epsilon*b^2*sigma
             *    = 0
             *
             * We convert this to a*V^3 + b*V^2 + c*V + d = 0 and solve.
             */
            double[] cubic = new double[4];
            cubic[3] = -P;

            cubic[2] = P * b - P * sigma * b -
                    P * epsilon * b + R * Tsat;

            cubic[1] = R * Tsat * epsilon * b - a +
                    R * Tsat * sigma * b +
                    P * Math.pow(b, 2) * sigma +
                    P * epsilon * Math.pow(b, 2) -
                    P * epsilon * Math.pow(b, 2) * sigma;

            cubic[0] = P * epsilon * Math.pow(b, 3) * sigma +
                    a * b + R * Tsat * epsilon * Math.pow(b, 2) * sigma;

            if (DEBUG) {
                log.trace("A: " + cubic[3] + " B: " + cubic[2] +
                    " C: " + cubic[1] + " D: " + cubic[0]);
            }

            roots = solver.solveAllComplex(cubic, vStart);

            for (int i = 0; i < roots.length; i++) {
                if (DEBUG) {
                    log.trace("V (search): " + roots[i]);
                }

                if (!convergence.isZero(roots[i].getImaginary())) {
                    // Complex root, this is an error
                    throw new ThermoStateException("Complex root found " +
                        " within 'valid' search region!");
                }
            }

            // Sort results.  There are only three so do it manually.

            // Least
            if ((roots[0].getReal() < roots[1].getReal()) &&
                (roots[0].getReal() < roots[2].getReal())
            ) {
                volumes[0] = roots[0].getReal();
            } else if ((roots[1].getReal() < roots[0].getReal()) &&
                (roots[1].getReal() < roots[2].getReal())
            ) {
                volumes[0] = roots[1].getReal();
            } else {
                volumes[0] = roots[2].getReal();
            }

            // Middle
            if (((roots[0].getReal() > roots[1].getReal()) &&
                    (roots[0].getReal() < roots[2].getReal())) ||
                ((roots[0].getReal() > roots[2].getReal()) &&
                    (roots[0].getReal() < roots[1].getReal()))
            ) {
                volumes[1] = roots[0].getReal();
            } else if (((roots[1].getReal() > roots[0].getReal()) &&
                    (roots[1].getReal() < roots[2].getReal())) ||
                ((roots[1].getReal() > roots[2].getReal()) &&
                    (roots[1].getReal() < roots[0].getReal()))
            ) {
                volumes[1] = roots[1].getReal();
            } else {
                volumes[1] = roots[2].getReal();
            }

            // Most
            if ((roots[0].getReal() > roots[1].getReal()) &&
                (roots[0].getReal() > roots[2].getReal())
            ) {
                volumes[2] = roots[0].getReal();
            } else if ((roots[1].getReal() > roots[0].getReal()) &&
                (roots[1].getReal() > roots[2].getReal())
            ) {
                volumes[2] = roots[1].getReal();
            } else {
                volumes[2] = roots[2].getReal();
            }
            if (DEBUG) {
                for (int i = 0; i < volumes.length; i++) {
                    log.trace("volumes: " + i + " " + volumes[i]);
                }
            }

            // Pressure is within the range of a valid Psat, look at fugacity.
            Vl = volumes[0];
            Vv = volumes[2];

            Zl = Vl * P / (R * Tsat);
            Zv = Vv * P / (R * Tsat);
            beta = b * P / (R * Tsat);
            Iv = 1.0 / (sigma - epsilon) * Math.log((Zv + sigma * beta) /
                (Zv + epsilon * beta));
            Il = 1.0 / (sigma - epsilon) * Math.log((Zl + sigma * beta) /
                (Zl + epsilon * beta));
            q = a / (b * R * Tsat);
            double fugacity_v = Math.exp(Zv - 1 - Math.log(Zv - beta) - q * Iv);
            double fugacity_l = Math.exp(Zl - 1 - Math.log(Zl - beta) - q * Il);

            if (DEBUG) {
                log.trace(String.format("Vv: %f Vl: %f", Vv, Vl));
                log.trace(String.format("Zv: %f Zl: %f", Zv, Zl));
                log.trace(String.format("beta: %f", beta));
                log.trace(String.format("Iv: %f Il: %f", Iv, Il));
                log.trace(String.format("q: %f", q));
                log.debug(String.format("fugacity vapor: %f liq: %f",
                        fugacity_v, fugacity_l));
            }

            if (DEBUG) {
                log.trace("iter " + iter + " oldPsat " + oldPsat +
                    " delta " + delta + " fugacity vapor " +
                    fugacity_v + " fugacity liquid " + fugacity_l);
            }

            // If we nailed it, awesome
            if (convergence.isSame(fugacity_v, fugacity_l)) {
                if (DEBUG) {
                    log.trace("CONVERGED");
                }
                break;
            }

            if (fugacity_v > fugacity_l) {
                // If vapor fugacity is higher than liquid fugacity, reduce
                // pressure.
                if (wentUp == true) {
                    // Last iteration we went up in pressure, now we need to
                    // switch direction.  Reduce delta.
                    delta *= 0.5;
                }
                while (testPsat - delta <= minPsat) {
                    if (DEBUG) {
                        log.debug("OUT OF RANGE delta: " + delta);
                    }
                    delta *= 0.5;
                }
                testPsat -= delta;
                wentUp = false;
            } else {
                // If vapor fugacity is lower than liquid fugacity, increase
                // pressure.
                if (wentUp == false) {
                    // Last iteration we went down in pressure, now we need
                    // to switch direction.  Reduce delta.
                    delta *= 0.5;
                }
                while (testPsat + delta >= maxPsat) {
                    if (DEBUG) {
                        log.debug("OUT OF RANGE delta: " + delta);
                    }
                    delta *= 0.5;
                }
                testPsat += delta;
                wentUp = true;
            }

            // See if we are out of iterations
            if (iter == maxIter) {
                log.warn("vle(compound, T): giving up after " + iter +
                    " iterations: " + oldPsat);
                break;
            }
            iter++;

        } // for (;;)

        if (iter != maxIter) {
            if (DEBUG) {
                log.debug("Psat found after " + iter + " iterations: " +
                    oldPsat);
            }
        }

        // 4. At the end of the search, save the intermediate values to the ---
        //    result object and return it. ------------------------------------

        Equilibrium result = new Equilibrium();
        result.put(Equilibrium.KEY_ITER, iter);
        result.put(Equilibrium.KEY_PSAT, Pressure.at(oldPsat, "Pa"));
        result.put(Equilibrium.KEY_TSAT, T);

        // This is a vapor-liquid equilibrium
        result.put(Equilibrium.KEY_PHASE, Phase.VLE);

        // Populate the volumes.  Note that this is volume, not density:
        // highest volume/mol is vapor.
        result.put(Equilibrium.KEY_LIQUID_V, Volume.at(volumes[0], "m3/mol"));
        result.put(Equilibrium.KEY_INTERIOR_CUBIC_EOS_V,
            Volume.at(volumes[1], "m3/mol"));
        result.put(Equilibrium.KEY_VAPOR_V, Volume.at(volumes[2], "m3/mol"));

        // Put the residual enthalpy values in.
        result.put(Equilibrium.KEY_VAPOR_Z, Zv);
        result.put(Equilibrium.KEY_LIQUID_Z, Zl);

        double GRl = R * Tsat * (Zl - 1 - Math.log(Zl - beta) - q * Il);
        double GRv = R * Tsat * (Zv - 1 - Math.log(Zv - beta) - q * Iv);
        result.put(Equilibrium.KEY_LIQUID_GR, Gibbs.at(GRl, "J/mol"));
        result.put(Equilibrium.KEY_VAPOR_GR, Gibbs.at(GRv, "J/mol"));

        double d_ln_alpha = dLnAlpha(Tr, w);

        double HRl = R * Tsat * (Zl - 1 + q * Il * (d_ln_alpha - 1));
        double HRv = R * Tsat * (Zv - 1 + q * Iv * (d_ln_alpha - 1));
        result.put(Equilibrium.KEY_LIQUID_HR, Enthalpy.at(HRl, "J/mol"));
        result.put(Equilibrium.KEY_VAPOR_HR, Enthalpy.at(HRv, "J/mol"));

        double SRl = R * (Math.log(Zl - beta) + q * Il * d_ln_alpha);
        double SRv = R * (Math.log(Zv - beta) + q * Iv * d_ln_alpha);
        result.put(Equilibrium.KEY_LIQUID_SR, Entropy.at(SRl, "J/mol-K"));
        result.put(Equilibrium.KEY_VAPOR_SR, Entropy.at(SRv, "J/mol-K"));

        if (compound.Cp_IG_Function() != null) {
            // Compound has a heat capacity function, so go ahead and
            // put the real enthalpies in.
            double H_0 = compound.Hf_IG().Jmol();
            double H_v = compound.Cp_IG_Function().integrate(Constants.STP_T,
                T).Jmol();

            result.put(Equilibrium.KEY_LIQUID_H,
                Enthalpy.at(H_0 + H_v + HRl, "J/mol"));
            result.put(Equilibrium.KEY_VAPOR_H,
                Enthalpy.at(H_0 + H_v + HRv, "J/mol"));
        }

        // All done!
        return result;
    }

    /**
     * Determine the equilibrium properties of a pure compound at a specific
     * saturated pressure.
     *
     * @param compound pure compound
     * @param P pressure
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium vle(final Compound compound, final Pressure P)
        throws ThermoStateException {

        log.entry(compound, P);

        /*
         * The general procedure is as follows:
         *
         * 1. Verify that the provided Psat is between the triple point and
         *    criticial point.
         *
         * 2. Perform a search on Tsat such that the Psat matches P.
         *
         * 3. Return the result of #2.
         *
         * We have to do it this way because there is no decent analytical
         * method to find T given V and P in the Peng-Robinson equation.
         */

        // 1. Verify the bounds of Psat. --------------------------------------

        if (P.compareTo(compound.Pc()) > 0) {
            // This is a vapor-liquid equilibrium, which is not valid at this
            // pressure (it is supercritical).
            throw new ThermoStateException("Compound: " + compound +
                " P: " + P + " Pc: " + compound.Pc());
        }
        if (P.compareTo(compound.Pp()) < 0) {
            // This is a vapor-liquid equilibrium, which is not valid at this
            // pressure (it is below the triple point).
            throw new ThermoStateException("Compound: " + compound +
                " P: " + P + " Pp: " + compound.Pp());
        }

        // 2. Perform a search on Tsat to match Psat == P. --------------------

        // 3. Perform a search on Psat such that the difference between  ------
        //    liquid and vapor fugacities is zero.  This result is Psat. ------

        // The bounds of the search
        double maxTsat = compound.Tc().K();
        double minTsat = compound.Tp().K();
        double testTsat = (minTsat + maxTsat) / 2;
        double delta = (maxTsat - minTsat) / 3;
        double oldTsat = compound.Tc().K();

        int iter = 0;
        int maxIter = 100;
        boolean wentUp = true;
        Equilibrium result;

        // We doing a "double-search" here: Psat needs to match P, but inside
        // the call to vle(T) we have fugacities that need to match.  Holding
        // the same convergence in the outer loop would force basically twice
        // the convergence of the inner loop, which is unrealistic
        // (especially since we use LaguerreSolver to find the volume roots
        // using its even lower EPSILON).
        double innerConvergence = convergence.getRelativeDifference();
        double outerConvergence = Math.exp(Math.log(innerConvergence) / 2);

        for (;;) {
            if (DEBUG) {
                log.trace("ENTER iter " + iter + " testTsat " + testTsat +
                    " oldTsat " + oldTsat + " delta: " + delta);
            }

            oldTsat = testTsat;
            Temperature T = Temperature.at(testTsat, 'K');

            convergence.setRelativeDifference(innerConvergence);
            result = vle(compound, T);
            convergence.setRelativeDifference(outerConvergence);

            if (convergence.isSame(P.Pa(), result.Psat().Pa())) {
                // We nailed it, awesome
                if (DEBUG) {
                    log.trace("CONVERGED");
                }
                break;
            }

            if (result.Psat().compareTo(P) > 0) {
                // The computed Psat is too high, reduce temperature.
                if (wentUp == true) {
                    // Last iteration we went up in temperature, now we need
                    // to switch direction.  Reduce delta.
                    delta *= 0.5;
                }
                while (testTsat - delta <= minTsat) {
                    if (DEBUG) {
                        log.debug("OUT OF RANGE delta: " + delta);
                    }
                    delta *= 0.5;
                }
                testTsat -= delta;
                wentUp = false;
            } else {
                // The computed Psat is low, increase temperature.
                if (wentUp == false) {
                    // Last iteration we went down in temperature, now we
                    // need to switch direction.  Reduce delta.
                    delta *= 0.5;
                }
                while (testTsat + delta >= maxTsat) {
                    if (DEBUG) {
                        log.debug("OUT OF RANGE delta: " + delta);
                    }
                    delta *= 0.5;
                }
                testTsat += delta;
                wentUp = true;
            }

            // See if we are out of iterations
            if (iter == maxIter) {
                log.warn("vle(compound, P): giving up after " + iter +
                    " iterations: " + oldTsat);
                throw new ArithmeticException("vle(compound, P): giving up after " + iter +
                    " iterations: " + oldTsat);
                // break;
            }
            iter++;

        } // for (;;)
        convergence.setRelativeDifference(innerConvergence);

        if (iter != maxIter) {
            if (DEBUG) {
                log.debug("Tsat found after " + iter + " iterations: " +
                    oldTsat);
            }
        }

        result.put(Equilibrium.KEY_ITER, iter);
        return result;
    }

    // Other ------------------------------------------------------------------

    /**
     * Get the relative convergence criteria.
     *
     * @return the convergence criteria
     */
    public final Convergence getConvergence() {
        return convergence;
    }

    /**
     * Set the relative convergence criteria.
     *
     * @param convergence the new convergence criteria
     */
    public final void setConvergence(final Convergence convergence) {
        this.convergence = convergence;
    }

    // ActivityModel ----------------------------------------------------------

    /**
     * Determine the liquid-phase activity coefficients of a liquid
     * composition at a specific temperature and pressure.
     *
     * @param mixture the liquid-phase composition
     * @param T temperature
     * @param P pressure
     * @return the activity coefficients
     * @throws ThermoStateException if this state is not physically realizable
     */
    public List<Double> gamma(final Mixture mixture, final Temperature T,
        final Pressure P) throws ThermoStateException {

        // TODO
        return null;
    }

    /**
     * Determine the vapor-phase fugacity coefficients of a vapor composition
     * at a specific temperature and pressure.
     *
     * @param mixture the vapor-phase composition
     * @param T temperature
     * @param P pressure
     * @return the fugacity coefficients
     * @throws ThermoStateException if this state is not physically realizable
     */
    public List<Double> phi(final Mixture mixture, final Temperature T,
        final Pressure P) throws ThermoStateException {

        // TODO
        return null;
    }

}
