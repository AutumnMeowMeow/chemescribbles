/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.methods;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.math3.linear.BlockRealMatrix;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import cheme.compounds.Compound;
import cheme.compounds.Mixture;
import cheme.compounds.MoleFraction;
import cheme.properties.Constants;
import cheme.properties.Enthalpy;
import cheme.properties.Entropy;
import cheme.properties.Equilibrium;
import cheme.properties.Gibbs;
import cheme.properties.Phase;
import cheme.properties.Pressure;
import cheme.properties.Temperature;
import cheme.properties.Volume;

/**
 * This class performs various thermodynamic calculations for an ideal-gas
 * ideal-liquid fluid.  NOTE THAT THE PUBLIC FUNCTIONS ARE NOT RE-ENTRANT.
 */
public class IdealFluid implements Method {

    /**
     * The Logger to emit to.
     */
    protected static Logger log         = LogManager.getLogger(IdealFluid.class.getName());

    /**
     * If true, allow tracing to the Logger.
     */
    private static final boolean DEBUG = true;

    // Constants used by the method
    private static final double R = Constants.R;

    /**
     * Convergence criteria.
     */
    private Convergence convergence = new Convergence();

    /**
     * Compute the critical volume.
     *
     * @param compound the compound
     * @throws ThermoStateException if this state is not physically realizable
     */
    private double criticalVolume(final Compound compound)
        throws ThermoStateException {

        return criticalVolume(compound.Tc().K(), compound.Pc().Pa());
    }

    /**
     * Compute the critical volume.
     *
     * @param Tc the critical temperature in K
     * @param Pc the critical pressure in Pa
     * @throws ThermoStateException if this state is not physically realizable
     */
    private double criticalVolume(final double Tc,
        final double Pc) throws ThermoStateException {

        return (R * Tc) / Pc;
    }

    /**
     * Compute the saturated liquid volume.
     *
     * @param compound pure compound
     * @param Tr the reduced temperature
     * @return the saturated liquid volume in m3/mol
     */
    private double rackett(final Compound compound, final double Tr) {
        return (compound.Vc().m3mol() *
            Math.pow(compound.Zc(), Math.pow(1 - Tr, 0.2857)));
    }

    /**
     * Compute the saturated liquid temperature.
     *
     * @param compound pure compound
     * @param Vsat the saturated liquid volume in m3/mol
     * @return the saturated liquid temperature in K
     * @throws ThermoStateException if this state is not physically realizable
     */
    private double invertRackett(final Compound compound,
        final double Vsat) throws ThermoStateException {
        double Vc = compound.Vc().m3mol();
        double Zc = compound.Zc();
        double Tc = compound.Tc().K();

        if (Vsat > Vc) {
            throw new ThermoStateException("Vsat (" +Vsat + ") must be " +
                "less than Vc (" + Vc + ")");
        }
        double Tr = 1 - Math.exp(Math.log(Math.log(Vsat / Vc) / Math.log(Zc)) /
            0.2857);
        return Tr * Tc;
    }

    /**
     * Compute the saturated pressure.
     *
     * @param compound pure compound
     * @param Tsat the saturated temperature
     * @return the saturated pressure
     * @throws ThermoStateException if this state is not physically realizable
     */
    private Pressure antoine(final Compound compound,
        final Temperature Tsat) throws ThermoStateException {

        return compound.PsatFunction().eval(Tsat);
    }

    /**
     * Compute the saturated temperature.
     *
     * @param compound pure compound
     * @param Psat the saturated pressure
     * @return the saturated temperature
     * @throws ThermoStateException if this state is not physically realizable
     */
    private Temperature antoine(final Compound compound,
        final Pressure Psat) throws ThermoStateException {

        return compound.TsatFunction().eval(Psat);
    }

    /**
     * Compute the heat of vaporization.
     *
     * @param compound pure compound
     * @param Tsat the saturated temperature
     * @return the heat of vaporization in J/mol
     */
    private double watson(final Compound compound, final double Tsat) {
        double Tc = compound.Tc().K();
        double Tr1 = 298.15 / Tc;
        double Tr2 = Tsat / Tc;
        double dH1 = compound.Hlv().Jmol();
        double dH2 = dH1 * Math.pow((1 - Tr2) / (1 - Tr1), 0.38);

        return dH2;
    }

    /**
     * Compute and fill in the liquid equilibrium values.
     *
     * @param result the Equilibrium to add these values to
     * @param compound pure compound
     * @param p the pressure in Pa
     * @param t the temperature in K
     * @param volume the liquid volume in m3/mol
     * @throws ThermoStateException if this state is not physically realizable
     */
    private void addLiquidValues(final Equilibrium result,
        final Compound compound, final double p, final double t,
        final double volume) throws ThermoStateException {

        result.put(Equilibrium.KEY_LIQUID_V, Volume.at(volume, "m3/mol"));
        result.put(Equilibrium.KEY_LIQUID_Z, p * volume / (R * t));

        result.put(Equilibrium.KEY_LIQUID_GR, Gibbs.at(0, "J/mol"));
        result.put(Equilibrium.KEY_LIQUID_HR, Enthalpy.at(0, "J/mol"));
        result.put(Equilibrium.KEY_LIQUID_SR, Entropy.at(0, "J/mol-K"));

        if (compound.Cp_IG_Function() != null) {
            // Compound has a heat capacity function, so go ahead and
            // put the real enthalpies in.
            double H_0 = compound.Hf_IG().Jmol();
            double H_v = compound.Cp_IG_Function().integrate(Constants.STP_T,
                Temperature.at(t, "K")).Jmol();

            // Use Watson to find heat of vaporization
            double H_lv = watson(compound, t);

            result.put(Equilibrium.KEY_LIQUID_H,
                Enthalpy.at(H_0 + H_v - H_lv, "J/mol"));
        }
    }

    /**
     * Compute and fill in the vapor equilibrium values.
     *
     * @param result the Equilibrium to add these values to
     * @param compound pure compound
     * @param p the pressure in Pa
     * @param t the temperature in K
     * @param volume the vapor volume in m3/mol
     * @throws ThermoStateException if this state is not physically realizable
     */
    private void addVaporValues(final Equilibrium result,
        final Compound compound, final double p, final double t,
        final double volume) throws ThermoStateException {

        result.put(Equilibrium.KEY_VAPOR_V, Volume.at(volume, "m3/mol"));
        result.put(Equilibrium.KEY_VAPOR_Z, 1.0);

        result.put(Equilibrium.KEY_VAPOR_GR, Gibbs.at(0, "J/mol"));
        result.put(Equilibrium.KEY_VAPOR_HR, Enthalpy.at(0, "J/mol"));
        result.put(Equilibrium.KEY_VAPOR_SR, Entropy.at(0, "J/mol-K"));

        if (compound.Cp_IG_Function() != null) {
            // Compound has a heat capacity function, so go ahead and
            // put the real enthalpies in.
            double H_0 = compound.Hf_IG().Jmol();
            double H_v = compound.Cp_IG_Function().integrate(Constants.STP_T,
                Temperature.at(t, "K")).Jmol();

            result.put(Equilibrium.KEY_VAPOR_H,
                Enthalpy.at(H_0 + H_v, "J/mol"));
        }
    }

    // Mixture state ----------------------------------------------------------

    /**
     * Determine the equilibrium properties of a mixture (vapor or liquid) at
     * a specific temperature and pressure.
     *
     * @param mixture the mixture
     * @param T temperature
     * @param P pressure
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium state(final Mixture mixture, final Temperature T,
        final Pressure P) throws ThermoStateException {

        /*
         * This problem is: find the vapor and liquid compositions, and vapor
         * fraction, for a mixture at specified total molar composition and
         * system pressure and temperature.
         *
         * The traditional way to solve this is to iterate on K values and
         * compositions.  But I prefer (for now) to go to the fundamental
         * relations instead:
         *
         *    x_i * P_i^{sat} = y_i * P
         *    P_i^{sat} = f(T)
         *    x_i * L + y_i * V = z_i * (L + V)
         *    V = 1 - L
         *    \Sum x_i = 1
         *    \Sum y_i = 1
         *
         * These can all be represented as a simple linear algebra problem.
         * Assuming P and T are valid (e.g. this is a true multi-phase
         * system), then the trick is figuring out L to make the following
         * matrices work.
         *
         * Given N components, we will construct a 2N x 2N matrix A and 2N
         * column vectors x and b to solve the system A*x = b.
         *
         * Matrix A looks like:
         *
         * | P_1^{sat}   0         ...    0        -P    0  ...  0  |
         * |   0        P_2^{sat}  ...    0         0   -P  ...  0  |
         * |   :         :          :     :         :    :   :   :  |
         * |   0         0          0   P_N^{sat}   0    0  ... -P  |
         * |   L         0         ...    0        1-L   0  ...  0  |
         * |   0         L         ...    0         0   1-L ...  0  |
         * |   :         :          :     0         :    :   :   :  |
         * |   0         0         ...    L         0    0  ... 1-L |
         *
         *
         * Column x looks like:
         *
         * | x_1 |
         * | x_2 |
         * |  :  |
         * | x_n |
         * | y_1 |
         * | y_2 |
         * |  :  |
         * | y_n |
         *
         * Column b looks like:
         *
         * |  0 |
         * |  0 |
         * |  : |
         * |  0 |
         * | z_1 |
         * | z_2 |
         * |  :  |
         * | z_n |
         *
         *
         * The computation below is HIDEOUSLY EXPENSIVE.  But it works!  We
         * can iterate to find L by:
         *
         *     Invert A --> Ainv
         *     Find "x" by way of (Ainv . b)
         *     Renormalize this "x" --> xPrime
         *     Find "b" by way of (A . xPrime)
         *
         *     Sum the top half of the relations (Raoult's Law relations):
         *        - If the sum is negative, then the y_i's are dominating -->
         *          increase L.
         *        - If the sum is positive, then the x_i's are dominating -->
         *          decrease L.
         *     Once L is found, x_i and y_i are already known!
         *
         * I believe this matrix multiplication stuff is essentially the same
         * math as Smith, van Ness, and Abbott.  But now I can understand the
         * calculation path.
         *
         * And BTW hideously expensive is still solving a two-component flash
         * in ~5 milliseconds.
         */

        int N = mixture.size();

        // Perform a search on L to make the material balance solvable.
        // Start at 50/50.
        double L = 0.5;
        int iter = 0;
        int maxIter = 100;
        boolean wentUp = true;
        double delta = 0.1;

        RealVector xResult = null;

        for (;;) {
            if (DEBUG) {
                log.trace("ENTER iter " + iter + " L " + L +
                    " delta: " + delta);
            }

            BlockRealMatrix A = new BlockRealMatrix(2 * N, 2 * N);
            BlockRealMatrix b = new BlockRealMatrix(2 * N, 1);

            int i = 0;
            for (MoleFraction component: mixture.getComponents()) {
                double Psat = component.compound.PsatFunction().eval(T).Pa();
                A.setEntry(i, i, Psat);
                A.setEntry(i, i + N, -P.Pa());
                A.setEntry(i + N, i, L);
                A.setEntry(i + N, i + N, 1.0 - L);
                b.setEntry(i + N, 0, component.fraction);
                i++;
            }

            if (DEBUG) {
                log.trace("A: " + A);
                log.trace("b: " + b);
            }

            RealMatrix Ainv = MatrixUtils.inverse(A);
            RealMatrix xPrime = Ainv.multiply(b);

            // Renormalize x and y
            double totalX = 0.0;
            double totalY = 0.0;
            for (i = 0; i < N; i++) {
                totalX += xPrime.getEntry(i, 0);
                totalY += xPrime.getEntry(i + N, 0);
            }
            for (i = 0; i < N; i++) {
                xPrime.setEntry(i, 0, xPrime.getEntry(i, 0) / totalX);
                xPrime.setEntry(i + N, 0, xPrime.getEntry(i + N, 0) / totalY);
            }

            // Now multiply (A . (Ainverse . xPrime)).  If the top half rows
            // are negative, we need more L.
            RealMatrix bPrime = A.multiply(xPrime);

            double difference = 0.0;
            for (i = 0; i < N; i++) {
                difference += bPrime.getEntry(i, 0);
            }
            if (DEBUG) {
                log.trace("xPrime: " + xPrime);
                log.trace("bPrime: " + bPrime);
                log.trace("difference: " + difference);
            }

            if (convergence.isZero(difference)) {
                // If we nailed it, awesome
                if (DEBUG) {
                    log.trace("CONVERGED");
                }

                // Now save xPrime as our answer.
                xResult = xPrime.getColumnVector(0);
                break;
            }

            if (difference < 0) {
                // To make the pressure balance work, we don't have enough
                // liquid in the final equilibrium.  Increase L.
                if (wentUp == false) {
                    delta /= 2;
                    wentUp = true;
                }
                while (L + delta > 1.0) {
                    delta /= 2;
                }
                L += delta;
            } else {
                // The opposite of above.
                if (wentUp == true) {
                    delta /= 2;
                    wentUp = false;
                }
                while (L - delta < 0.0) {
                    delta /= 2;
                }
                L -= delta;
            }

            // See if we are out of iterations
            if (iter == maxIter) {
                log.warn("state(mixture, T, P): giving up after " + iter +
                    " iterations: " + L + " T " + T + " xPrime " + xPrime +
                    " bPrime " + bPrime);
                break;
            }
            iter++;

        } // for (;;)

        // Save and return the results. ---------------------------------------
        Equilibrium result = new Equilibrium();

        result.put(Equilibrium.KEY_P, P);
        result.put(Equilibrium.KEY_T, T);
        result.put(Equilibrium.KEY_ITER, iter);

        if (iter != maxIter) {
            if (DEBUG) {
                log.debug("L found after " + iter + " iterations: " + L);
                log.debug("x result: " + xResult);
            }
        } else {
            if (L < convergence.getRelativeDifference()) {
                // L < 0: this wants to be a vapor
                result.put(Equilibrium.KEY_VFRAC, 1.0);
                result.put(Equilibrium.KEY_PHASE, Phase.VAPOR_MIXTURE);
                result.put(Equilibrium.KEY_Y_I, mixture.getComponents());
                return result;
            }
            if ((1.0 - L) < convergence.getRelativeDifference()) {
                // L > 1: this wants to be a liquid
                result.put(Equilibrium.KEY_VFRAC, 0.0);
                result.put(Equilibrium.KEY_PHASE, Phase.LIQUID_MIXTURE);
                result.put(Equilibrium.KEY_X_I, mixture.getComponents());
                return result;
            }
            throw new ThermoStateException("Insufficient iterations to " +
                "find solution within convergence tolerance");
        }

        // Valid vapor/liquid mixture composition
        result.put(Equilibrium.KEY_VFRAC, 1.0 - L);
        result.put(Equilibrium.KEY_PHASE, Phase.VLE_MIXTURE);

        List<MoleFraction> yI = new ArrayList<MoleFraction>(mixture.size());
        List<MoleFraction> xI = new ArrayList<MoleFraction>(mixture.size());

        int i = 0;
        for (MoleFraction component: mixture.getComponents()) {
            MoleFraction x = new MoleFraction(component.compound,
                xResult.getEntry(i));
            MoleFraction y = new MoleFraction(component.compound,
                xResult.getEntry(i + N));
            xI.add(x);
            yI.add(y);
            i++;
        }

        result.put(Equilibrium.KEY_X_I, xI);
        result.put(Equilibrium.KEY_Y_I, yI);
        result.put(Equilibrium.KEY_Z_I, mixture.getComponents());
        return result;
    }

    /**
     * Determine the equilibrium properties of a mixture (vapor or liquid) at
     * a specific temperature and volume.
     *
     * @param mixture the mixture
     * @param T temperature
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium state(final Mixture mixture, final Temperature T,
        final Volume V) throws ThermoStateException {

        // TODO
        return null;
    }

    /**
     * Determine the equilibrium properties of a mixture (vapor or liquid) at
     * a specific pressure and volume.
     *
     * @param mixture the mixture
     * @param P pressure
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium state(final Mixture mixture, final Pressure P,
        final Volume V) throws ThermoStateException {

        // TODO
        return null;
    }

    // General state ----------------------------------------------------------

    /**
     * Determine the equilibrium properties of a pure compound (vapor or
     * liquid) at a specific temperature and pressure.
     *
     * @param compound pure compound
     * @param T temperature
     * @param P pressure
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium state(final Compound compound, final Temperature T,
        final Pressure P) throws ThermoStateException {

        log.entry(compound, T, P);

        double Tc = compound.Tc().K();
        double Pc = compound.Pc().Pa();
        double t = T.K();
        double p = P.Pa();
        if ((p > Pc) && (t > Tc)) {
            // This is a supercritical fluid.  Return the vapor() case.
            // log.info("1 vapor");
            return vapor(compound, T, P);
        }
        if (p > Pc) {
            // This is supercritical pressure but subcritical temperature.
            // Return the liquid() case.
            // log.info("2 liquid");
            return liquid(compound, T, P);
        }
        if (t > Tc) {
            // This is supercritical temperature but subcritical pressure.
            // Return the vapor() case.
            // log.info("3 vapor");
            return vapor(compound, T, P);
        }

        // At this point we might be in VLE.  Get the saturated pressure for
        // this temperature.  If the provided pressure (P) is below the
        // saturated pressure (Psat) then return vapor(), if it is above Psat
        // return liquid(), otherwise the caller nailed it and we can return
        // the result of vle().
        Equilibrium result = vle(compound, T);
        Pressure Psat = result.Psat();
        if (p < Psat.Pa()) {
            // log.info("4 vapor: " + p + " " + Psat.Pa());
            return vapor(compound, T, P);
        } else if (p > Psat.Pa()) {
            // log.info("5 liquid: " + p + " " + Psat.Pa());
            return liquid(compound, T, P);
        }
        return result;
    }

    /**
     * Determine the equilibrium properties of a pure compound (vapor or
     * liquid) at a specific temperature and volume.
     *
     * @param compound pure compound
     * @param T temperature
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium state(final Compound compound, final Temperature T,
        final Volume V) throws ThermoStateException {

        log.entry(compound, T, V);

        double v = V.m3mol();

        if (T.K() < compound.Tc().K()) {
            // This might be VLE.  Get the saturated properties to see.
            Equilibrium sat = vle(compound, T);
            if ((sat.LiquidV() != null) && (sat.VaporV() != null)) {
                double Vl = sat.LiquidV().m3mol();
                double Vv = sat.VaporV().m3mol();
                if ((v > Vl) && (v < Vv)) {
                    // We are in VLE.  What this means is that we have X
                    // moles of liquid at a higher density (Vl < V), and Y
                    // moles of vapor at a lower density (Vv > V):
                    //
                    // V = X*Vl + Y*Vv
                    // X + Y = 1
                    // V = X*Vl + (1 - X)*Vv
                    //   = X*Vl + Vv - X*Vv
                    //   = X*(Vl - Vv) + Vv
                    // X = (V - Vv) / (Vl - Vv)
                    sat.put(Equilibrium.KEY_VFRAC,
                        (v - Vv) / (Vl - Vv));
                    return sat;
                }
            }
        }

        // We are not in VLE.  Return either a liquid or vapor state.
        double Vc = criticalVolume(compound);
        if (v > Vc) {
            // This is a vapor-like density.  Return the vapor() case.
            return vapor(compound, T, V);
        } else {
            // This is a liquid-like density.  Return the liquid() case.

            // NOTE THAT STATE(T, V) MIGHT RETURN A LIQUID IN THE SAME
            // SUPERCRITICAL PRESSURE REGIME THAT STATE(T, P) RETURNS A
            // VAPOR.
            return liquid(compound, T, V);
        }
    }

    /**
     * Determine the equilibrium properties of a pure compound (vapor or
     * liquid) at a specific pressure and volume.
     *
     * @param compound pure compound
     * @param P pressure
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium state(final Compound compound, final Pressure P,
        final Volume V) throws ThermoStateException {

        log.entry(compound, P, V);

        double v = V.m3mol();

        if (P.Pa() < compound.Pc().Pa()) {
            // This might be VLE.  Get the saturated properties to see.
            Equilibrium sat = vle(compound, P);
            if ((sat.LiquidV() != null) && (sat.VaporV() != null)) {
                double Vl = sat.LiquidV().m3mol();
                double Vv = sat.VaporV().m3mol();
                if ((v > Vl) && (v < Vv)) {
                    // We are in VLE.  What this means is that we have X
                    // moles of liquid at a higher density (Vl < V), and Y
                    // moles of vapor at a lower density (Vv > V):
                    //
                    // V = X*Vl + Y*Vv
                    // X + Y = 1
                    // V = X*Vl + (1 - X)*Vv
                    //   = X*Vl + Vv - X*Vv
                    //   = X*(Vl - Vv) + Vv
                    // X = (V - Vv) / (Vl - Vv)
                    sat.put(Equilibrium.KEY_VFRAC,
                        (v - Vv) / (Vl - Vv));
                    return sat;
                }
            }
        }

        double Vc = criticalVolume(compound);
        if (v > Vc) {
            // This is a vapor-like density.  Return the vapor() case.
            return vapor(compound, P, V);
        } else {
            // This is a liquid-like density.  Return the liquid() case.
            //
            // NOTE THAT STATE(P, V) MIGHT RETURN A LIQUID IN THE SAME
            // SUPERCRITICAL PRESSURE REGIME THAT STATE(T, P) RETURNS A
            // VAPOR.
            return liquid(compound, P, V);
        }
    }

    // Vapor ------------------------------------------------------------------

    /**
     * Determine the equilibrium properties of a pure compound (vapor phase)
     * at a specific temperature and pressure.
     *
     * @param compound pure compound
     * @param T temperature
     * @param P pressure
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium vapor(final Compound compound, final Temperature T,
        final Pressure P) throws ThermoStateException {

        log.entry(compound, T, P);

        double t = T.K();
        double p = P.Pa();
        double volume = R * t / p;

        // Save and return the results. ---------------------------------------

        Equilibrium result = new Equilibrium();
        result.put(Equilibrium.KEY_ITER, 1);
        result.put(Equilibrium.KEY_P, P);
        result.put(Equilibrium.KEY_T, T);

        // This is a vapor-only case.
        result.put(Equilibrium.KEY_PHASE, Phase.VAPOR);

        addVaporValues(result, compound, p, t, volume);

        // All done!
        return result;
    }

    /**
     * Determine the equilibrium properties of a pure compound (vapor phase)
     * at a specific temperature and volume.
     *
     * @param compound pure compound
     * @param T temperature
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium vapor(final Compound compound, final Temperature T,
        final Volume V) throws ThermoStateException {

        log.entry(compound, T, V);

        double t = T.K();
        double volume = V.m3mol();
        double p = R * t / volume;

        // Verify the bounds of T and V. --------------------------------------

        double Pc = compound.Pc().Pa();
        double Tc = compound.Tc().K();

        if ((p > Pc) && (t < Tc)) {
            // This is supposed to be a vapor state, which is not valid at
            // this temperature and pressure: supercritical pressure,
            // subcritical temperature.
            throw new ThermoStateException("Compound: " + compound +
                " T: " + T + " Tc: " + compound.Tc() +
                " P: " + p + " Pc: " + Pc + " Pa");
        }

        double Vc = criticalVolume(Tc, Pc);
        if (volume > Vc) {
            // This is supposed to be a vapor state, which is not valid at
            // this volume.
            throw new ThermoStateException("Compound: " + compound +
                " V: " + volume + " Vc: " + Vc + " m3/mol");
        }

        // Save and return the results. ---------------------------------------

        Equilibrium result = new Equilibrium();
        result.put(Equilibrium.KEY_ITER, 1);
        result.put(Equilibrium.KEY_P, Pressure.at(p, "Pa"));
        result.put(Equilibrium.KEY_T, T);

        // This is a vapor-only case.
        result.put(Equilibrium.KEY_PHASE, Phase.VAPOR);

        addVaporValues(result, compound, p, t, volume);

        // All done!
        return result;
    }

    /**
     * Determine the equilibrium properties of a pure compound (vapor phase)
     * at a specific pressure and volume.
     *
     * @param compound pure compound
     * @param P pressure
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium vapor(final Compound compound, final Pressure P,
        final Volume V) throws ThermoStateException {

        log.entry(compound, P, V);

        double Pc = compound.Pc().Pa();
        double Tc = compound.Tc().K();
        double p = P.Pa();
        double v = V.m3mol();

        // Verify the bounds of P and V. --------------------------------------

        double Vc = criticalVolume(Tc, Pc);
        if (v < Vc) {
            // This is supposed to be a vapor state, which is not valid at
            // this volume.
            throw new ThermoStateException("Compound: " + compound +
                " V: " + v + " Vc: " + Vc + " m3/mol");
        }

        double t = p * v / R;

        // Save and return the results. ---------------------------------------

        Equilibrium result = new Equilibrium();
        result.put(Equilibrium.KEY_ITER, 1);
        result.put(Equilibrium.KEY_P, P);
        result.put(Equilibrium.KEY_T, Temperature.at(t, "K"));

        // This is a vapor-only case.
        result.put(Equilibrium.KEY_PHASE, Phase.VAPOR);

        addVaporValues(result, compound, p, t, v);

        // All done!
        return result;
    }

    // Liquid -----------------------------------------------------------------

    /**
     * Determine the equilibrium properties of a pure compound (liquid phase)
     * at a specific temperature and pressure.
     *
     * @param compound pure compound
     * @param T temperature
     * @param P pressure
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium liquid(final Compound compound, final Temperature T,
        final Pressure P) throws ThermoStateException {

        log.entry(compound, T, P);

        double t = T.K();
        double Tr = T.K() / compound.Tc().K();

        // Verify the bounds of T and P. --------------------------------------

        if (T.compareTo(compound.Tc()) > 0) {
            // This is a liquid state, which is not valid at this temperature
            // (it is supercritical).
            throw new ThermoStateException("Compound: " + compound +
                " T: " + T + " Tc: " + compound.Tc());
        }
        if (T.compareTo(compound.Tp()) < 0) {
            // This is a liquid state, which is not valid at this temperature
            // (it is below the triple point).
            throw new ThermoStateException("Compound: " + compound +
                " T: " + T + " Tp: " + compound.Tp());
        }
        if (P.compareTo(compound.Pp()) < 0) {
            // This is a liquid state, which is not valid at this pressure
            // (it is below the triple point).
            throw new ThermoStateException("Compound: " + compound +
                " P: " + P + " Pp: " + compound.Pp());
        }

        double p = P.Pa();

        // Find the volume using Rackett.
        double volume = rackett(compound, Tr);

        // Save and return the results. ---------------------------------------

        Equilibrium result = new Equilibrium();
        result.put(Equilibrium.KEY_ITER, 1);
        result.put(Equilibrium.KEY_P, P);
        result.put(Equilibrium.KEY_T, T);

        // This is a liquid-only case.
        result.put(Equilibrium.KEY_PHASE, Phase.LIQUID);

        addLiquidValues(result, compound, p, t, volume);

        // All done!
        return result;
    }

    /**
     * Determine the equilibrium properties of a pure compound (liquid phase)
     * at a specific temperature and volume.
     *
     * @param compound pure compound
     * @param T temperature
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium liquid(final Compound compound, final Temperature T,
        final Volume V) throws ThermoStateException {

        log.entry(compound, T, V);

        // This cannot be determined by ideal method, since volume is
        // independent of pressure.
        throw new ThermoStateException("Ideal fluid volume is independent " +
            "of pressure");
    }

    /**
     * Determine the equilibrium properties of a pure compound (liquid phase)
     * at a specific pressure and volume.
     *
     * @param compound pure compound
     * @param P pressure
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium liquid(final Compound compound, final Pressure P,
        final Volume V) throws ThermoStateException {

        log.entry(compound, P, V);

        double Pc = compound.Pc().Pa();
        double Tc = compound.Tc().K();
        double p = P.Pa();
        double v = V.m3mol();

        // Verify the bounds of P and V. --------------------------------------

        double Vc = criticalVolume(Tc, Pc);
        if (v > Vc) {
            // This is supposed to be a liquid state, which is not valid at
            // this volume.
            throw new ThermoStateException("Compound: " + compound +
                " V: " + v + " Vc: " + Vc + " m3/mol");
        }

        // Find the T that matches Rackett.
        double t = invertRackett(compound, v);

        // Save and return the results. ---------------------------------------

        Equilibrium result = new Equilibrium();
        result.put(Equilibrium.KEY_ITER, 1);
        result.put(Equilibrium.KEY_P, P);
        result.put(Equilibrium.KEY_T, Temperature.at(t, "K"));

        // This is a liquid-only case.
        result.put(Equilibrium.KEY_PHASE, Phase.LIQUID);

        addLiquidValues(result, compound, p, t, v);

        // All done!
        return result;
    }

    // Vapor/Liquid Equilibrium -----------------------------------------------

    /**
     * Determine the equilibrium properties of a pure compound at a specific
     * saturated temperature.
     *
     * @param compound pure compound
     * @param T temperature
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium vle(final Compound compound, final Temperature T)
        throws ThermoStateException {

        log.entry(compound, T);

        double Tsat = T.K();

        // Use an Antoine relation to find Psat.
        Pressure Psat = antoine(compound, T);

        // Now that we have Psat, the rest is easy.
        double Vv = R * Tsat / Psat.Pa();
        double Tc = compound.Tc().K();
        double Vl = rackett(compound, Tsat / Tc);
        double Zv = 1.0;
        double Zl = Vl * Psat.Pa() / (R * Tsat);

        Equilibrium result = new Equilibrium();
        result.put(Equilibrium.KEY_ITER, 1);
        result.put(Equilibrium.KEY_PSAT, Psat);
        result.put(Equilibrium.KEY_TSAT, T);

        // This is a vapor-liquid equilibrium
        result.put(Equilibrium.KEY_PHASE, Phase.VLE);

        // Populate the volumes.  Note that this is volume, not density:
        // highest volume/mol is vapor.
        result.put(Equilibrium.KEY_VAPOR_V, Volume.at(Vv, "m3/mol"));
        result.put(Equilibrium.KEY_LIQUID_V, Volume.at(Vl, "m3/mol"));

        // Put the residual enthalpy values in.
        result.put(Equilibrium.KEY_VAPOR_Z, Zv);
        result.put(Equilibrium.KEY_LIQUID_Z, Zl);

        result.put(Equilibrium.KEY_LIQUID_GR, Gibbs.at(0, "J/mol"));
        result.put(Equilibrium.KEY_VAPOR_GR, Gibbs.at(0, "J/mol"));

        result.put(Equilibrium.KEY_LIQUID_HR, Enthalpy.at(0, "J/mol"));
        result.put(Equilibrium.KEY_VAPOR_HR, Enthalpy.at(0, "J/mol"));

        result.put(Equilibrium.KEY_LIQUID_SR, Entropy.at(0, "J/mol-K"));
        result.put(Equilibrium.KEY_VAPOR_SR, Entropy.at(0, "J/mol-K"));

        if (compound.Cp_IG_Function() != null) {
            // Compound has a heat capacity function, so go ahead and
            // put the real enthalpies in.
            double H_0 = compound.Hf_IG().Jmol();
            double H_v = compound.Cp_IG_Function().integrate(Constants.STP_T,
                T).Jmol();

            // Use Watson to find heat of vaporization
            double H_lv = watson(compound, Tsat);

            result.put(Equilibrium.KEY_LIQUID_H,
                Enthalpy.at(H_0 + H_v - H_lv, "J/mol"));
            result.put(Equilibrium.KEY_VAPOR_H,
                Enthalpy.at(H_0 + H_v, "J/mol"));
        }

        // All done!
        return result;
    }

    /**
     * Determine the equilibrium properties of a pure compound at a specific
     * saturated pressure.
     *
     * @param compound pure compound
     * @param P pressure
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium vle(final Compound compound, final Pressure P)
        throws ThermoStateException {

        log.entry(compound, P);

        // Verify the bounds of Psat.
        if (P.compareTo(compound.Pc()) > 0) {
            // This is a vapor-liquid equilibrium, which is not valid at this
            // pressure (it is supercritical).
            throw new ThermoStateException("Compound: " + compound +
                " P: " + P + " Pc: " + compound.Pc());
        }
        if (P.compareTo(compound.Pp()) < 0) {
            // This is a vapor-liquid equilibrium, which is not valid at this
            // pressure (it is below the triple point).
            throw new ThermoStateException("Compound: " + compound +
                " P: " + P + " Pp: " + compound.Pp());
        }

        // Use an Antoine relation to find Tsat.
        double Tsat = antoine(compound, P).K();

        // Now that we have Tsat, the rest is easy.
        double Vv = R * Tsat / P.Pa();
        double Tc = compound.Tc().K();
        double Vl = rackett(compound, Tsat / Tc);
        double Zv = 1.0;
        double Zl = Vl * P.Pa() / (R * Tsat);

        Equilibrium result = new Equilibrium();
        result.put(Equilibrium.KEY_ITER, 1);
        result.put(Equilibrium.KEY_PSAT, P);
        result.put(Equilibrium.KEY_TSAT, Temperature.at(Tsat, "K"));

        // This is a vapor-liquid equilibrium
        result.put(Equilibrium.KEY_PHASE, Phase.VLE);

        // Populate the volumes.  Note that this is volume, not density:
        // highest volume/mol is vapor.
        result.put(Equilibrium.KEY_VAPOR_V, Volume.at(Vv, "m3/mol"));
        result.put(Equilibrium.KEY_LIQUID_V, Volume.at(Vl, "m3/mol"));

        // Put the residual enthalpy values in.
        result.put(Equilibrium.KEY_VAPOR_Z, Zv);
        result.put(Equilibrium.KEY_LIQUID_Z, Zl);

        result.put(Equilibrium.KEY_LIQUID_GR, Gibbs.at(0, "J/mol"));
        result.put(Equilibrium.KEY_VAPOR_GR, Gibbs.at(0, "J/mol"));

        result.put(Equilibrium.KEY_LIQUID_HR, Enthalpy.at(0, "J/mol"));
        result.put(Equilibrium.KEY_VAPOR_HR, Enthalpy.at(0, "J/mol"));

        result.put(Equilibrium.KEY_LIQUID_SR, Entropy.at(0, "J/mol-K"));
        result.put(Equilibrium.KEY_VAPOR_SR, Entropy.at(0, "J/mol-K"));

        if (compound.Cp_IG_Function() != null) {
            // Compound has a heat capacity function, so go ahead and
            // put the real enthalpies in.
            double H_0 = compound.Hf_IG().Jmol();
            double H_v = compound.Cp_IG_Function().integrate(Constants.STP_T,
                Temperature.at(Tsat, "K")).Jmol();

            // Use Watson to find heat of vaporization
            double H_lv = watson(compound, Tsat);

            result.put(Equilibrium.KEY_LIQUID_H,
                Enthalpy.at(H_0 + H_v - H_lv, "J/mol"));
            result.put(Equilibrium.KEY_VAPOR_H,
                Enthalpy.at(H_0 + H_v, "J/mol"));
        }

        // All done!
        return result;
    }

    // Other ------------------------------------------------------------------

    /**
     * Get the relative convergence criteria.
     *
     * @return the convergence criteria
     */
    public final Convergence getConvergence() {
        return convergence;
    }

    /**
     * Set the relative convergence criteria.
     *
     * @param convergence the new convergence criteria
     */
    public final void setConvergence(final Convergence convergence) {
        this.convergence = convergence;
    }

    /**
     * Make human-readable description of this Method.
     *
     * @return displayable String
     */
    @Override
    public String toString() {
        return "Ideal Fluid";
    }

}
