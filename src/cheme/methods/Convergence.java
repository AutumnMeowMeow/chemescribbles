/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.methods;

/**
 * Convergence calculates various criteria used by methods to determine if
 * they have converged correctly or not.
 */
public class Convergence {

    /**
     * The relative difference between A and B to use to determine if they
     * are the same or not.
     */
    private double relativeDifference;

    /**
     * Get the relative difference used by isSame() and isZero().
     *
     * @return the difference
     */
    public final double getRelativeDifference() {
        return relativeDifference;
    }

    /**
     * Set the relative difference used by isSame() and isZero().
     *
     * @param epsilon the difference
     */
    public final void setRelativeDifference(final double epsilon) {
        relativeDifference = epsilon;
    }

    /**
     * Public contructor using default relative difference of 1e-6.
     */
    public Convergence() {
        relativeDifference = 1e-6;
    }

    /**
     * Public contructor.
     *
     * @param epsilon the relative difference to use for isSame() and
     * isZero()
     */
    public Convergence(final double epsilon) {
        relativeDifference = epsilon;
    }

    /**
     * Test if two values are equal to each other.
     *
     * @param a the first value
     * @param b the second value
     * @return true if the arguments are the same
     * @throws ArithmeticException if either a or b are NaN
     */
    public final boolean isSame(final double a,
        final double b) throws ArithmeticException {

        if (Double.isNaN(a)) {
            throw new ArithmeticException("A is NaN");
        }
        if (Double.isNaN(b)) {
            throw new ArithmeticException("B is NaN");
        }
        if (a == 0) {
            return (b < relativeDifference);
        }
        if (b == 0) {
            return (a < relativeDifference);
        }
        return (Math.abs(a - b) < (b * relativeDifference));
    }

    /**
     * Test if a value is zero.
     *
     * @param a the value
     * @return true if a is zero
     * @throws ArithmeticException if a is NaN
     */
    public final boolean isZero(final double a) throws ArithmeticException {
        if (Double.isNaN(a)) {
            throw new ArithmeticException("A is NaN");
        }
        if (a == 0) {
            return true;
        }
        return (Math.abs(a) < relativeDifference);
    }

}
