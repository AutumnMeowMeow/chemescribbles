/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.methods;

import java.util.List;

import cheme.compounds.Mixture;
import cheme.properties.Pressure;
import cheme.properties.Temperature;

/**
 * This interface defines various thermodynamic calculations.  Note that ALL
 * of these methods should be assumed to be NOT re-entrant.  Multiple threads
 * should instantiate separate instances to perform parallel calculations.
 */
public interface ActivityModel {

    /**
     * Determine the liquid-phase activity coefficients of a liquid
     * composition at a specific temperature and pressure.
     *
     * @param mixture the liquid-phase composition
     * @param T temperature
     * @param P pressure
     * @return the activity coefficients
     * @throws ThermoStateException if this state is not physically realizable
     */
    public List<Double> gamma(final Mixture mixture, final Temperature T,
        final Pressure P) throws ThermoStateException;

    /**
     * Determine the vapor-phase fugacity coefficients of a vapor composition
     * at a specific temperature and pressure.
     *
     * @param mixture the vapor-phase composition
     * @param T temperature
     * @param P pressure
     * @return the fugacity coefficients
     * @throws ThermoStateException if this state is not physically realizable
     */
    public List<Double> phi(final Mixture mixture, final Temperature T,
        final Pressure P) throws ThermoStateException;

    // Other ------------------------------------------------------------------

    /**
     * Get the relative convergence criteria.
     *
     * @return the convergence criteria
     */
    public Convergence getConvergence();

    /**
     * Set the relative convergence criteria.
     *
     * @param convergence the new convergence criteria
     */
    public void setConvergence(final Convergence convergence);

}
