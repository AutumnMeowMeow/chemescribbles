/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.methods;

import org.apache.logging.log4j.LogManager;

/**
 * This class performs various thermodynamic calculations via the
 * Redlich-Kwong equation of state method.  NOTE THAT THE PUBLIC FUNCTIONS
 * ARE NOT RE-ENTRANT.
 */
public class RedlichKwong extends CubicEOS {

    static {
        log     = LogManager.getLogger(RedlichKwong.class.getName());
    }

    /**
     * Constructor sets the CubicEOS parameters to be Redlich-Kwong.
     */
    public RedlichKwong() {
        omega   = 0.08664;
        epsilon = 0.0;
        sigma   = 1.0;
        psi     = 0.42748;
    }

    /**
     * Compute the alpha(Tr, w) function.
     *
     * @param Tr the reduced temperature
     * @param w the acentric factor
     * @return alpha
     */
    protected double alpha(final double Tr, final double w) {
        return 1.0 / Math.sqrt(Tr);
    }

    /**
     * Compute the d(ln(alpha(Tr, w)))/d(Tr) function.
     *
     * @param Tr the reduced temperature
     * @param w the acentric factor
     * @return alpha
     */
    protected double dLnAlpha(final double Tr, final double w) {
        return -0.5;
    }

    /**
     * Make human-readable description of this Method.
     *
     * @return displayable String
     */
    @Override
    public String toString() {
        return "Redlich-Kwong EOS";
    }

}
