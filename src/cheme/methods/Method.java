/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.methods;

import cheme.compounds.Compound;
import cheme.compounds.Mixture;
import cheme.properties.Equilibrium;
import cheme.properties.Pressure;
import cheme.properties.Temperature;
import cheme.properties.Volume;

/**
 * This interface defines various thermodynamic calculations.  Note that ALL
 * of these methods should be assumed to be NOT re-entrant.  Multiple threads
 * should instantiate separate instances to perform parallel calculations.
 */
public interface Method {

    // Mixture state ----------------------------------------------------------

    /**
     * Determine the equilibrium properties of a mixture (vapor or liquid) at
     * a specific temperature and pressure.
     *
     * @param mixture the mixture
     * @param T temperature
     * @param P pressure
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium state(final Mixture mixture, final Temperature T,
        final Pressure P) throws ThermoStateException;

    /**
     * Determine the equilibrium properties of a mixture (vapor or liquid) at
     * a specific temperature and volume.
     *
     * @param mixture the mixture
     * @param T temperature
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium state(final Mixture mixture, final Temperature T,
        final Volume V) throws ThermoStateException;

    /**
     * Determine the equilibrium properties of a mixture (vapor or liquid) at
     * a specific pressure and volume.
     *
     * @param mixture the mixture
     * @param P pressure
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium state(final Mixture mixture, final Pressure P,
        final Volume V) throws ThermoStateException;

    // General state ----------------------------------------------------------

    /**
     * Determine the equilibrium properties of a pure compound (vapor or
     * liquid) at a specific temperature and pressure.
     *
     * @param compound pure compound
     * @param T temperature
     * @param P pressure
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium state(final Compound compound, final Temperature T,
        final Pressure P) throws ThermoStateException;

    /**
     * Determine the equilibrium properties of a pure compound (vapor or
     * liquid) at a specific temperature and volume.
     *
     * @param compound pure compound
     * @param T temperature
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium state(final Compound compound, final Temperature T,
        final Volume V) throws ThermoStateException;

    /**
     * Determine the equilibrium properties of a pure compound (vapor or
     * liquid) at a specific pressure and volume.
     *
     * @param compound pure compound
     * @param P pressure
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium state(final Compound compound, final Pressure P,
        final Volume V) throws ThermoStateException;

    // Vapor ------------------------------------------------------------------

    /**
     * Determine the equilibrium properties of a pure compound (vapor phase)
     * at a specific temperature and pressure.
     *
     * @param compound pure compound
     * @param T temperature
     * @param P pressure
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium vapor(final Compound compound, final Temperature T,
        final Pressure P) throws ThermoStateException;

    /**
     * Determine the equilibrium properties of a pure compound (vapor phase)
     * at a specific temperature and volume.
     *
     * @param compound pure compound
     * @param T temperature
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium vapor(final Compound compound, final Temperature T,
        final Volume V) throws ThermoStateException;

    /**
     * Determine the equilibrium properties of a pure compound (vapor phase)
     * at a specific pressure and volume.
     *
     * @param compound pure compound
     * @param P pressure
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium vapor(final Compound compound, final Pressure P,
        final Volume V) throws ThermoStateException;

    // Liquid -----------------------------------------------------------------

    /**
     * Determine the equilibrium properties of a pure compound (liquid phase)
     * at a specific temperature and pressure.
     *
     * @param compound pure compound
     * @param T temperature
     * @param P pressure
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium liquid(final Compound compound, final Temperature T,
        final Pressure P) throws ThermoStateException;

    /**
     * Determine the equilibrium properties of a pure compound (liquid phase)
     * at a specific temperature and volume.
     *
     * @param compound pure compound
     * @param T temperature
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium liquid(final Compound compound, final Temperature T,
        final Volume V) throws ThermoStateException;

    /**
     * Determine the equilibrium properties of a pure compound (liquid phase)
     * at a specific pressure and volume.
     *
     * @param compound pure compound
     * @param P pressure
     * @param V volume
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium liquid(final Compound compound, final Pressure P,
        final Volume V) throws ThermoStateException;

    // Vapor/Liquid Equilibrium -----------------------------------------------

    /**
     * Determine the equilibrium properties of a pure compound at a specific
     * saturated temperature.
     *
     * @param compound pure compound
     * @param T temperature
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium vle(final Compound compound, final Temperature T)
        throws ThermoStateException;

    /**
     * Determine the equilibrium properties of a pure compound at a specific
     * saturated pressure.
     *
     * @param compound pure compound
     * @param P pressure
     * @return the equilibrium state
     * @throws ThermoStateException if this state is not physically realizable
     */
    public Equilibrium vle(final Compound compound, final Pressure P)
        throws ThermoStateException;

    // Other ------------------------------------------------------------------

    /**
     * Get the relative convergence criteria.
     *
     * @return the convergence criteria
     */
    public Convergence getConvergence();

    /**
     * Set the relative convergence criteria.
     *
     * @param convergence the new convergence criteria
     */
    public void setConvergence(final Convergence convergence);

}
