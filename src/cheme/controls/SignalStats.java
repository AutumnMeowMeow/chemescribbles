/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.controls;

/**
 * SignalStats tracks the progress of a signal with noise.
 */
public class SignalStats {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The total number of points in this signal.
     */
    private int count = 0;

    /**
     * The confidence interval to use for determining if a signal has changed
     * character.
     */
    private double confidence = 0.95;

    /**
     * The number of points to use for each time period.
     */
    private int cycleN = 30;

    /**
     * The Six Sigma upper control limit (UCL) corresponding to the given
     * confidence level, from the previous time period.
     */
    private double ucl = 0.0;

    /**
     * The Six Sigma lower control limit (LCL) corresponding to the given
     * confidence level, from the previous time period.
     */
    private double lcl = 0.0;

    /**
     * The signal data.
     */
    private Distribution signal = new Distribution();

    /**
     * The signal data slope.
     */
    private Distribution slope = new Distribution();

    /**
     * The data for the "slope-corrected" signal.
     */
    private Distribution slopedSignal = new Distribution();

    /**
     * The "slope-corrected" standard deviation.
     */
    private Distribution slopedSigma = new Distribution();

    /**
     * The signal data for the previous time period.
     */
    private Distribution previousSignal = new Distribution();

    /**
     * The signal data slope for the previous time period.
     */
    private Distribution previousSlope = new Distribution();

    /**
     * The "sloped-corrected" standard deviation for the previous time period.
     */
    private Distribution previousSlopedSigma = new Distribution();

    /**
     * The last value added.
     */
    private double lastX;

    /**
     * Rising, falling, or linear behavior.
     */
    private Behavior linearity = Behavior.UNKNOWN;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param confidence the percent of confidence, between 0.0 and 1.0
     * @param n the number of points to use for each time period
     */
    public SignalStats(final double confidence, final int n) {
        if ((confidence < 0.0) || (confidence > 1.0)) {
            throw new IllegalArgumentException("confidence interval " +
                confidence + " not between 0.0 and 1.0");
        }
        this.confidence = confidence;
        this.cycleN = n;
    }

    /**
     * Public constructor.  Assume a default confidence of 0.95 and 30 points
     * per time period.
     */
    public SignalStats() {
        this(0.95, 30);
    }

    // ------------------------------------------------------------------------
    // SignalStats ------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Get number of points for each time period.
     *
     * @return the number of points used in each time period
     */
    public int cycleSize() {
        return cycleN;
    }

    /**
     * Get number of points collected so far in this time period.
     *
     * @return the number of points so far in this time period
     */
    public int size() {
        assert (signal.size() == slope.size());
        return signal.size();
    }

    /**
     * Clear all data for this signal.
     */
    public void reset() {
        previousSignal.reset();
        previousSlope.reset();
        previousSlopedSigma.reset();
        signal.reset();
        slope.reset();
        slopedSignal.reset();
        slopedSigma.reset();
        ucl = 0.0;
        lcl = 0.0;
        count = 0;
    }

    /**
     * Add one point to this set.
     *
     * @param x the point to add
     * @return a behavior indication
     */
    public Behavior add(final double x) {
        count++;
        Behavior result = Behavior.OK;
        boolean behaviorChanged = false;

        if ((signal.size() == 0) && (previousSignal.size() == 0)) {
            // Very first point, just add it and bail out.
            // System.err.println("first point");
            signal.add(x);
            slope.add(0.0);
            lastX = x;
            return result;
        }

        assert (signal.size() > 0);

        if (signal.size() == cycleN) {
            // Check signal and previousSignal for change in behavior
            if ((previousSlope.getMeanUcl(confidence) > 0.0)
                && (previousSlope.getMeanLcl(confidence) < 0.0)
            ) {
                // We WERE linear.  Are we still linear?
                if ((slope.getMeanUcl(confidence) > 0.0)
                    && (slope.getMeanLcl(confidence) > 0.0)
                ) {
                    result = Behavior.LINE_RISE;
                    linearity = Behavior.RISING;
                    behaviorChanged = true;
                    System.err.println(count + " LINEAR --> RISE");
                } else if ((slope.getMeanUcl(confidence) < 0.0)
                    && (slope.getMeanLcl(confidence) < 0.0)
                ) {
                    result = Behavior.LINE_FALL;
                    linearity = Behavior.FALLING;
                    behaviorChanged = true;
                    System.err.println(count + " LINEAR --> FALL");
                }
            } else if ((previousSlope.getMeanUcl(confidence) > 0.0)
                && (previousSlope.getMeanLcl(confidence) > 0.0)
            ) {
                // We WERE rising.  Are we still rising?
                if ((slope.getMeanUcl(confidence) > 0.0)
                    && (slope.getMeanLcl(confidence) < 0.0)
                ) {
                    result = Behavior.RISE_LINE;
                    linearity = Behavior.LINEAR;
                    behaviorChanged = true;
                    System.err.println(count + " RISE --> LINEAR");
                } else if ((slope.getMeanUcl(confidence) < 0.0)
                    && (slope.getMeanLcl(confidence) < 0.0)
                ) {
                    result = Behavior.RISE_FALL;
                    linearity = Behavior.FALLING;
                    behaviorChanged = true;
                    System.err.println(count + " RISE --> FALL");
                }
            } else if ((previousSlope.getMeanUcl(confidence) < 0.0)
                && (previousSlope.getMeanLcl(confidence) < 0.0)
            ) {
                // We WERE falling.  Are we still falling?
                if ((slope.getMeanUcl(confidence) > 0.0)
                    && (slope.getMeanLcl(confidence) < 0.0)
                ) {
                    result = Behavior.FALL_LINE;
                    linearity = Behavior.LINEAR;
                    behaviorChanged = true;
                    System.err.println(count + " FALL --> LINEAR");
                } else if ((slope.getMeanUcl(confidence) > 0.0)
                    && (slope.getMeanLcl(confidence) > 0.0)
                ) {
                    result = Behavior.FALL_RISE;
                    linearity = Behavior.RISING;
                    behaviorChanged = true;
                    System.err.println(count + " FALL --> RISE");
                }
            }

            if (behaviorChanged == false) {
                // We are still characterized by the same rising, falling, or
                // linear behavior as before.  Are we noisier or quieter now?
                if (!slopedSigma.isSame(previousSlopedSigma, confidence)) {
                    // t-test says sigma has likely changed.
                    if (slopedSigma.getMean() > previousSlopedSigma.getMean()) {
                        result = Behavior.MORE_NOISE;
                        behaviorChanged = true;
                        System.err.println(count + " more noise");
                    } else {
                        result = Behavior.LESS_NOISE;
                        behaviorChanged = true;
                        System.err.println(count + " less noise");
                    }
                }
            }

            // Hang onto the control limits from the previous cycle.
            ucl = slopedSignal.getUcl(confidence) - slopedSignal.getMean();
            lcl = slopedSignal.getLcl(confidence) - slopedSignal.getMean();

            // Cycle to the new time period.
            previousSlopedSigma = slopedSigma;
            previousSignal = signal;
            previousSlope = slope;
            signal = new Distribution();
            slopedSignal = new Distribution();
            slopedSigma = new Distribution();
            slope = new Distribution();
        }
        signal.add(x);
        slope.add(x - lastX);
        lastX = x;

        if (previousSignal.size() >= cycleN) {
            // We have data from the previous cycle, use that for estimating
            // the current cycle's slope, UCL, and LCL.
            slopedSignal.add(x - (signal.size() * previousSlope.getMean()));
            slopedSigma.add(slopedSignal.getStdDev());

            // See if there was a full step change.
            if ((behaviorChanged == false) && (isLinear())) {
                if (!previousSignal.isSame(signal, confidence)) {
                    result = Behavior.STEP_CHANGE;
                    behaviorChanged = true;
                    System.err.println(count + " STEP CHANGE");
                }
            }

            // Check x against the UCL/LCL to trigger a response.
            if (behaviorChanged == false) {
                if (x > getUcl()) {
                    result = Behavior.OUTLIER_HIGH;
                    behaviorChanged = true;
                    System.err.println(count + " outlier HIGH");
                } else if (x < getLcl()) {
                    result = Behavior.OUTLIER_LOW;
                    behaviorChanged = true;
                    System.err.println(count + " outlier LOW");
                }
            }
        } else {
            // Still collecting data for the two cycles.
            System.err.println("still collecting on previous cycle");
        }

        return result;
    }

    /**
     * Get the current slope.
     *
     * @return the slope
     */
    public double getSlope() {
        return (slope.getMean());
    }

    /**
     * Compute the previous cycle slope-corrected "expected" mean.
     *
     * @return the expected mean
     */
    public double getMean() {
        return (slopedSignal.getMean() +
            (signal.size() * previousSlope.getMean()));
    }

    /**
     * Compute the standard deviation.
     *
     * @return the standard deviation
     */
    public double getStdDev() {
        return slopedSignal.getStdDev();
    }

    /**
     * Compute Six Sigma lower control limit (LCL).
     *
     * @return the lower control limit for this signal
     */
    public double getLcl() {
        return lcl + getMean();
    }

    /**
     * Compute Six Sigma upper control limit (UCL).
     *
     * @return the upper control limit for this signal
     */
    public double getUcl() {
        return ucl + getMean();
    }

    /**
     * Returns true if the signal is known to be linear.
     *
     * @return true if linear, false if not or unknown
     */
    public boolean isLinear() {
        if (linearity == Behavior.LINEAR) {
            return true;
        }
        return false;
    }

}
