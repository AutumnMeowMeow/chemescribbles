/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.controls;

/**
 * The PIDController implements proportional/integral/derivative control.
 *
 * <p>Note that ALL of these methods should be assumed to be NOT re-entrant.
 * Multiple threads should instantiate separate instances to perform parallel
 * calculations.
 */
public class PIDController implements Controller {

    /**
     * The present error value: the difference between PV and SP.
     */
    private double errorNow;

    /**
     * The previous scan error value: the difference between PV and SP.
     */
    private double errorLast;

    /**
     * The number of seconds since compute() was last called.
     */
    private double lastComputeTime;

    /**
     * The controller output value, as a fraction of its span.
     */
    private double op;

    /**
     * The OP span range minimum.
     */
    private double opSpanLower;

    /**
     * The OP span range maximum.
     */
    private double opSpanUpper;

    /**
     * The present process value, as a fraction of its span.
     */
    private double pv;

    /**
     * The PV span range minimum.
     */
    private double pvSpanLower;

    /**
     * The PV span range maximum.
     */
    private double pvSpanUpper;

    /**
     * The set point, as a fraction of its span.
     */
    private double sp;

    /**
     * The SP span range minimum.
     */
    private double spSpanLower;

    /**
     * The SP span range maximum.
     */
    private double spSpanUpper;

    /**
     * The SP allowed minimum.
     */
    private double spAllowedMin;

    /**
     * The SP allowed maximum.
     */
    private double spAllowedMax;

    /**
     * Construct a new PID controller.
     *
     * @param Kd the proportional gain
     * @param Ti the integral time
     * @param Td the derivative time
     * @param pvSpanMin the min span on PV
     * @param pvSpanMax the max span on PV
     * @param spSpanMin the min span on SP
     * @param spSpanMax the max span on SP
     * @param opSpanMin the min span on OP
     * @param opSpanMax the max span on OP
     * @param opAllowedMin the min allowed on OP
     * @param opAllowedMax the max allowed on OP
     * @throws IllegalArgumentException if op allowed min/max is outside its
     * span min/max
     */
    

    /**
     * Compute the new state of this controller.
     *
     * @param deltaT the number of seconds between the last call to compute()
     * and this call to compute()
     */
    public void compute(final double deltaT) {
        /*
         TODO

        errorLast = errorNow;
        error_now = sp - pv;
        op = op + K*((error_now - error_last) + error_now/Ti) / xmitter_range;
        if (op < opSpanLower) {
            op = opSpanLower;
        }
        if (op > opSpanUpper) {
            op = opSpanUpper;
        }
         */
    }

    /**
     * Get the current output (OP).
     *
     * @return the controller output, as a fraction of its span (0 to 1)
     */
    public double getOutput() {
        return op;
    }

    /**
     * Get the output (OP) span lower limit.
     *
     * @return the controller output's minimum value
     */
    public double getOutputSpanLower() {
        return opSpanLower;
    }

    /**
     * Set the output (OP) span lower limit.
     *
     * @param lower the controller output's minimum value
     */
    public void setOutputSpanLower(final double lower) {
        opSpanLower = lower;
    }

    /**
     * Get the output (OP) span upper limit.
     *
     * @return the controller output's maximum value
     */
    public double getOutputSpanUpper() {
        return opSpanUpper;
    }

    /**
     * Set the output (OP) span upper limit.
     *
     * @param upper the controller output's maximum value
     */
    public void setOutputSpanUpper(final double upper) {
        opSpanUpper = upper;
    }

    /**
     * Get the current input (PV).
     *
     * @return the controller input, as a fraction of its span (0 to 1)
     */
    public double getInput() {
        return pv;
    }

    /**
     * Set the current input (PV).
     *
     * @param input the controller input, as a fraction of its span (0 to 1)
     */
    public void setInput(final double input) {
        pv = input;
    }

    /**
     * Get the input (PV) span lower limit.
     *
     * @return the controller input's minimum value
     */
    public double getInputSpanLower() {
        return pvSpanLower;
    }

    /**
     * Set the input (PV) span lower limit.
     *
     * @param lower the controller input's minimum value
     */
    public void setInputSpanLower(final double lower) {
        pvSpanLower = lower;
    }

    /**
     * Get the input (PV) span upper limit.
     *
     * @return the controller input's maximum value
     */
    public double getInputSpanUpper() {
        return pvSpanUpper;
    }

    /**
     * Set the input (PV) span upper limit.
     *
     * @param upper the controller input's maximum value
     */
    public void setInputSpanUpper(final double upper) {
        pvSpanUpper = upper;
    }

    /**
     * Get the current set point (SP).
     *
     * @return the controller set point, as a fraction of its span (0 to 1)
     */
    public double getSetPoint() {
        return sp;
    }

    /**
     * Set the current set point (SP).
     *
     * @param setPoint the controller set point, as a fraction of its span (0
     * to 1)
     */
    public void setSetPoint(final double setPoint) {
        sp = setPoint;
    }

    /**
     * Get the set point (SP) span lower limit.
     *
     * @return the controller set point's minimum value
     */
    public double getSetPointSpanLower() {
        return spSpanLower;
    }

    /**
     * Set the set point (SP) span lower limit.
     *
     * @param lower the controller set point's minimum value
     */
    public void setSetPointSpanLower(final double lower) {
        spSpanLower = lower;
    }

    /**
     * Get the set point (SP) span upper limit.
     *
     * @return the controller set point's maximum value
     */
    public double getSetPointSpanUpper() {
        return spSpanUpper;
    }

    /**
     * Set the set point (SP) span upper limit.
     *
     * @param upper the controller set point's maximum value
     */
    public void setSetPointSpanUpper(final double upper) {
        spSpanUpper = upper;
    }

    /**
     * Get the set point (SP) minimum allowed value.
     *
     * @return the controller set point's minimum allowed value
     */
    public double getSetPointMin() {
        return spAllowedMin;
    }

    /**
     * Set the set point (SP) minimum allowed value.
     *
     * @param min the controller set point's minimum allowed value
     */
    public void setSetPointMin(final double min) {
        spAllowedMin = min;
    }

    /**
     * Get the set point (SP) span maximum allowed value.
     *
     * @return the controller set point's maximum allowed value
     */
    public double getSetPointMax() {
        return spAllowedMax;
    }

    /**
     * Set the set point (SP) span maximum allowed value.
     *
     * @param max the controller set point's maximum allowed value
     */
    public void setSetPointMax(final double max) {
        spAllowedMax = max;
    }

}
