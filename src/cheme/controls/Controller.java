/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.controls;

/**
 * A Controller represents a control element for a dynamic process.  The
 * controller computes a desired output (OP) given a present value (PV) and a
 * desired set point (SP).
 *
 * <p>Note that ALL of these methods should be assumed to be NOT re-entrant.
 * Multiple threads should instantiate separate instances to perform parallel
 * calculations.
 */
public interface Controller {

    /**
     * Get the current output (OP).
     *
     * @return the controller output, as a fraction of its span (0 to 1)
     */
    double getOutput();

    /**
     * Get the output (OP) span lower limit.
     *
     * @return the controller output's minimum value
     */
    double getOutputSpanLower();

    /**
     * Set the output (OP) span lower limit.
     *
     * @param lower the controller output's minimum value
     */
    void setOutputSpanLower(final double lower);

    /**
     * Get the output (OP) span upper limit.
     *
     * @return the controller output's maximum value
     */
    double getOutputSpanUpper();

    /**
     * Set the output (OP) span upper limit.
     *
     * @param upper the controller output's maximum value
     */
    void setOutputSpanUpper(final double upper);

    /**
     * Get the current input (PV).
     *
     * @return the controller input, as a fraction of its span (0 to 1)
     */
    double getInput();

    /**
     * Set the current input (PV).
     *
     * @param input the controller input, as a fraction of its span (0 to 1)
     */
    void setInput(final double input);

    /**
     * Get the input (PV) span lower limit.
     *
     * @return the controller input's minimum value
     */
    double getInputSpanLower();

    /**
     * Set the input (PV) span lower limit.
     *
     * @param lower the controller input's minimum value
     */
    void setInputSpanLower(final double lower);

    /**
     * Get the input (PV) span upper limit.
     *
     * @return the controller input's maximum value
     */
    double getInputSpanUpper();

    /**
     * Set the input (PV) span upper limit.
     *
     * @param upper the controller input's maximum value
     */
    void setInputSpanUpper(final double upper);

    /**
     * Get the current set point (SP).
     *
     * @return the controller set point, as a fraction of its span (0 to 1)
     */
    double getSetPoint();

    /**
     * Set the current set point (SP).
     *
     * @param setPoint the controller set point, as a fraction of its span (0
     * to 1)
     */
    void setSetPoint(final double setPoint);

    /**
     * Get the set point (SP) span lower limit.
     *
     * @return the controller set point's minimum value
     */
    double getSetPointSpanLower();

    /**
     * Set the set point (SP) span lower limit.
     *
     * @param lower the controller set point's minimum value
     */
    void setSetPointSpanLower(final double lower);

    /**
     * Get the set point (SP) span upper limit.
     *
     * @return the controller set point's maximum value
     */
    double getSetPointSpanUpper();

    /**
     * Set the set point (SP) span upper limit.
     *
     * @param upper the controller set point's maximum value
     */
    void setSetPointSpanUpper(final double upper);

    /**
     * Get the set point (SP) minimum allowed value.
     *
     * @return the controller set point's minimum allowed value
     */
    double getSetPointMin();

    /**
     * Set the set point (SP) minimum allowed value.
     *
     * @param min the controller set point's minimum allowed value
     */
    void setSetPointMin(final double min);

    /**
     * Get the set point (SP) span maximum allowed value.
     *
     * @return the controller set point's maximum allowed value
     */
    double getSetPointMax();

    /**
     * Set the set point (SP) span maximum allowed value.
     *
     * @param max the controller set point's maximum allowed value
     */
    void setSetPointMax(final double max);

    /**
     * Compute the new state of this controller.
     *
     * @param deltaT the number of seconds between the last call to compute()
     * and this call to compute()
     */
    void compute(final double deltaT);

}
