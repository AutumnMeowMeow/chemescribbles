/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.controls;

/**
 * Distribution uses the Welford online algorithm to compute mean, variance,
 * and standard deviation for a set of data points, and provides additional
 * functions for estimating confidence intervals, control limits, and a
 * t-test.
 */
public class Distribution {

    // ------------------------------------------------------------------------
    // Constants --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The number of standard deviations to use in computing the Six Sigma
     * UCL and LCL.
     */
    private static final double controlLimitK = 3.0;

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The number of points in this set.
     */
    private int count = 0;

    /**
     * The arithmetic mean value for this set of points.
     */
    private double mean = 0.0;

    /**
     * The accumulated squared distances from the mean for this set of
     * points.
     */
    private double sumOfDeltas = 0.0;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // Distribution -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Compute the gamma function using Lanczos' approximation.
     *
     * @param x the parameter for the gamma function
     */
    private static double gamma(double x) {
        final int g = 7;
        if (x < 0.5) {
            return (Math.PI / (Math.sin(Math.PI * x) * gamma(1.0 - x)));
        }
        x -= 1.0;
        double a = 0.99999999999980993;
        final double t = x + g + 0.5;
        a +=    676.5203681218851    / (x + 1);
        a +=  -1259.1392167224028    / (x + 2);
        a +=   771.32342877765313    / (x + 3);
        a +=  -176.61502916214059    / (x + 4);
        a +=   12.507343278686905    / (x + 5);
        a += -0.13857109526572012    / (x + 6);
        a +=   9.9843695780195716e-6 / (x + 7);
        a +=   1.5056327351493116e-7 / (x + 8);

        return (Math.sqrt(2 * Math.PI) * Math.pow(t, x + 0.5) *
            Math.exp(-t) * a);
    }

    /**
     * Compute the error function using Winitzki's approximation.  This
     * approximation is very fast, but has relative error up to 4e-4.
     *
     * References:
     *
     *    Sergei Winitzki, "A handy approximation for the error function and
     *    its inverse".  February 6, 2008.
     *
     * @param x the parameter for the error function
     */
    private static double erf(final double x) {
        // double a = 8 * (Math.PI - 3) / (3 * Math.PI * (4 - Math.PI));
        double a = 0.147;
        double num = (4 / Math.PI) + a * Math.pow(Math.abs(x), 2);
        double denom = 1 + a * Math.pow(Math.abs(x), 2);
        double term = -Math.pow(Math.abs(x), 2) * num / denom;
        if (x < 0) {
            // Use the fact that erf(-x) = -erf(x)
            return -Math.sqrt(1 - Math.exp(term));
        }
        return Math.sqrt(1 - Math.exp(term));
    }

    /**
     * Compute the density value (PDF) for the Student t-distribution.
     *
     * @param t the t value
     * @param v the degrees of freedom
     */
    private static double tDensity(final double t, final int v) {
        double term1 = 1.0 / (Math.sqrt(Math.PI * v));
        double term2 = gamma((v + 1.0) / 2.0) / gamma(v / 2.0);
        double term3 = 1.0 / (Math.pow(1 + t * t / v, (v + 1.0) / 2.0));
        return (term1 * term2 * term3);
    }

    /**
     * Compute the cumulative density value (CDF) for the Student
     * t-distribution.
     *
     * @param t the t value
     * @param v the degrees of freedom
     */
    private static double tCumulative(final double t, final int v) {
        if (v < 1) {
            throw new IllegalArgumentException("v (DOF) must be >= 1");
        }

        /*
         * The t-CDF function is commonly computed either by a series sum of
         * fractions for the incomplete beta function, or by some really old
         * transliterated Fortran code that is often cargo culted (the C code
         * on Rosetta stone for example has an unreachable section).  I would
         * rather rely on something that is actually understood, even if less
         * accurate.  I also want to avoid iteration or recursion, so that
         * this can be more easily transferred to other platforms.
         *
         * This function uses several correct analytical solutions and some
         * other approximations for the t-CDF for specific values of v, and
         * then switches to the normal CDF for v > 20.
         *
         * References:
         *
         *    William T. Shaw, "New Methods for Managing "Student's" T
         *    Distribution".  April 20, 2006.
         */
        if (v == 1) {
            // Cauchy distribution formula
            return (0.5 + (1 / Math.PI) * Math.atan(t));
        } else if (v == 2) {
            // Shaw
            return (0.5 + t / (2 * Math.sqrt(t * t + 2)));
        } else if (v == 3) {
            // Shaw
            double term1 = (1 / Math.PI) * Math.atan(t / Math.sqrt(3));
            double term2 = t * Math.sqrt(3) / (Math.PI * (t * t + 3));
            return (0.5 + term1 + term2);
        } else if (v == 4) {
            // Shaw
            double num = t * (t * t + 6);
            double denom = 2 * Math.pow((t * t + 4), 1.5);
            return (0.5 + num / denom);
        } else if (v == 5) {
            // Shaw
            double term1 = (1 / Math.PI) * Math.atan(t / Math.sqrt(5));
            double term2num = t * Math.sqrt(5) * (3 * t * t + 25);
            double term2denom = 3 * Math.PI * Math.pow((t * t + 5), 2);
            return (0.5 + term1 + term2num / term2denom);
        } else if (v == 6) {
            // Shaw
            double num = t * (2 * Math.pow(t, 4) + 30 * t * t + 135);
            double denom = 4 * Math.pow((t * t + 6), 2.5);
            return (0.5 + num / denom);
        } else if (v <= 10) {
            // A custom fit to data generated at v = 10.

            /*
             * We fitted a function of the form exp(-t) + t^n + c to this
             * distribution in the range 0.0 to 4.0.  It is hideous but
             * better than the normal distribution in this range.
             */
            double a = 0.0;
            double sign = 1.0;
            double x = t;
            if (t >= 4.0) {
                return 1.0;
            } else if (t <= -4.0) {
                return 0.0;
            }
            if (t < 0.0) {
                x = -t;
                sign = -1.0;
                a = 1.0;
            }

            double c1 = -1.264349;
            double c2 = -1.169755;
            double c3 = 0.044388;
            double c4 = 0.969324;
            double c5 = 2.162842;
            double c6 = 1.587432;
            double c7 = -2.340341;

            double result = a;
            result += sign * ((c1 * Math.exp(c2 * (x + c3))) + c4 +
                c5 * Math.pow(x + c6, c7));
            if (result > 1.0) {
                result = 1.0;
            }
            return result;
        } else if (v <= 15) {
            // A custom fit to data generated at v = 15.

            /*
             * We fitted a function of the form exp(-t) + t^n + c to this
             * distribution in the range 0.0 to 4.0.  It is hideous but
             * better than the normal distribution in this range.
             */
            double a = 0.0;
            double sign = 1.0;
            double x = t;
            if (t >= 4.0) {
                return 1.0;
            } else if (t <= -4.0) {
                return 0.0;
            }
            if (t < 0.0) {
                x = -t;
                sign = -1.0;
                a = 1.0;
            }

            double c1 = -0.850637;
            double c2 = -1.193139;
            double c3 = -0.408488;
            double c4 = 0.970054;
            double c5 = 6.308151;
            double c6 = 1.967315;
            double c7 = -2.849346;

            double result = a;
            result += sign * ((c1 * Math.exp(c2 * (x + c3))) + c4 +
                c5 * Math.pow(x + c6, c7));
            if (result > 1.0) {
                result = 1.0;
            }
            return result;
        } else if (v <= 20) {
            // A custom fit to data generated at v = 20.

            /*
             * We fitted a function of the form exp(-t) + t^n + c to this
             * distribution in the range 0.0 to 4.0.  It is hideous but
             * better than the normal distribution in this range.
             */
            double a = 0.0;
            double sign = 1.0;
            double x = t;
            if (t >= 4.0) {
                return 1.0;
            } else if (t <= -4.0) {
                return 0.0;
            }
            if (t < 0.0) {
                x = -t;
                sign = -1.0;
                a = 1.0;
            }

            double c1 = -0.848908;
            double c2 = -1.209134;
            double c3 = -0.486336;
            double c4 = 0.971544;
            double c5 = 16.575950;
            double c6 = 2.298452;
            double c7 = -3.304500;

            double result = a;
            result += sign * ((c1 * Math.exp(c2 * (x + c3))) + c4 +
                c5 * Math.pow(x + c6, c7));
            if (result > 1.0) {
                result = 1.0;
            }
            return result;
        }

        // Everything with v > 20 uses the normal distribution, using our
        // approximation for erf.
        return (0.5 * (1 + erf(t / Math.sqrt(2))));
    }

    /**
     * Clear all data for this set of points.
     */
    public void reset() {
        count = 0;
        mean = 0.0;
        sumOfDeltas = 0.0;
    }

    /**
     * Add one point to this set.
     *
     * @param x the point to add
     */
    public void add(final double x) {
        if (Double.isNaN(x) || Double.isInfinite(x)) {
            return;
        }
        count++;
        double delta = x - mean;
        mean += delta / count;
        double delta2 = x - mean;
        sumOfDeltas += (delta * delta2);
    }

    /**
     * Get number of points in this set.
     *
     * @return the number of points in this set
     */
    public int size() {
        return count;
    }

    /**
     * Get the mean value for this distribution.
     *
     * @return the arithmetic mean
     */
    public double getMean() {
        return mean;
    }

    /**
     * Get the variance for this distribution.
     *
     * @return the variance
     */
    public double getVariance() {
        if (count < 2) {
            return Double.NaN;
        }
        return sumOfDeltas / (count - 1);
    }

    /**
     * Get the standard deviation for this distribution.
     *
     * @return the standard deviation
     */
    public double getStdDev() {
        if (count < 2) {
            return Double.NaN;
        }
        return Math.sqrt(sumOfDeltas / (count - 1));
    }

    /**
     * Check if this distribution is the "same" as another, i.e. perform
     * Welch's t-test to see if the two distributions have a different mean
     * within a given confidence interval.
     *
     * @param other the other distribution
     * @param confidence the percent of confidence, between 0.0 and 1.0
     * @return true if these two distributions are not statistically
     * different at this confidence level
     */
    public boolean isSame(final Distribution other, final double confidence) {
        if ((confidence < 0.0) || (confidence > 1.0)) {
            throw new IllegalArgumentException("confidence interval " +
                confidence + " not between 0.0 and 1.0");
        }

        double s1_2 = getVariance();
        double s2_2 = other.getVariance();
        double denom = (s1_2 / count) + (s2_2 / other.count);
        double t = (mean - other.mean) / Math.sqrt(denom);
        double v1 = count - 1;
        double v2 = other.count - 1;
        double denom2 = Math.pow(s1_2, 2) / (Math.pow(count, 2) * v1);
        denom2 += Math.pow(s2_2, 2) / (Math.pow(other.count, 2) * v2);
        int v = (int) Math.round(Math.pow(denom, 2) / denom2);
        if (v < 1) {
            // Not enough data to know, bail out.
            return true;
        }

        /*
         * We now have t and v for a t-test.  Find the p-value.
         */
        double p = 2 * tCumulative(-Math.abs(t), v);

        /*
        System.err.println("t = " + t);
        System.err.println("df = " + v);
        System.err.println("p = " + p);
        */

        if (p < (1.0 - confidence)) {
            return false;
        }
        return true;
    }

    /**
     * Compute the lower limit of the mean corresponding to a given
     * confidence level.
     *
     * @param confidence the percent of confidence, between 0.0 and 1.0
     * @return the lower control limit for this set of points
     */
    public double getMeanLcl(final double confidence) {
        if ((confidence < 0.0) || (confidence > 1.0)) {
            throw new IllegalArgumentException("confidence interval " +
                confidence + " not between 0.0 and 1.0");
        }

        double p = confidence;
        double z1 = 1.0;
        if (p < 0.5) {
            p = 1.0 - p;
            z1 = -1.0;
        }

        // We use Shore to approximate the inverse normal distribution CDF.
        double z = z1 * 5.5556 * (1.0 - Math.pow((1.0 - p) / p, 0.1186));

        return (mean - z * getStdDev() / Math.sqrt(count));
    }

    /**
     * Compute the upper limit of the mean corresponding to a given
     * confidence level.
     *
     * @param confidence the percent of confidence, between 0.0 and 1.0
     * @return the upper control limit for this set of points
     */
    public double getMeanUcl(final double confidence) {
        if ((confidence < 0.0) || (confidence > 1.0)) {
            throw new IllegalArgumentException("confidence interval " +
                confidence + " not between 0.0 and 1.0");
        }

        double p = confidence;
        double z1 = 1.0;
        if (p < 0.5) {
            p = 1.0 - p;
            z1 = -1.0;
        }

        // We use Shore to approximate the inverse normal distribution CDF.
        double z = z1 * 5.5556 * (1.0 - Math.pow((1.0 - p) / p, 0.1186));

        return (mean + z * getStdDev() / Math.sqrt(count));
    }

    /**
     * Compute Six Sigma lower control limit (LCL) corresponding to a given
     * confidence level.
     *
     * @param confidence the percent of confidence, between 0.0 and 1.0
     * @return the lower control limit for this set of points
     */
    public double getLcl(final double confidence) {
        if ((confidence < 0.0) || (confidence > 1.0)) {
            throw new IllegalArgumentException("confidence interval " +
                confidence + " not between 0.0 and 1.0");
        }

        return (getMeanLcl(confidence) - controlLimitK * getStdDev());
    }

    /**
     * Compute Six Sigma upper control limit (UCL) corresponding to a given
     * confidence level.
     *
     * @param confidence the percent of confidence, between 0.0 and 1.0
     * @return the upper control limit for this set of points
     */
    public double getUcl(final double confidence) {
        if ((confidence < 0.0) || (confidence > 1.0)) {
            throw new IllegalArgumentException("confidence interval " +
                confidence + " not between 0.0 and 1.0");
        }

        return (getMeanUcl(confidence) + controlLimitK * getStdDev());
    }

}
