/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.controls;

/**
 * Behavior contains responses for SignalStats.add() and
 * isLinear()/isRising()/isFalling().
 */
public enum Behavior {

    /**
     * Signal is too short to be characterized.
     */
    UNKNOWN,

    /**
     * Signal appears linear.
     */
    LINEAR,

    /**
     * Signal appears to be rising.
     */
    RISING,

    /**
     * Signal appears to be falling.
     */
    FALLING,

    /**
     * Normal data point, nothing interesting happening.
     */
    OK,

    /**
     * Significant step change.
     */
    STEP_CHANGE,

    /**
     * The signal has gotten noticeably noisier.
     */
    MORE_NOISE,

    /**
     * The signal has gotten noticeably less noisy.
     */
    LESS_NOISE,

    /**
     * This point is outside the expected upper control limit.
     */
    OUTLIER_HIGH,

    /**
     * This point is outside the expected lower control limit.
     */
    OUTLIER_LOW,

    /**
     * The signal was lined out, but is now rising.
     */
    LINE_RISE,

    /**
     * The signal was rising, but is now lined out.
     */
    RISE_LINE,

    /**
     * The signal was lined out, but is now falling.
     */
    LINE_FALL,

    /**
     * The signal was falling, but is now lined out.
     */
    FALL_LINE,

    /**
     * The signal was rising, but is now falling.
     */
    RISE_FALL,

    /**
     * The signal was falling, but is now rising.
     */
    FALL_RISE,

}
