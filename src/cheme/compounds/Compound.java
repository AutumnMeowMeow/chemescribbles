/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.compounds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import cheme.compounds.functions.AntoineEquation;
import cheme.compounds.functions.CombinedHeatCapacityFunction;
import cheme.compounds.functions.HeatCapacityFunction;
import cheme.compounds.functions.IdealGasHeatCapacityPolynomial;
import cheme.compounds.functions.LiquidHeatCapacityDIPPR107;
import cheme.compounds.functions.VaporPressureFunction;
import cheme.methods.Method;
import cheme.methods.ThermoStateException;
import cheme.properties.Constants;
import cheme.properties.Enthalpy;
import cheme.properties.Entropy;
import cheme.properties.Equilibrium;
import cheme.properties.HeatCapacity;
import cheme.properties.Pressure;
import cheme.properties.Temperature;
import cheme.properties.Volume;
import cheme.util.CSVFile;

/**
 * This class defines a chemical compound and its pure-species properties.
 */
public class Compound {

    /**
     * The Logger to emit to.
     */
    private static Logger log = LogManager.getLogger(Compound.class.getName());

    // Compound properties database -------------------------------------------

    /**
     * The list of recognized aliases.
     */
    private static CSVFile allCompoundAliases;

    /**
     * The list of known compounds.
     */
    private static CSVFile allCompounds;

    /**
     * Coefficients for IdealGasHeatCapacityPolynomial.
     */
    private static CSVFile allIdealGasHeatCapacityPolynomials;

    /**
     * Coefficients for LiquidHeatCapacityDIPPR107.
     */
    private static CSVFile allLiquidHeatCapacityDIPPR107s;

    /**
     * Coefficients for AntoineEquation.
     */
    private static CSVFile allAntoineEquations;

    static {
        try {
            allCompounds = new CSVFile(ClassLoader.getSystemClassLoader().
                getResourceAsStream("Compounds.csv"));

            allCompoundAliases = new CSVFile(ClassLoader.getSystemClassLoader().
                getResourceAsStream("CompoundAliases.csv"));

            allIdealGasHeatCapacityPolynomials = new CSVFile(ClassLoader.
                getSystemClassLoader().
                getResourceAsStream("IdealGasHeatCapacityPolynomial.csv"));

            allLiquidHeatCapacityDIPPR107s = new CSVFile(ClassLoader.
                getSystemClassLoader().
                getResourceAsStream("LiquidHeatCapacityDIPPR107.csv"));

            allAntoineEquations = new CSVFile(ClassLoader.
                getSystemClassLoader().
                getResourceAsStream("AntoineEquation.csv"));

        } catch (IOException e) {
            log.error(e, e);
        }
    }

    // Single compound properties ---------------------------------------------

    /**
     * The critical temperature for this compound.
     */
    private Temperature criticalTemperature;

    /**
     * The critical pressure for this compound.
     */
    private Pressure criticalPressure;

    /**
     * The acentric (Pitzer correction) for this compound.
     */
    private double acentricFactor;

    /**
     * The critical volume for this compound.
     */
    private Volume criticalVolume;

    /**
     * The critical compressibility factor for this compound.
     */
    private double criticalCompressibility;

    /**
     * The triple-point temperature for this compound.
     */
    private Temperature triplePointTemperature;

    /**
     * The triple-point pressure for this compound.
     */
    private Pressure triplePointPressure;

    /**
     * The molecular weight for this compound.
     */
    private double molecularWeight;

    /**
     * The list of atom+count making up this compound.
     */
    private ArrayList<ArrayList<Object>> formula;

    /**
     * The ideal-gas heat capacity function to use.
     */
    private HeatCapacityFunction idealGasHeatCapacityFunction;

    /**
     * The liquid heat capacity function to use.
     */
    private HeatCapacityFunction liquidHeatCapacityFunction;

    /**
     * The liquid vapor pressure function to use.
     */
    private VaporPressureFunction vaporPressureFunction;

    /**
     * The ideal-gas heat of formation for this compound at 25 C (298.15 K)
     * and 1 atm (101325 Pa).
     */
    private Enthalpy idealGasHeatOfFormation;

    /**
     * The ideal-gas entropy of formation for this compound at 25 C (298.15
     * K) and 1 atm (101325 Pa).
     */
    private Entropy idealGasEntropyOfFormation;

    /**
     * The normal boiling point temperature (P = 1 atm) for this compound.
     */
    private Temperature normalBoilingPointTemperature;

    /**
     * The heat of vaporization for this compound at 25 C (298.15 K) and 1
     * atm (101325 Pa).
     */
    private Enthalpy heatOfVaporization;

    /**
     * Load molecular weight from a formula string.
     *
     * @param formulaString "CH4", "SiHCl3", etc.
     */
    private void loadFormulaString(final String formulaString) {

        formula = new ArrayList<ArrayList<Object>>();

        StringBuilder atom = new StringBuilder();
        StringBuilder count = new StringBuilder();

        // Parsing state
        int state = 0;

        for (int i = 0; i < formulaString.length(); i++) {
            char ch = formulaString.charAt(i);

            // log.info("ch: " + ch + " state: " + state);

            switch (state) {
            case 0:
                // Looking for alphanumeric atom name
                if (Character.isLetter(ch)) {
                    if ((atom.length() > 0) && (Character.isUpperCase(ch))) {
                        // Another atom specified (previous has count = 1)
                        ArrayList<Object> term = new ArrayList<Object>();
                        term.add(Atom.getAtom(atom.toString()));
                        term.add(1);
                        formula.add(term);
                        atom = new StringBuilder();
                        atom.append(ch);
                    } else if (atom.length() > 0) {
                        assert (Character.isLowerCase(ch));
                        atom.append(ch);
                    } else {
                        assert (atom.length() == 0);
                        atom.append(ch);
                    }
                } else if (Character.isDigit(ch)) {
                    count = new StringBuilder();
                    count.append(ch);
                    state = 1;
                } else if (Character.isWhitespace(ch)) {
                    // Ignore whitespace
                } else {
                    throw new IllegalArgumentException("Invalid chemical formula: " + formulaString);
                }
                break;
            case 1:
                if (Character.isLetter(ch)) {
                    // Atom with count has been specified
                    ArrayList<Object> term = new ArrayList<Object>();
                    term.add(Atom.getAtom(atom.toString()));
                    term.add(Integer.parseInt(count.toString()));
                    formula.add(term);

                    atom = new StringBuilder();
                    atom.append(ch);
                    state = 0;
                    count = new StringBuilder();
                } else if (Character.isDigit(ch)) {
                    count.append(ch);
                } else if (Character.isWhitespace(ch)) {
                    // Ignore whitespace
                } else {
                    throw new IllegalArgumentException("Invalid chemical formula: " + formulaString);
                }
                break;
            }
        } // for (int i = 0; i < formulaString.length(); i++)

        // Add the final term
        if (atom.length() > 0) {
            ArrayList<Object> term = new ArrayList<Object>();
            term.add(Atom.getAtom(atom.toString()));
            if (count.length() > 0) {
                term.add(Integer.parseInt(count.toString()));
            } else {
                term.add(1);
            }
            formula.add(term);
        }

        // Compute molecular weight
        double weight = 0;
        for (ArrayList<Object> term: formula) {
            Atom atomObj = (Atom)term.get(0);
            Integer atomCount = (Integer)term.get(1);
            weight += atomObj.getWeight() * atomCount;
        }
        molecularWeight = weight;
    }


    /**
     * Create an instance using a unique ID string.
     *
     * @param idString a unique ID string takes from Compounds.csv
     */
    private Compound(final String idString) {
        log.entry(idString);

        Properties compoundProps = allCompounds.getRow("ID", idString);
        loadFormulaString(compoundProps.getProperty("Formula"));
        // log.info(compoundProps);

        // Load properties
        Properties units = allCompounds.getUnits();

        criticalTemperature = Temperature.at(Double.
            parseDouble(compoundProps.getProperty(
                "CriticalTemperature")),
            units.getProperty("CriticalTemperature"));

        criticalPressure = Pressure.at(Double.
            parseDouble(compoundProps.getProperty(
                "CriticalPressure")),
            units.getProperty("CriticalPressure"));

        acentricFactor = Double.parseDouble(compoundProps.
            getProperty("AcentricFactor"));

        criticalVolume = Volume.at(Double.
            parseDouble(compoundProps.getProperty(
                "CriticalVolume")),
            units.getProperty("CriticalVolume"));

        criticalCompressibility = Double.parseDouble(compoundProps.
            getProperty("CriticalZ"));

        triplePointTemperature = Temperature.at(Double.
            parseDouble(compoundProps.getProperty(
                "TriplePointTemperature")),
            units.getProperty("TriplePointTemperature"));

        triplePointPressure = Pressure.at(Double.
            parseDouble(compoundProps.getProperty(
                "TriplePointPressure")),
            units.getProperty("TriplePointPressure"));

        if (compoundProps.getProperty("HeatCapacityFunction").equals(
            "IdealGasHeatCapacityPolynomial")) {

            // Ideal gas heat capacity
            ArrayList<Properties> allCpProps = null;
            allCpProps = allIdealGasHeatCapacityPolynomials.getRows("ID",
                idString);

            if (allCpProps == null) {
                throw new RuntimeException("IdealGasHeatCapacityPolynomial " +
                    "specified, but no actual properties found");
            }
            Properties cpUnits = allIdealGasHeatCapacityPolynomials.getUnits();
            ArrayList<HeatCapacityFunction> functions = null;
            functions = new ArrayList<HeatCapacityFunction>();

            for (Properties cpProps: allCpProps) {
                double [] cp = new double[4];
                cp[0] =  Double.parseDouble(cpProps.getProperty("cp0"));
                cp[1] =  Double.parseDouble(cpProps.getProperty("cp1"));
                cp[2] =  Double.parseDouble(cpProps.getProperty("cp2"));
                cp[3] =  Double.parseDouble(cpProps.getProperty("cp3"));

                Temperature Tmin = Temperature.at(Double.parseDouble(
                    cpProps.getProperty("Tmin")), cpUnits.getProperty("Tmin"));
                Temperature Tmax = Temperature.at(Double.parseDouble(
                    cpProps.getProperty("Tmax")), cpUnits.getProperty("Tmax"));
                String Tunit = cpProps.getProperty("Tunit");
                String cpUnit = cpProps.getProperty("cpUnit");

                IdealGasHeatCapacityPolynomial cpFunction;
                cpFunction = new IdealGasHeatCapacityPolynomial(cp, Tmin, Tmax,
                    Tunit, cpUnit);
                functions.add(cpFunction);
            }

            assert (functions.size() >= 1);
            if (functions.size() > 1) {
                idealGasHeatCapacityFunction = new CombinedHeatCapacityFunction(
                        functions);
            } else if (functions.size() == 1) {
                idealGasHeatCapacityFunction = functions.get(0);
            }

        }

        // Ideal gas heat of formation
        idealGasHeatOfFormation = Enthalpy.at(Double.
            parseDouble(compoundProps.getProperty(
                "IdealGasHeatOfFormation")),
            units.getProperty("IdealGasHeatOfFormation"));

        // Ideal gas entropy of formation
        idealGasEntropyOfFormation = Entropy.at(Double.
            parseDouble(compoundProps.getProperty(
                "IdealGasEntropyOfFormation")),
            units.getProperty("IdealGasEntropyOfFormation"));

        // Normal boiling point and heat of vaporization
        normalBoilingPointTemperature = Temperature.at(Double.
            parseDouble(compoundProps.getProperty(
                "NormalBoilingPoint")),
            units.getProperty("NormalBoilingPoint"));

        heatOfVaporization = Enthalpy.at(Double.
            parseDouble(compoundProps.getProperty(
                "HeatOfVaporization")),
            units.getProperty("HeatOfVaporization"));

        // Liquid heat capacity
        if (compoundProps.getProperty("LiquidHeatCapacityFunction").equals(
            "LiquidHeatCapacityDIPPR107")) {

            Properties cpProps = allLiquidHeatCapacityDIPPR107s.getRow("ID",
                idString);
            Properties cpUnits = allLiquidHeatCapacityDIPPR107s.getUnits();

            double [] cp = new double[5];
            cp[0] =  Double.parseDouble(cpProps.getProperty("cp0"));
            cp[1] =  Double.parseDouble(cpProps.getProperty("cp1"));
            cp[2] =  Double.parseDouble(cpProps.getProperty("cp2"));
            cp[3] =  Double.parseDouble(cpProps.getProperty("cp3"));
            cp[4] =  Double.parseDouble(cpProps.getProperty("cp4"));

            Temperature Tmin = Temperature.at(Double.parseDouble(
                cpProps.getProperty("Tmin")), cpUnits.getProperty("Tmin"));
            Temperature Tmax = Temperature.at(Double.parseDouble(
                cpProps.getProperty("Tmax")), cpUnits.getProperty("Tmax"));
            String Tunit = cpProps.getProperty("Tunit");
            String cpUnit = cpProps.getProperty("cpUnit");

            liquidHeatCapacityFunction = new LiquidHeatCapacityDIPPR107(
                cp, Tmin, Tmax, Tunit, cpUnit);

        }

        // Antoine equation
        if (compoundProps.getProperty("VaporPressureFunction").equals(
            "AntoineEquation")) {

            Properties vpProps = allAntoineEquations.getRow("ID", idString);
            Properties vpUnits = allAntoineEquations.getUnits();

            double [] c = new double[3];
            c[0] =  Double.parseDouble(vpProps.getProperty("c0"));
            c[1] =  Double.parseDouble(vpProps.getProperty("c1"));
            c[2] =  Double.parseDouble(vpProps.getProperty("c2"));

            Temperature Tmin = Temperature.at(Double.parseDouble(
                vpProps.getProperty("Tmin")), vpUnits.getProperty("Tmin"));
            Temperature Tmax = Temperature.at(Double.parseDouble(
                vpProps.getProperty("Tmax")), vpUnits.getProperty("Tmax"));
            String Tunit = vpProps.getProperty("Tunit");
            String pressureUnit = vpProps.getProperty("pressureUnit");

            vaporPressureFunction = new AntoineEquation(c, Tmin, Tmax, Tunit,
                pressureUnit);

        }



    }

    /**
     * Obtain a compound based on a name string.
     *
     * @param nameString the string identifying a compound
     * @return the compound
     */
    public static Compound getByIdString(final String nameString) {
        Properties matchAlias = allCompoundAliases.getRow("Alias", nameString);
        String idString = matchAlias.getProperty("ID");
        // log.info("Alias: " + nameString + " ID: " + idString);
        Compound compound = new Compound(idString);
        return compound;
    }

    /**
     * Make human-readable description of this Compound.
     *
     * @return displayable String
     */
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (ArrayList<Object> term: formula) {
            str.append(((Atom) term.get(0)).getSymbol());
            Integer count = (Integer) term.get(1);
            if (count > 1) {
                str.append(term.get(1));
            }
        }
        return str.toString();
    }

    /**
     * Get the critical temperature for this compound.
     *
     * @return critical temperature
     */
    public final Temperature Tc() {
        return criticalTemperature;
    }

    /**
     * Get the critical pressure for this compound.
     *
     * @return critical pressure
     */
    public final Pressure Pc() {
        return criticalPressure;
    }

    /**
     * Get the acentric factor for this compound.
     *
     * @return acentric factor
     */
    public final double omega() {
        return acentricFactor;
    }

    /**
     * Get the critical volume for this compound.
     *
     * @return critical volume
     */
    public final Volume Vc() {
        return criticalVolume;
    }

    /**
     * Get the critical compressibility for this compound.
     *
     * @return critical compressibility
     */
    public final double Zc() {
        return criticalCompressibility;
    }

    /**
     * Get the molecular weight for this compound.
     *
     * @return molecular weight
     */
    public final double mw() {
        return molecularWeight;
    }

    /**
     * Get the triple point temperature for this compound.
     *
     * @return triple point temperature
     */
    public final Temperature Tp() {
        return triplePointTemperature;
    }

    /**
     * Get the triple point pressure for this compound.
     *
     * @return triple point pressure
     */
    public final Pressure Pp() {
        return triplePointPressure;
    }

    /**
     * Get the ideal-gas heat of formation for this compound.
     *
     * @return ideal-gas heat of formation
     */
    public final Enthalpy Hf_IG() {
        return idealGasHeatOfFormation;
    }

    /**
     * Get the ideal-gas entropy of formation for this compound.
     *
     * @return ideal-gas entropy of formation
     */
    public final Entropy Sf_IG() {
        return idealGasEntropyOfFormation;
    }

    /**
     * Get the normal boiling point temperature (P = 1 atm) for this
     * compound.
     *
     * @return normal boiling point temperature
     */
    public final Temperature Tb() {
        return normalBoilingPointTemperature;
    }

    /**
     * Get the heat of vaporization for this compound.
     *
     * @return heat of vaporization
     */
    public final Enthalpy Hlv() {
        return heatOfVaporization;
    }

    /**
     * Compute the ideal-gas heat capacity at a specific temperature.
     *
     * @param T temperature to compute heat capacity at
     * @return the heat capacity
     * @throws ThermoStateException if the temperature is outside the valid
     * range
     */
    public final HeatCapacity Cp_IG(final Temperature T)
        throws ThermoStateException {

        return idealGasHeatCapacityFunction.eval(T);
    }

    /**
     * Get the ideal-gas heat capacity function.
     *
     * @return the heat capacity function
     */
    public final HeatCapacityFunction Cp_IG_Function() {
        return idealGasHeatCapacityFunction;
    }

    /**
     * Compute the liquid-phase heat capacity at a specific temperature.
     *
     * @param T temperature to compute heat capacity at
     * @return the heat capacity
     * @throws ThermoStateException if the temperature is outside the valid
     * range
     */
    public final HeatCapacity Cp_L(final Temperature T)
        throws ThermoStateException {

        return liquidHeatCapacityFunction.eval(T);
    }

    /**
     * Get the liquid-phase heat capacity function.
     *
     * @return the heat capacity function
     */
    public final HeatCapacityFunction Cp_L_Function() {
        return liquidHeatCapacityFunction;
    }

    /**
     * Compute the real enthalpy at a specific temperature and pressure.
     *
     * @param method the method to use
     * @param T temperature to compute enthalpy at
     * @param P pressure to compute enthalpy at
     * @return the enthalpy value
     * @throws ThermoStateException if the temperature is outside the valid
     * range
     */
    public final Enthalpy H(final Method method, final Temperature T,
        final Pressure P) throws ThermoStateException {

        // Heat of formation (ideal gas state)
        double hRef = idealGasHeatOfFormation.Jmol();
        // Ideal-gas state heat capacity integration
        double hCp = idealGasHeatCapacityFunction.integrate(Constants.STP_T,
            T).Jmol();

        double hR = 0;

        // Determine if this is a liquid or vapor, using the method.
        Equilibrium state = method.state(this, T, P);
        if ((state.LiquidV() != null) && (state.VaporV() == null)) {
            // This is a liquid: add the liquid residual enthalpy
            hR = state.LiquidHR().Jmol();
        } else if ((state.LiquidV() == null) && (state.VaporV() != null)) {
            // This is a vapor: add the vapor residual enthalpy
            hR = state.VaporHR().Jmol();
        } else {
            // This is right at the vapor/liquid equilibrium.  Use H_L() or
            // H_V() instead.
            throw new ThermoStateException("(T, P) is two-phase.  " +
                "Use H_L() or H_V() to obtain pure-phase enthalpy.");
        }
        return Enthalpy.at(hRef + hCp + hR, "J/mol");
    }

    /**
     * Compute the ideal-gas enthalpy at a specific temperature.
     *
     * @param T temperature to compute enthalpy at
     * @return the enthalpy
     * @throws ThermoStateException if the temperature is outside the valid
     * range for the heat capacity function
     */
    public final Enthalpy H_IG(final Temperature T)
        throws ThermoStateException {

        double h0 = idealGasHeatOfFormation.Jmol();
        double h1 = idealGasHeatCapacityFunction.integrate(Constants.STP_T,
            T).Jmol();
        return Enthalpy.at(h0 + h1, "J/mol");
    }

    /**
     * Get the liquid vapor pressure function.
     *
     * @return the vapor pressure function
     */
    public final VaporPressureFunction PsatFunction() {
        return vaporPressureFunction;
    }

    /**
     * Get the liquid vapor pressure function.
     *
     * @return the vapor pressure function
     */
    public final VaporPressureFunction TsatFunction() {
        return vaporPressureFunction;
    }

}
