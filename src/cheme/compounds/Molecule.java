/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.compounds;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * This class defines a molecule as a collection of Atoms (which may or may
 * not be bonded).
 */
public class Molecule {

    /**
     * A Molecule is a list of { Atom, Position, Rotation } tuples.
     */
    private class Tuple {

        /**
         * The Atom.
         */
        public Atom atom;

        /**
         * The Atom's position.
         */
        public Vector3D position;

        /**
         * The Atom's rotation.
         */
        public Vector3D rotation;

    }

    /**
     * The Logger to emit to.
     */
    private static Logger log = LogManager.getLogger(Molecule.class.getName());

    /**
     * The list of atoms making up this molecule.
     */
    private ArrayList<Tuple> atoms;

    /**
     * The description of this molecule when read from file.
     */
    private String description;

    /**
     * Construct by reading from an XYZ file.
     *
     * @param filename the name of the file to read from
     * @throws IOException if a java.io operation throws
     */
    public Molecule(final String filename) throws IOException {

        // Read each line, break it into tokens (honoring " as a quote
        // character), and append to atoms.
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new InputStreamReader(
                new FileInputStream(filename)));

            int lineNumber = 0;
            int atomCount = -1;
            atoms = new ArrayList<Tuple>();

            for (;;) {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                lineNumber++;

                if (lineNumber == 1) {
                    // First line has count of atoms.
                    atomCount = Integer.parseInt(line);
                    continue;
                }

                if (lineNumber == 2) {
                    // Second line is a comment.
                    log.info("Molecule name: '" + line + "'");
                    description = line;
                    continue;
                }

                if (lineNumber - 2 > atomCount) {
                    // No more atoms, done.
                    break;
                }

                String [] tokens = line.split("\\s+");
                if (tokens.length != 4) {
                    throw new IllegalArgumentException("Invalid XYZ line: '" +
                        line + "'");
                }

                String atomName = tokens[0];
                if (atomName.length() == 2) {
                    atomName = "" + Character.toUpperCase(atomName.charAt(0)) +
                    Character.toLowerCase(atomName.charAt(1));
                }
                double x = Double.parseDouble(tokens[1]);
                double y = Double.parseDouble(tokens[2]);
                double z = Double.parseDouble(tokens[3]);
                Vector3D position = new Vector3D(x, y, z);
                Tuple tuple = new Tuple();
                tuple.atom = Atom.getAtom(atomName);
                tuple.position = position;
                tuple.rotation = new Vector3D(0, 0, 0);
                atoms.add(tuple);

            } // for (;;)

            /*
             for (Tuple t: atoms) {
             log.info("Atom: " + t.atom + " " + t.position);
             }
             */

        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }

    /**
     * Save this molecule to an XYZ file.
     *
     * @param filename the name of the file to write to
     * @throws IOException if a java.io operation throws
     */
    public void writeXYZ(final String filename) throws IOException {
        PrintWriter writer = new PrintWriter(
                new OutputStreamWriter(new FileOutputStream(filename, true)));

        writer.printf("%d\n", atoms.size());
        writer.println(description);
        for (Tuple atom: atoms) {
            writer.printf("%-8s%19.12f%19.12f%19.12f\n",
                atom.atom.getSymbol().toUpperCase(), atom.position.getX(),
                atom.position.getY(), atom.position.getZ());
        }
        writer.close();
    }

    public static void main(String [] args) {
        try {
            log.info("Starting up...");
            Molecule molecule = new Molecule(args[0]);
            molecule.writeXYZ(args[1]);
        } catch (Throwable t) {
            log.error(t, t);
        }

    }


}
