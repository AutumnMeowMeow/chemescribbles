/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.compounds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import cheme.util.CSVFile;

/**
 * This class wraps properties from the periodic table.
 */
public class Atom {

    /**
     * The Logger to emit to.
     */
    private static Logger log = LogManager.getLogger(Atom.class.getName());

    /**
     * The periodic table raw data.
     */
    private static TreeMap<String, Atom> periodicTable;

    /**
     * Static constructor sets global properties.
     */
    static {
        periodicTable = new TreeMap<String, Atom>();

        try {
            CSVFile csvFile = new CSVFile(ClassLoader.getSystemClassLoader().
                getResourceAsStream("PeriodicTable.csv"));

            ArrayList<ArrayList<String>> tokens = csvFile.getTokens();

            boolean first       = true;

            int columnName      = -1;
            int columnSymbol    = -1;
            int columnNumber    = -1;
            int columnWeight    = -1;

            String name         = null;
            String symbol       = null;
            int number          = -1;
            double weight       = 0;

            for (ArrayList<String> line: tokens) {

                int i = 0;
                for (String token: line) {
                    if (first == true) {
                        if (token.equals("Name")) {
                            columnName = i;
                        }
                        if (token.equals("Symbol")) {
                            columnSymbol = i;
                        }
                        if (token.equals("Number")) {
                            columnNumber = i;
                        }
                        if (token.equals("Weight")) {
                            columnWeight = i;
                        }
                    } else {

                        if (i == columnName) {
                            if (token.equals("string")) {
                                // This is the dimensions line, skip it.
                                break;
                            }
                            name = token;
                        }
                        if (i == columnSymbol) {
                            symbol = token;
                        }
                        if (i == columnNumber) {
                            number = Integer.parseInt(token);
                        }
                        if (i == columnWeight) {
                            weight = Double.parseDouble(token);
                        }
                        if ((name != null) &&
                            (symbol != null) &&
                            (number != -1) &&
                            (weight != 0)
                        ) {
                            // We have all the fields for an Atom,
                            // instantiate it.
                            Atom atom = new Atom(symbol, name, number, weight);
                            periodicTable.put(symbol, atom);
                            name        = null;
                            symbol      = null;
                            number      = -1;
                            weight      = 0;
                        }
                    }
                    i++;
                } // for (String token: line)

                first = false;

            } // for (ArrayList<String> line; tokens)

        } catch (IOException e) {
            log.error(e, e);
        }
    }

    /**
     * Retrieve an Atom based on its short symbol.
     *
     * @param symbol "H", "He", etc.
     * @return the atom
     */
    public final static Atom getAtom(final String symbol) {
        return periodicTable.get(symbol);
    }

    /**
     * Atomic number.
     */
    private int atomicNumber;

    /**
     * Atomic weight.
     */
    private double atomicWeight;

    /**
     * Element symbol.
     */
    private String symbol;

    /**
     * Element name.
     */
    private String name;

    /**
     * Constructor used by getAtom().
     *
     * @param symbol element symbol, e.g. "H"
     * @param name element full name, e.g. "Hydrogen"
     * @param number atomic number
     * @param weight atomic weight
     */
    private Atom(final String symbol, final String name, final int number,
        final double weight) {

        this.symbol     = symbol;
        this.name       = name;
        atomicNumber    = number;
        atomicWeight    = weight;
    }

    /**
     * Get the element symbol.
     *
     * @return the element symbol
     */
    public final String getSymbol() {
        return this.symbol;
    }

    /**
     * Get the element name.
     *
     * @return the element name
     */
    public final String getName() {
        return this.name;
    }

    /**
     * Get the atomic number.
     *
     * @return the atomic number
     */
    public final int getNumber() {
        return atomicNumber;
    }

    /**
     * Get the atomic weight.
     *
     * @return the atomic weight
     */
    public final double getWeight() {
        return atomicWeight;
    }

    /**
     * Make human-readable description of this Atom.
     *
     * @return displayable String
     */
    @Override
    public String toString() {
        return String.format("Atom: %s", symbol);
    }

}
