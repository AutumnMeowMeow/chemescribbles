/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.compounds;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import cheme.methods.Method;
import cheme.methods.ThermoStateException;
import cheme.properties.Enthalpy;
import cheme.properties.Pressure;
import cheme.properties.Temperature;

/**
 * This class defines a mixture of chemical compounds and provides interfaces
 * to compute mixture properties.
 */
public class Mixture {

    /**
     * The Logger to emit to.
     */
    private static Logger log = LogManager.getLogger(Mixture.class.getName());

    /**
     * The components of this mixture.
     */
    private ArrayList<MoleFraction> components;

    /**
     * Compute the molecular weight for this mixture.
     *
     * @return molecular weight
     */
    public final double mw() {
        double mw = 0;
        for (MoleFraction x: components) {
            mw += x.fraction * x.compound.mw();
        }
        return mw;
    }

    /**
     * Get the number of components in this mixture.
     *
     * @return the number of components
     */
    public final int size() {
        return components.size();
    }

    /**
     * Get a MoleFraction by index.
     *
     * @param index the index.  0 means the first component.
     * @return the MoleFraction
     */
    public final MoleFraction get(final int index) {
        return components.get(index);
    }

    /**
     * Get all the components in this mixture.
     *
     * @return the MoleFractions
     */
    public final List<MoleFraction> getComponents() {
        return components;
    }

    /**
     * Construct a mixture using a list of components.  Note that the final
     * mole fractions will be normalized against those provided.  So one
     * could for example provide a list of compounds and mole flows and they
     * would be converted to mole fractions here.
     *
     * @param components a list of components and mole fractions.  Mixture
     * creates its own copy of this list.
     */
    public Mixture(final List<MoleFraction> components) {
        log.entry(components);

        double total = 0;

        for (MoleFraction x: components) {
            if (x.fraction < 0) {
                throw new IllegalArgumentException("Invalid molar fraction: " +
                    x.fraction + " for component " + x.compound);
            }
            total += x.fraction;
        }
        if (total <= 0) {
            throw new IllegalArgumentException("Invalid molar fraction sum: " +
                total);
        }

        this.components = new ArrayList<MoleFraction>(components.size());
        for (MoleFraction x: components) {
            this.components.add(new MoleFraction(x.compound,
                    x.fraction / total));
        }
    }

    /**
     * Make human-readable description of this Mixture.
     *
     * @return displayable String
     */
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (MoleFraction x: components) {
            str.append(String.format("%f ", x.fraction));
            str.append(x.compound);
            str.append("\n");
        }
        return str.toString();
    }

    /**
     * Compute the real enthalpy at a specific temperature and pressure.
     *
     * @param method the method to use
     * @param T temperature to compute enthalpy at
     * @param P pressure to compute enthalpy at
     * @return the enthalpy value
     * @throws ThermoStateException if the temperature is outside the valid
     * range
     */
    public final Enthalpy H(final Method method, final Temperature T,
        final Pressure P) throws ThermoStateException {

        // TODO
        return Enthalpy.at(0, "J/mol");
    }

}
