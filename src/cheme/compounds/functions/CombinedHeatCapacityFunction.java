/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.compounds.functions;

import java.util.List;

import cheme.methods.ThermoStateException;
import cheme.properties.Enthalpy;
import cheme.properties.HeatCapacity;
import cheme.properties.Temperature;

/**
 * This class combines two heat capacity functions to make one function with
 * a larger temperature range.
 */
public class CombinedHeatCapacityFunction implements HeatCapacityFunction {

    /**
     * The heat capacity functions to combine.
     */
    private List<HeatCapacityFunction> functions;

    /**
     * Get the minimum temperature bound for this function.
     *
     * @return the minimum temperature
     */
    public Temperature getMinimumTemp() {
        return functions.get(0).getMinimumTemp();
    }

    /**
     * Get the maximum temperature bound for this function.
     *
     * @return the maximum temperature
     */
    public Temperature getMaximumTemp() {
        return functions.get(functions.size() - 1).getMaximumTemp();
    }

    /**
     * Get the temperature units for this function.
     *
     * @return the temperature unit
     */
    public String getTempUnit() {
        return functions.get(0).getTempUnit();
    }

    /**
     * Public constructor.
     *
     * @param functions the functions to combine to make a new function.  The
     * temperature ranges of the functions need to overlap (no gaps).  The
     * overlap region will be naively averaged for both heat capacity and
     * enthalpy values.
     */
    public CombinedHeatCapacityFunction(final List<HeatCapacityFunction>
        functions) {

        if (functions.size() != 2) {
            throw new IllegalArgumentException(functions.size() +
                " functions provided, need exactly 2");
        }

        Temperature max = functions.get(0).getMaximumTemp();
        for (HeatCapacityFunction function: functions) {
            if (function.getMinimumTemp().K() > max.K()) {
                throw new IllegalArgumentException("Temperature gap " +
                    "between " +
                    function.getMinimumTemp().get(function.getTempUnit()) +
                    " and " + max.get(function.getTempUnit()));
            }
            max = function.getMaximumTemp();
        }
        this.functions = functions;
    }

    /**
     * Compute the heat capacity at a specific temperature.
     *
     * @param T temperature to compute heat capacity at
     * @return the heat capacity
     * @throws ThermoStateException if the temperature is outside the valid
     * range
     */
    public HeatCapacity eval(final Temperature T)
        throws ThermoStateException {

        if ((T.compareTo(getMinimumTemp()) < 0) ||
            (T.compareTo(getMaximumTemp()) > 0)) {

            throw new ThermoStateException("T: " + T + " outside range " +
                getMinimumTemp() + " <= T <= " + getMaximumTemp());
        }

        Temperature f1MaxTemp = functions.get(0).getMaximumTemp();
        Temperature f2MinTemp = functions.get(1).getMinimumTemp();
        double f1Max = f1MaxTemp.K();
        double f2Min = f2MinTemp.K();
        double t = T.K();

        if (t < f2Min) {
            // Only the first function can solve this, get it.
            return functions.get(0).eval(T);
        }
        if (t > f1Max) {
            // Only the second function can solve this, get it.
            return functions.get(1).eval(T);
        }

        // This is in the overlap region.  Return the point along the line
        // between (f2Min, f2MinCp) and (f1Max, f1MaxCp)
        double f2MinCp = functions.get(1).eval(f2MinTemp).JmolK();
        double f1MaxCp = functions.get(0).eval(f1MaxTemp).JmolK();
        double cp = (t - f2Min) * (f1MaxCp - f2MinCp) / (f1Max - f2Min) + f2MinCp;
        return HeatCapacity.at(cp, "J/mol");
    }

    /**
     * Compute the enthalpy change between two temperatures, i.e. integrate
     * the heat capacity change between T0 and T1.
     *
     * @param T0 starting temperature
     * @param T1 ending temperature
     * @return the enthalpy
     * @throws ThermoStateException if T0 or T1 is outside the valid range
     */
    public Enthalpy integrate(final Temperature T0, final Temperature T1)
        throws ThermoStateException {

        if ((T0.compareTo(getMinimumTemp()) < 0) ||
            (T0.compareTo(getMaximumTemp()) > 0)) {

            throw new ThermoStateException("T0: " + T0 + " outside range " +
                getMinimumTemp() + " <= T0 <= " + getMaximumTemp());
        }

        if ((T1.compareTo(getMinimumTemp()) < 0) ||
            (T1.compareTo(getMaximumTemp()) > 0)) {

            throw new ThermoStateException("T1: " + T1 + " outside range " +
                getMinimumTemp() + " <= T1 <= " + getMaximumTemp());
        }

        // The integral bits are:
        //
        //   A - function 1
        //   B - overlap
        //   C - function 2
        //
        // The cases to cover:
        //
        //   1. Begin in A, end in A
        //
        //   2. Begin in A, end in B
        //
        //   3. Begin in A, end in C
        //
        //   4. Begin in B, end in A --> reverse the answer from #2
        //
        //   5. Begin in B, end in B
        //
        //   6. Begin in B, end in C
        //
        //   7. Begin in C, end in A --> reverse the answer from #3
        //
        //   8. Begin in C, end in B --> reverse the answer from #6
        //
        //   9. Begin in C, end in C
        //
        //
        //                  |------------------------|
        // |----------------------|                  |
        // |                ax   bx                  |
        // |                                         |
        // |      t0....................t1           |
        // |                                         |
        // |      A         |  B  |         C        |

        double t0 = T0.K();
        double t1 = T1.K();
        double ax = functions.get(1).getMinimumTemp().K();
        double bx = functions.get(0).getMaximumTemp().K();

        if ((t0 < ax) && (t1 < ax)) {
            // Begin in A, end in A
            return functions.get(0).integrate(T0, T1);
        }
        if ((t0 < ax) && (t1 > ax) && (t1 < bx)) {
            // Begin in A, end in B
            Temperature AX = Temperature.at(ax, "K");
            double h1 = functions.get(0).integrate(T0, AX).Jmol();
            double h2 = functions.get(0).integrate(AX, T1).Jmol();
            double h3 = functions.get(1).integrate(AX, T1).Jmol();
            double result = h1 + (h2 + h3) / 2;
            return Enthalpy.at(result, "J/mol");
        }
        if ((t0 < ax) && (t1 > bx)) {
            // Begin in A, end in C
            Temperature AX = Temperature.at(ax, "K");
            Temperature BX = Temperature.at(bx, "K");
            double h1 = functions.get(0).integrate(T0, AX).Jmol();
            double h2 = functions.get(0).integrate(AX, BX).Jmol();
            double h3 = functions.get(1).integrate(AX, BX).Jmol();
            double h4 = functions.get(1).integrate(BX, T1).Jmol();
            double result = h1 + (h2 + h3) / 2 + h4;
            return Enthalpy.at(result, "J/mol");
        }
        if ((t0 > ax) && (t0 < bx) && (t1 < ax)) {
            // Begin in B, end in A
            Temperature AX = Temperature.at(ax, "K");
            double h1 = functions.get(0).integrate(AX, T1).Jmol();
            double h2 = functions.get(0).integrate(T0, AX).Jmol();
            double h3 = functions.get(1).integrate(T0, AX).Jmol();
            double result = h1 + (h2 + h3) / 2;
            return Enthalpy.at(result, "J/mol");
        }
        if ((t0 > ax) && (t0 < bx) && (t1 > ax) && (t1 < bx)) {
            // Begin in B, end in B
            double h1 = functions.get(0).integrate(T0, T1).Jmol();
            double h2 = functions.get(1).integrate(T0, T1).Jmol();
            double result = (h1 + h2) / 2;
            return Enthalpy.at(result, "J/mol");
        }
        if ((t0 > ax) && (t0 < bx) && (t1 > bx)) {
            // Begin in B, end in C
            Temperature BX = Temperature.at(bx, "K");
            double h1 = functions.get(0).integrate(T0, BX).Jmol();
            double h2 = functions.get(1).integrate(T0, BX).Jmol();
            double h3 = functions.get(1).integrate(BX, T1).Jmol();
            double result = (h1 + h2) / 2 + h3;
            return Enthalpy.at(result, "J/mol");
        }
        if ((t0 > ax) && (t0 < bx) && (t1 < ax)) {
            // Begin in C, end in A
            Temperature AX = Temperature.at(ax, "K");
            Temperature BX = Temperature.at(bx, "K");
            double h1 = functions.get(1).integrate(T0, BX).Jmol();
            double h2 = functions.get(1).integrate(BX, AX).Jmol();
            double h3 = functions.get(0).integrate(BX, AX).Jmol();
            double h4 = functions.get(0).integrate(AX, T1).Jmol();
            double result = h1 + (h2 + h3) / 2 + h4;
            return Enthalpy.at(result, "J/mol");
        }
        if ((t0 > ax) && (t0 < bx) && (t1 > ax) && (t1 < bx)) {
            // Begin in C, end in B
            Temperature BX = Temperature.at(bx, "K");
            double h1 = functions.get(1).integrate(T0, BX).Jmol();
            double h2 = functions.get(1).integrate(BX, T1).Jmol();
            double h3 = functions.get(0).integrate(BX, T1).Jmol();
            double result = h1 + (h2 + h3) / 2;
            return Enthalpy.at(result, "J/mol");
        }
        if ((t0 > bx) && (t1 > bx)) {
            // Begin in C, end in C
            return functions.get(1).integrate(T0, T1);
        }

        // Should never get here
        throw new RuntimeException("Logic flow error: t0 " + t0 + " t1 " + t1 +
            " ax " + ax + " bx " + bx);
    }

}
