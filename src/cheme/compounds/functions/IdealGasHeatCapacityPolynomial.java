/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.compounds.functions;

import org.apache.commons.math3.analysis.ParametricUnivariateFunction;

import cheme.methods.ThermoStateException;
import cheme.properties.Enthalpy;
import cheme.properties.HeatCapacity;
import cheme.properties.Temperature;

/**
 * This class computes heat capacity via the following polynomial:
 *
 * C_{p}^{*ig} = C_0 + C_1 T + C_2 T^2 + C_3 T^3
 *
 * The polynomial above was taken from Felder and Rousseau, _Elementary
 * Principles of Chemical Processes_.
 */
public class IdealGasHeatCapacityPolynomial implements HeatCapacityFunction,
                                            ParametricUnivariateFunction {

    /**
     * The four coefficients of the polynomial equation.
     */
    private double [] c;

    /**
     * Minimum temperature bound for this set of coefficients.
     */
    private Temperature minimumTemp;

    /**
     * Maximum temperature bound for this set of coefficients.
     */
    private Temperature maximumTemp;

    /**
     * The temperature unit for this set of coefficients.
     */
    private String tempUnit;

    /**
     * The heat capacity unit for this set of coefficients.
     */
    private String heatCapacityUnit;

    /**
     * Get the minimum temperature bound for this function.
     *
     * @return the minimum temperature
     */
    public Temperature getMinimumTemp() {
        return minimumTemp;
    }

    /**
     * Get the maximum temperature bound for this function.
     *
     * @return the maximum temperature
     */
    public Temperature getMaximumTemp() {
        return maximumTemp;
    }

    /**
     * Get the temperature units for this function.
     *
     * @return the temperature unit
     */
    public String getTempUnit() {
        return tempUnit;
    }

    /**
     * Public constructor.
     *
     * @param coefficients the four coefficients of the heat capacity
     * polynomial.
     * @param minimumTemp the minimum temperature bound for this set of
     * coefficients
     * @param maximumTemp the maximum temperature bound for this set of
     * coefficients
     * @param tempUnit the temperature unit ("K", "C", etc.) used by the
     * terms of this polynomial.
     * @param heatCapacityUnit the heat capacity unit ("J/mol-K",
     * "BTU/lbmol-R", etc.) produced by the terms of this function.
     */
    public IdealGasHeatCapacityPolynomial(final double [] coefficients,
        final Temperature minimumTemp, final Temperature maximumTemp,
        final String tempUnit, final String heatCapacityUnit) {

        if (coefficients.length != 4) {
            throw new IllegalArgumentException("Ideal gas heat capacity " +
                "requires 4 coefficients, only " + coefficients.length +
                "given");
        }
        c = new double[4];
        c[0] = coefficients[0];
        c[1] = coefficients[1];
        c[2] = coefficients[2];
        c[3] = coefficients[3];

        this.minimumTemp      = Temperature.at(minimumTemp.K(), 'K');
        this.maximumTemp      = Temperature.at(maximumTemp.K(), 'K');
        this.tempUnit         = tempUnit;
        this.heatCapacityUnit = heatCapacityUnit;
    }

    /**
     * Compute the heat capacity at a specific temperature.
     *
     * @param T temperature to compute heat capacity at
     * @return the heat capacity
     * @throws ThermoStateException if the temperature is outside the valid
     * range
     */
    public HeatCapacity eval(final Temperature T)
        throws ThermoStateException {

        if ((T.compareTo(minimumTemp) < 0) || (T.compareTo(maximumTemp) > 0)) {
            throw new ThermoStateException("T: " + T + " outside range " +
                minimumTemp + " <= T <= " + maximumTemp);
        }

        double t = T.get(tempUnit);
        double value = c[0] + (c[1] * t) + (c[2] * t * t) + (c[3] * t * t * t);
        return HeatCapacity.at(value, heatCapacityUnit);
    }

    /**
     * Compute the enthalpy change between two temperatures, i.e. integrate
     * the heat capacity change between T0 and T1.
     *
     * @param T0 starting temperature
     * @param T1 ending temperature
     * @return the enthalpy
     * @throws ThermoStateException if T0 or T1 is outside the valid range
     */
    public Enthalpy integrate(final Temperature T0, final Temperature T1)
        throws ThermoStateException {

        if ((T0.compareTo(minimumTemp) < 0) ||
            (T0.compareTo(maximumTemp) > 0)
        ) {
            throw new ThermoStateException("T0: " + T0 + " outside range " +
                minimumTemp + " <= T0 <= " + maximumTemp);
        }
        if ((T1.compareTo(minimumTemp) < 0) ||
            (T1.compareTo(maximumTemp) > 0)
        ) {
            throw new ThermoStateException("T1: " + T1 + " outside range " +
                minimumTemp + " <= T1 <= " + maximumTemp);
        }

        double t0 = T0.get(tempUnit);
        double t1 = T1.get(tempUnit);
        double value = c[0] * (t1 - t0) +
            (c[1] * (Math.pow(t1, 2) - Math.pow(t0, 2))) / 2 +
            (c[2] * (Math.pow(t1, 3) - Math.pow(t0, 3))) / 3 +
            (c[3] * (Math.pow(t1, 4) - Math.pow(t0, 4))) / 4;

        return Enthalpy.at(value,
            HeatCapacity.enthalpyUnit(heatCapacityUnit));
    }

    /**
     * Compute the value of the function.
     *
     * @param x Point for which the function value should be computed.
     * @param parameters Function parameters.
     * @return the value.
     */
    public double value(double x, double... parameters) {
        double t = x;
        double a = parameters[0];
        double b = parameters[1];
        double c = parameters[2];
        double d = parameters[3];

        return (a + (b * t) + (c * t * t) + (d * t * t * t));
    }

    /**
     * Compute the gradient of the function with respect to its parameters.
     *
     * @param x Point for which the function value should be computed.
     * @param parameters Function parameters.
     * @return the value.
     */
    public double[] gradient(double x, double... parameters) {
        double t = x;

        double [] grad = new double[4];
        grad[0] = 1;
        grad[1] = t;
        grad[2] = Math.pow(t, 2);
        grad[3] = Math.pow(t, 3);

        return grad;
    }

}
