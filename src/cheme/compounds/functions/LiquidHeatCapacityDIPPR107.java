/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.compounds.functions;

import java.util.ArrayList;
import java.util.Collection;
import org.apache.commons.math3.analysis.ParametricUnivariateFunction;
import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.analysis.integration.SimpsonIntegrator;
import org.apache.commons.math3.exception.ConvergenceException;
import org.apache.commons.math3.fitting.SimpleCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoint;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import cheme.methods.ThermoStateException;
import cheme.properties.Enthalpy;
import cheme.properties.HeatCapacity;
import cheme.properties.Temperature;

/**
 * This class computes liquid heat capacity via the following function:
 *
 * C_{p} = C_0 + C_1 * [ (C_2/T)/sinh(C_2/T) ]^2 +
 *               C_3 * [ (C_4/T)/cosh(C_4/T) ]^2
 */
public class LiquidHeatCapacityDIPPR107 implements HeatCapacityFunction,
                                        ParametricUnivariateFunction {

    /**
     * The Logger to emit to.
     */
    private static Logger log = LogManager.getLogger(LiquidHeatCapacityDIPPR107.class.getName());

    /**
     * If true, allow tracing to the Logger.
     */
    private static final boolean DEBUG = false;

    /**
     * The coefficients of the equation.
     */
    private double [] c;

    /**
     * Minimum temperature bound for this set of coefficients.
     */
    private Temperature minimumTemp;

    /**
     * Maximum temperature bound for this set of coefficients.
     */
    private Temperature maximumTemp;

    /**
     * The temperature unit for this set of coefficients.
     */
    private String tempUnit;

    /**
     * The heat capacity unit for this set of coefficients.
     */
    private String heatCapacityUnit;

    /**
     * Get the minimum temperature bound for this function.
     *
     * @return the minimum temperature
     */
    public Temperature getMinimumTemp() {
        return minimumTemp;
    }

    /**
     * Get the maximum temperature bound for this function.
     *
     * @return the maximum temperature
     */
    public Temperature getMaximumTemp() {
        return maximumTemp;
    }

    /**
     * Get the temperature units for this function.
     *
     * @return the temperature unit
     */
    public String getTempUnit() {
        return tempUnit;
    }

    /**
     * Public constructor.
     *
     * @param coefficients the four coefficients of the heat capacity
     * function.
     * @param minimumTemp the minimum temperature bound for this set of
     * coefficients
     * @param maximumTemp the maximum temperature bound for this set of
     * coefficients
     * @param tempUnit the temperature unit ("K", "C", etc.) used by the
     * terms of this function.
     * @param heatCapacityUnit the heat capacity unit ("J/mol-K",
     * "BTU/lbmol-R", etc.) produced by the terms of this function.
     */
    public LiquidHeatCapacityDIPPR107(final double [] coefficients,
        final Temperature minimumTemp, final Temperature maximumTemp,
        final String tempUnit, final String heatCapacityUnit) {

        if (coefficients.length != 5) {
            throw new IllegalArgumentException("Liquid heat capacity " +
                "requires 5 coefficients, only " + coefficients.length +
                "given");
        }
        c = new double[5];
        c[0] = coefficients[0];
        c[1] = coefficients[1];
        c[2] = coefficients[2];
        c[3] = coefficients[3];
        c[4] = coefficients[4];

        this.minimumTemp      = Temperature.at(minimumTemp.K(), 'K');
        this.maximumTemp      = Temperature.at(maximumTemp.K(), 'K');
        this.tempUnit         = tempUnit;
        this.heatCapacityUnit = heatCapacityUnit;
    }

    /**
     * Compute the heat capacity at a specific temperature.
     *
     * @param T temperature to compute heat capacity at
     * @return the heat capacity
     * @throws ThermoStateException if the temperature is outside the valid
     * range
     */
    public HeatCapacity eval(final Temperature T)
        throws ThermoStateException {

        if ((T.compareTo(minimumTemp) < 0) || (T.compareTo(maximumTemp) > 0)) {
            throw new ThermoStateException("T: " + T + " outside range " +
                minimumTemp + " <= T <= " + maximumTemp);
        }

        double t = T.get(tempUnit);
        double value = c[0] + c[1] * Math.pow((c[2]/t) / Math.sinh(c[2]/t), 2) +
                c[3] * Math.pow((c[4]/t) / Math.cosh(c[4]/t), 2);
        return HeatCapacity.at(value, heatCapacityUnit);
    }

    /**
     * UnivariateWrapper wraps an instance of LiquidHeatCapacityDIPPR107 so
     * that SimpsonIntegrator can call its value() function.
     */
    private class UnivariateWrapper implements UnivariateFunction {

        /**
         * The ParametricUnivariateFunction instance to wrap.
         */
        LiquidHeatCapacityDIPPR107 f;

        /**
         * Construct an instance.
         *
         * @param f the ParametricUnivariateFunction to wrap
         */
        UnivariateWrapper(LiquidHeatCapacityDIPPR107 f) {
            this.f = f;
        }

        /**
         * Compute the value of the function.
         *
         * @param x Point for which the function value should be computed.
         * @return the value.
         */
        public double value(double x) {
            try {
                return (f.eval(Temperature.at(x,
                            f.tempUnit)).get(f.heatCapacityUnit));
            } catch (ThermoStateException e) {
                return 0;
            }
        }
    }

    /**
     * Compute the enthalpy change between two temperatures, i.e. integrate
     * the heat capacity change between T0 and T1.
     *
     * @param T0 starting temperature
     * @param T1 ending temperature
     * @return the enthalpy
     * @throws ThermoStateException if T0 or T1 is outside the valid range
     */
    public Enthalpy integrate(final Temperature T0, final Temperature T1)
        throws ThermoStateException {

        if ((T0.compareTo(minimumTemp) < 0) ||
            (T0.compareTo(maximumTemp) > 0)
        ) {
            throw new ThermoStateException("T0: " + T0 + " outside range " +
                minimumTemp + " <= T0 <= " + maximumTemp);
        }
        if ((T1.compareTo(minimumTemp) < 0) ||
            (T1.compareTo(maximumTemp) > 0)
        ) {
            throw new ThermoStateException("T1: " + T1 + " outside range " +
                minimumTemp + " <= T1 <= " + maximumTemp);
        }

        // Integral is:
        //     a*t + b*c*cosh(c/t)/sinh(c/t) - d*e*sinh(e/t)/cosh(e/t)
        //
        // However, floating-point math around the hyperbolic functions is
        // really noisy.  So we will attempt a numeric integral instead.

        double t0 = T0.get(tempUnit);
        double t1 = T1.get(tempUnit);
        SimpsonIntegrator integrator = new SimpsonIntegrator();
        try {
            double result = integrator.integrate(1000,
                new UnivariateWrapper(this), t0, t1);
            return Enthalpy.at(result,
                HeatCapacity.enthalpyUnit(heatCapacityUnit));
        } catch (RuntimeException e) {
            log.debug(e);

            // The numeric integration failed, so resort to the analytical
            // version.
            double value1 = c[0] * t1 +
                c[1] * c[2] * Math.cosh(c[2]/t1) / Math.sinh(c[2]/t1) -
                c[3] * c[4] * Math.sinh(c[4]/t1) / Math.cosh(c[4]/t1);
            double value0 = c[0] * t0 +
                c[1] * c[2] * Math.cosh(c[2]/t0) / Math.sinh(c[2]/t0) -
                c[3] * c[4] * Math.sinh(c[4]/t0) / Math.cosh(c[4]/t0);

            return Enthalpy.at(value1 - value0,
                HeatCapacity.enthalpyUnit(heatCapacityUnit));
        }
    }

    /**
     * Compute the value of the function.
     *
     * @param x Point for which the function value should be computed.
     * @param parameters Function parameters.
     * @return the value.
     */
    public double value(double x, double... parameters) {
        double t = x;
        double a = parameters[0];
        double b = parameters[1];
        double c = parameters[2];
        double d = parameters[3];
        double e = parameters[4];

        return (a + b * Math.pow((c/t) / Math.sinh(c/t), 2) +
            d * Math.pow((e/t) / Math.cosh(e/t), 2));
    }

    /**
     * Compute the gradient of the function with respect to its parameters.
     *
     * @param x Point for which the function value should be computed.
     * @param parameters Function parameters.
     * @return the value.
     */
    public double[] gradient(double x, double... parameters) {
        double t = x;
        double b = parameters[1];
        double c = parameters[2];
        double d = parameters[3];
        double e = parameters[4];

        double [] grad = new double[5];
        grad[0] = 1;
        grad[1] = Math.pow((c/t) / Math.sinh(c/t), 2);
        grad[2] = 2 * b * c / (t * t * Math.pow(Math.sinh(c/t), 2)) -
            2 * b * c * c * Math.cosh(c/t) / (Math.pow(t * Math.sinh(c/t), 3));

        grad[3] = Math.pow((e/t) / Math.cosh(e/t), 2);
        grad[4] = 2 * d * e / (t * t * Math.pow(Math.cosh(e/t), 2)) -
            2 * d * e * e * Math.sinh(e/t) / (Math.pow(t * Math.cosh(e/t), 3));

        return grad;
    }

    /**
     * Private constructor used by fitTo().
     *
     * @param minimumTemp the minimum temperature bound
     * @param maximumTemp the maximum temperature bound
     * @param tempUnit the temperature unit ("K", "C", etc.) used by the
     * terms of this function.
     * @param heatCapacityUnit the heat capacity unit ("J/mol-K",
     * "BTU/lbmol-R", etc.) produced by the terms of this function.
     */
    public LiquidHeatCapacityDIPPR107(final Temperature minimumTemp,
        final Temperature maximumTemp, final String tempUnit,
        final String heatCapacityUnit) {

        c = null;

        this.minimumTemp      = Temperature.at(minimumTemp.K(), 'K');
        this.maximumTemp      = Temperature.at(maximumTemp.K(), 'K');
        this.tempUnit         = tempUnit;
        this.heatCapacityUnit = heatCapacityUnit;
    }

    /**
     * Compare this instance's values against a set of data points.
     *
     * @param points empirical data points to fit to
     */
    private double fitCompare(final Collection<WeightedObservedPoint> points) {
        double error = 0;
        for (WeightedObservedPoint point: points) {
            error += Math.pow(value(point.getX(), c) - point.getY(), 2);
        }

        return error;
    }

    /**
     * Create a heat capacity function using data points.
     *
     * @param points empirical data points to fit to
     * @param minimumTemp the minimum temperature bound
     * @param maximumTemp the maximum temperature bound
     * @param tempUnit the temperature unit ("K", "C", etc.) used by the
     * terms of this function.
     * @param heatCapacityUnit the heat capacity unit ("J/mol-K",
     * "BTU/lbmol-R", etc.) produced by the terms of this function.
     * @return a new function fitted to the data points
     */
    public static LiquidHeatCapacityDIPPR107 fitTo(final
        Collection<WeightedObservedPoint> points, final Temperature minimumTemp,
        final Temperature maximumTemp, final String tempUnit,
        final String heatCapacityUnit) {

        LiquidHeatCapacityDIPPR107 cp = new LiquidHeatCapacityDIPPR107(
        minimumTemp, maximumTemp, tempUnit, heatCapacityUnit);

        // Start with several potential heat capacity functions.
        double [][] start = new double[5][5];
        double [] error = new double[5];

        // Water - cal/mol-K
        start[0][0] = 7.969;
        start[0][1] = 6.399;
        start[0][2] = 2610.5;
        start[0][3] = 2.125;
        start[0][4] = 1169;

        // Ammonia - cal/mol-K
        start[1][0] = 7.983;
        start[1][1] = 11.699;
        start[1][2] = 2036;
        start[1][3] = 5.388;
        start[1][4] = 882;

        // Hexane - cal/mol-K
        start[2][0] = 24.936;
        start[2][1] = 84.145;
        start[2][2] = 1694.6;
        start[2][3] = 56.583;
        start[2][4] = 761.6;


        // Dodecane - cal/mol-K
        start[3][0] = 50.862;
        start[3][1] = 158.43;
        start[3][2] = 1715.2;
        start[3][3] = 107.87;
        start[3][4] = 777.5;


        // SiCl4 - kJ/mol-K
        start[4][0] = 0.131578;
        start[4][1] = 3266746;
        start[4][2] = 6012.4391;
        start[4][3] = 0.2981757;
        start[4][4] = 1020.1966;

        double minError = Double.MAX_VALUE;
        double [] parameters = new double[5];
        for (int i = 0; i < 5; i++) {
            SimpleCurveFitter fitter = SimpleCurveFitter.create(cp, start[i]);
            try {
                cp.c = fitter.fit(points);
            } catch (ConvergenceException e) {
                // Floating point error, skip this one
                if (DEBUG) {
                    log.debug("Floating point error from start " +
                        "point " + i);
                }
                continue;
            }

            error[i] = cp.fitCompare(points);
            if (error[i] < minError) {
                parameters = cp.c;
                minError = error[i];
                if (DEBUG) {
                    log.debug("KEEP i " + i + " error[i]: " +
                        error[i]);
                    log.debug("    " + cp.c[0]);
                    log.debug("    " + cp.c[1]);
                    log.debug("    " + cp.c[2]);
                    log.debug("    " + cp.c[3]);
                    log.debug("    " + cp.c[4]);
                }
            } else {
                if (DEBUG) {
                    log.debug("REJECT i " + i + " error[i]: " +
                        error[i]);
                    log.debug("    " + cp.c[0]);
                    log.debug("    " + cp.c[1]);
                    log.debug("    " + cp.c[2]);
                    log.debug("    " + cp.c[3]);
                    log.debug("    " + cp.c[4]);
                }
            }

        }

        if (minError == Double.MAX_VALUE) {
            throw new RuntimeException("Unable to fit to points, " +
                "floating point error?");
        }

        cp.c = parameters;
        if (DEBUG) {
            log.debug("-- FINAL --");
            log.debug("    " + parameters[0]);
            log.debug("    " + parameters[1]);
            log.debug("    " + parameters[2]);
            log.debug("    " + parameters[3]);
            log.debug("    " + parameters[4]);
        }
        return cp;
    }

    public static void main(String [] args) {
        Collection<WeightedObservedPoint> points = new ArrayList<WeightedObservedPoint>();

        // Test fluid 1
        points.add(new WeightedObservedPoint(1.0,  419.67, 138.553164));
        points.add(new WeightedObservedPoint(1.0,  499.67, 145.480822));
        points.add(new WeightedObservedPoint(1.0,  579.67, 152.408480));
        points.add(new WeightedObservedPoint(1.0,  659.67, 159.336139));
        points.add(new WeightedObservedPoint(1.0,  739.67, 166.263797));
        points.add(new WeightedObservedPoint(1.0,  819.67, 173.576325));
        points.add(new WeightedObservedPoint(1.0,  899.67, 180.503983));
        points.add(new WeightedObservedPoint(1.0,  979.67, 187.431641));
        points.add(new WeightedObservedPoint(1.0, 1059.67, 194.359299));
        points.add(new WeightedObservedPoint(1.0, 1139.67, 201.286958));
        points.add(new WeightedObservedPoint(1.0, 1219.67, 208.214616));

        LiquidHeatCapacityDIPPR107 cp = fitTo(points,
            Temperature.at(-40, "F"), Temperature.at(760, "F"), "R",
            "BTU/lbmol-R");

        System.err.println("Fitted parameters:");
        System.err.printf("    %f\n", cp.c[0]);
        System.err.printf("    %f\n", cp.c[1]);
        System.err.printf("    %f\n", cp.c[2]);
        System.err.printf("    %f\n", cp.c[3]);
        System.err.printf("    %f\n", cp.c[4]);

        for (int t = -40; t <= 760; t += 80) {
            try {
                log.debug(t + ", " +
                    cp.eval(Temperature.at(t, "F")).BTUlbmolR());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Test fluid 2
        points = new ArrayList<WeightedObservedPoint>();
        points.add(new WeightedObservedPoint(1.0, 207.66, 0.130730));
        points.add(new WeightedObservedPoint(1.0, 235.11, 0.136270));
        points.add(new WeightedObservedPoint(1.0, 265.96, 0.141740));
        points.add(new WeightedObservedPoint(1.0, 293.93, 0.146860));
        points.add(new WeightedObservedPoint(1.0, 319.55, 0.151910));
        points.add(new WeightedObservedPoint(1.0, 343.61, 0.157880));
        points.add(new WeightedObservedPoint(1.0, 358.25, 0.162930));
        points.add(new WeightedObservedPoint(1.0, 388.84, 0.174020));
        points.add(new WeightedObservedPoint(1.0, 408.44, 0.181840));
        points.add(new WeightedObservedPoint(1.0, 439.82, 0.196120));
        points.add(new WeightedObservedPoint(1.0, 464.91, 0.215180));
        points.add(new WeightedObservedPoint(1.0, 485.04, 0.240770));
        points.add(new WeightedObservedPoint(1.0, 497.59, 0.273470));
        points.add(new WeightedObservedPoint(1.0, 503.87, 0.310710));

        cp = fitTo(points, Temperature.at(207.66, "K"),
            Temperature.at(503.87, "K"), "K", "kJ/mol-K");

        System.err.println("Fitted parameters:");
        System.err.printf("    %f\n", cp.c[0]);
        System.err.printf("    %f\n", cp.c[1]);
        System.err.printf("    %f\n", cp.c[2]);
        System.err.printf("    %f\n", cp.c[3]);
        System.err.printf("    %f\n", cp.c[4]);

        for (WeightedObservedPoint point: points) {
            try {
                log.debug(point.getX() + ", " +
                    cp.eval(Temperature.at(point.getX(), "K")).kJmolK());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }



    }

}
