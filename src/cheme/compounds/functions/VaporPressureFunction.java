/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.compounds.functions;

import cheme.methods.ThermoStateException;
import cheme.properties.Temperature;
import cheme.properties.Pressure;

/**
 * This class relates saturated temperature and pressure for a compound.
 */
public abstract class VaporPressureFunction {

    /**
     * Minimum temperature bound for this set of coefficients.
     */
    protected Temperature minimumTemp;

    /**
     * Maximum temperature bound for this set of coefficients.
     */
    protected Temperature maximumTemp;

    /**
     * The temperature unit for this set of coefficients.
     */
    protected String tempUnit;

    /**
     * The pressure unit for this set of coefficients.
     */
    protected String pressureUnit;

    /**
     * Get the minimum temperature bound for this function.
     *
     * @return the minimum temperature
     */
    public Temperature getMinimumTemp() {
        return minimumTemp;
    }

    /**
     * Get the maximum temperature bound for this function.
     *
     * @return the maximum temperature
     */
    public Temperature getMaximumTemp() {
        return maximumTemp;
    }

    /**
     * Get the temperature units for this function.
     *
     * @return the temperature unit
     */
    public String getTempUnit() {
        return tempUnit;
    }

    /**
     * Get the pressure unit for this function.
     *
     * @return the pressure unit
     */
    public String getPressureUnit() {
        return pressureUnit;
    }

    /**
     * Compute the vapor pressure at a specific temperature.
     *
     * @param T temperature to compute vapor pressure at
     * @return the vapor pressure
     * @throws ThermoStateException if the temperature is outside the valid
     * range
     */
    public abstract Pressure eval(final Temperature T)
        throws ThermoStateException;

    /**
     * Compute the saturated temperature at a specific vapor pressure.
     *
     * @param P pressure to compute saturated temperature at
     * @return the saturated temperature
     * @throws ThermoStateException if the pressure is outside the valid
     * range
     */
    public abstract Temperature eval(final Pressure P)
        throws ThermoStateException;

}
