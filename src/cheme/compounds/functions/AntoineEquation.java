/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.compounds.functions;

import cheme.methods.ThermoStateException;
import cheme.properties.Temperature;
import cheme.properties.Pressure;

/**
 * This class computes liquid vapor pressure via the following polynomial:
 *
 * ln(Psat) = c0 + c1 / (c2 + Tsat)
 */
public class AntoineEquation extends VaporPressureFunction {

    /**
     * The coefficients of the equation.
     */
    private double [] c;

    /**
     * Public constructor.
     *
     * @param coefficients the four coefficients of the heat capacity
     * polynomial.
     * @param minimumTemp the minimum temperature bound for this set of
     * coefficients
     * @param maximumTemp the maximum temperature bound for this set of
     * coefficients
     * @param tempUnit the temperature unit ("K", "C", etc.) used by the
     * terms of this polynomial.
     * @param pressureUnit the pressure unit ("Pa", "psig", etc.) produced by
     * the terms of this function.
     */
    public AntoineEquation(final double [] coefficients,
        final Temperature minimumTemp, final Temperature maximumTemp,
        final String tempUnit, final String pressureUnit) {

        if (coefficients.length != 3) {
            throw new IllegalArgumentException("Antoine equation " +
                "requires 3 coefficients, only " + coefficients.length +
                "given");
        }
        c = new double[3];
        c[0] = coefficients[0];
        c[1] = coefficients[1];
        c[2] = coefficients[2];

        this.minimumTemp  = Temperature.at(minimumTemp.K(), 'K');
        this.maximumTemp  = Temperature.at(maximumTemp.K(), 'K');
        this.tempUnit     = tempUnit;
        this.pressureUnit = pressureUnit;
    }

    /**
     * Compute the vapor pressure at a specific temperature.
     *
     * @param T temperature to compute vapor pressure at
     * @return the vapor pressure
     * @throws ThermoStateException if the temperature is outside the valid
     * range
     */
    @Override
    public Pressure eval(final Temperature T)
        throws ThermoStateException {

        if ((T.compareTo(minimumTemp) < 0) || (T.compareTo(maximumTemp) > 0)) {
            throw new ThermoStateException("T: " + T + " outside range " +
                minimumTemp + " <= T <= " + maximumTemp);
        }

        double t = T.get(tempUnit);
        double value = Math.exp(c[0] + c[1] / (c[2] + t));
        return Pressure.at(value, pressureUnit);
    }

    /**
     * Compute the saturated temperature at a specific vapor pressure.
     *
     * @param P pressure to compute saturated temperature at
     * @return the saturated temperature
     * @throws ThermoStateException if the pressure is outside the valid
     * range
     */
    @Override
    public Temperature eval(final Pressure P)
        throws ThermoStateException {

        // Find the temperature, and then throw if it ends up begin outside
        // the valid range.
        double t = c[1] / (Math.log(P.get(pressureUnit)) - c[0]) - c[2];
        Temperature T = Temperature.at(t, tempUnit);

        if ((T.compareTo(minimumTemp) < 0) || (T.compareTo(maximumTemp) > 0)) {
            throw new ThermoStateException("T: " + T + " outside range " +
                minimumTemp + " <= T <= " + maximumTemp);
        }

        return T;
    }

}
