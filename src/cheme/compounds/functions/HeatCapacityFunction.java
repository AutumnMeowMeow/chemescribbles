/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.compounds.functions;

import cheme.methods.ThermoStateException;
import cheme.properties.Enthalpy;
import cheme.properties.HeatCapacity;
import cheme.properties.Temperature;

/**
 * A HeatCapacityFunction has methods for computing the heat capacity of a
 * compound or mixture.
 */
public interface HeatCapacityFunction {

    /**
     * Get the minimum temperature bound for this function.
     *
     * @return the minimum temperature
     */
    public Temperature getMinimumTemp();

    /**
     * Get the maximum temperature bound for this function.
     *
     * @return the maximum temperature
     */
    public Temperature getMaximumTemp();

    /**
     * Get the temperature units for this function.
     *
     * @return the temperature unit
     */
    public String getTempUnit();

    /**
     * Compute the heat capacity at a specific temperature.
     *
     * @param T temperature to compute heat capacity at
     * @return the heat capacity
     * @throws ThermoStateException if the temperature is outside the valid
     * range
     */
    public HeatCapacity eval(final Temperature T) throws ThermoStateException;

    /**
     * Compute the enthalpy change between two temperatures, i.e. integrate
     * the heat capacity change between T0 and T1.
     *
     * @param T0 starting temperature
     * @param T1 ending temperature
     * @return the enthalpy
     * @throws ThermoStateException if T0 or T1 is outside the valid range
     */
    public Enthalpy integrate(final Temperature T0, final Temperature T1)
        throws ThermoStateException;

}
