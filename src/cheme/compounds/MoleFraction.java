/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme.compounds;

/**
 * A MoleFraction links a compound to a mole fraction.
 */
public class MoleFraction {

    /**
     * The compound.
     */
    public Compound compound;

    /**
     * Its mole fraction.
     */
    public double fraction;

    /**
     * Construct an instance with compound and mole fraction.
     *
     * @param compound the compound
     * @param fraction the mole fraction
     */
    public MoleFraction(final Compound compound, final double fraction) {
        if (fraction < 0) {
            throw new IllegalArgumentException("Invalid molar fraction: " +
                fraction + " for component " + compound);
        }

        this.compound = compound;
        this.fraction = fraction;
    }

    /**
     * Make human-readable description of this MoleFraction.
     *
     * @return displayable String
     */
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append(String.format("%f ", fraction));
        str.append(compound);
        return str.toString();
    }

}
