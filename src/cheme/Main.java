/*
 * ChemE Scribbles
 *
 * Copyright (c) 2021 Autumn Lamonte
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package cheme;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import cheme.tests.method.CubicEOSTests;
import cheme.tests.method.IdealFlashTests;
import cheme.tests.ui.DiagramTests;
import cheme.tests.data.StatisticsTests;

/**
 * This class is the main driver for ChemE Scribbles.
 */
public class Main {

    /**
     * The Logger to emit to.
     */
    private static Logger log = LogManager.getLogger(Main.class.getName());

    /**
     * Main entry point.
     *
     * @param args Command line arguments
     */
    public static void main(final String [] args) {
        try {
            log.info("Starting up...");

            // CubicEOSTests.runTests();
            DiagramTests.runTests();
            // IdealFlashTests.runTests();
            // StatisticsTests.runTests();


            log.info("Done.");
        } catch (Throwable t) {
            log.error(t, t);
        }
    }

}
